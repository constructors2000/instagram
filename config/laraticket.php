<?php

return[
	'layout' => 'laraticket::layouts.new',
	/*
	|--------------------------
	| If you probably need to use a different template layout for the admin part of the ticket
	| you can make change this value to match
	|--------------------------
	*/
	'admin_layout' => 'laraticket::layouts.admin',
	/**
	 * In case your default users tablename is not users
	 * You can change this value to reflect your table name 
	 * This will have an effect in migrations
	 * 
	 * *
	 * */

	'user_table_name' => 'users',

	/**
	 * Change this value if your user model is not located in the laravel's default App directory
	 * 
	 * */
	'user_model_namespace' => 'App\User',

	/**@internal Where should users be taken to when they click on navbar brand
	**/
	'return_url' => 'user',
	/*
	|-----------------------------------------------------------------------
	|Use this to set the number of items returned when retrieving from database
	|For pagination purpose
	|-----------------------------------------------------------------------
	*/
	'per_page' => 20,
	/*
	|--------------------------------------------------------
	|If you have a customised pagination template to use,
	|set it's value here
	|
	*/
	'pagination_view_name' => 'pagination::bootstrap-4'
];