<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('price')->nullable();
            $table->string('instagram_accounts')->nullable()->default("1");
            $table->text('features')->nullable();
            $table->string('automation_speed')->nullable()->default("medium");
            $table->string('support')->nullable();
            $table->string('valid_time')->nullable()->default("7");
            $table->boolean('enabled')->default(true);
            
            $table->string('stripe_plan_name')->nullable();
            $table->string('stripe_plan_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
