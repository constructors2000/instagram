<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('username');
            $table->string('password');
            $table->string('instagram_pk');
            $table->string('instagram_bio');
            $table->string('instagram_full_name');
            $table->string('instagram_username');
            $table->string('instagram_followers');
            $table->string('instagram_following');
            $table->string('instagram_posts');
            $table->string('instagram_pic');
            $table->string('instagram_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_accounts');
    }
}
