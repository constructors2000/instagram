<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('instagram_id');
            $table->string('user_id');

            // mode
            $table->string('mode')->default('automatic');

            // enabled / disabled
            $table->boolean('status')->default(true);

            // blacklist
            $table->string('blacklist')->nullable();
            
            // messages
            $table->boolean('message_by_bot')->default(false);
            $table->string('message_time_interval')->default('600');
            $table->string('message_time')->nullable();
            $table->string('max_messages')->default('20');
            $table->string('messages_count_condition')->nullable();
            $table->string('messages')->nullable();

            // likes
            $table->boolean('like_by_bot')->default(false);
            $table->string('max_likes')->default('20');
            
            // comments
            $table->boolean('comment_by_bot')->default(false);
            $table->string('max_comments')->default('20');
            $table->string('comments')->nullable();

            $table->string('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
