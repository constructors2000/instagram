<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table("users")->insert([
            "first_name" => "Super",
            "last_name"  => "Admin",
            "email"      => "contact@the360circle.com",
            "enabled"    => true,
            'password' => bcrypt('hREAegb437R5hj'),
        ]);

        DB::table("user_role")->insert([
            'user_id'   =>  '1',
            'levels'    =>  '["0"]'
        ]);

        DB::table("site_settings")->insert([
            'language'  =>  'en',
            'theme'     =>  'blue',
            'name'      =>  'My Brand Bot'
        ]);

        DB::table("levels")->insert([
            'name'                   =>  'core',
            'instagram_accounts'     =>  '1',
            'automation_speed'       =>  'medium',
            'valid_time'             =>  '7',
            'enabled'                =>  false
        ]);
    }
}
