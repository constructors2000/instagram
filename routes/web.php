<?php

    // guest section

        Route::get('/', 'AccountController@index');
        
        // logout
        Route::post('/logout', 'AccountController@postLogout');

        // activate
        Route::get('/activate', 'AccountController@getActivate');
        Route::post('/activate', 'AccountController@postActivate');

        Route::group(['middleware' => 'guest'],function(){
            
            // login
            Route::get('/login', 'AccountController@getLogin');
            Route::post('/login', 'AccountController@postLogin');

            // register
            Route::get('/register', 'AccountController@getRegister');
            Route::post('/register', 'AccountController@postRegister');
            
            // reset
            Route::get('/reset', 'AccountController@getReset');
            Route::post('/reset', 'AccountController@postReset');

            // reset password
            Route::get('/reset_password', 'AccountController@getResetPassword');
            Route::post('/reset_password', 'AccountController@postResetPassword');
        });

    // admin

        Route::group(['prefix' => 'admin', 'middleware' => 'admin'],function(){
                
            // overview
            Route::get('/', 'AdminController@index');

            Route::get('/dashboard', 'AdminController@dashboard');

            // users
                // list all users
                // Route::get('/users', 'AdminController@getUsers');
                
                // add new user
                Route::get('/users/add', 'AdminController@getAddUser');
                Route::post('/users/add', 'AdminController@postAddUser');

                // edit user
                Route::get('/users/edit/{id}', 'AdminController@getEditUser');
                Route::post('/users/edit', 'AdminController@postEditUser');

                // delete user
                Route::get('/users/delete/{id}', 'AdminController@getDeleteUser');

            // levels
            Route::get('/levels', 'AdminController@getLevels');
            
            Route::get('/levels/add', 'AdminController@getAddLevel');
            Route::post('/levels/add', 'AdminController@postAddLevel');

            Route::get('/levels/edit/{id}', 'AdminController@getEditLevel');
            Route::post('/levels/edit', 'AdminController@postEditLevel');

            Route::get('/levels/enable/{id}', 'AdminController@postEnableLevel');
            Route::get('/levels/disable/{id}', 'AdminController@postDisableLevel');

            // settings
            // Route::get('/settings', 'AdminController@getSettings');
            // Route::post('/settings', 'AdminController@postSettings');

            Route::get('/speed', 'AdminController@getSpeed');
            Route::post('/speed', 'AdminController@postSpeed');

            Route::get('/site', 'AdminController@getSite');
            Route::post('/site', 'AdminController@postSite');

            Route::get('/remove-logo', 'AdminController@getRemoveLogo');

            // settings

            Route::get('/settings/email', 'AdminController@getEmail');
            Route::post('/settings/email', 'AdminController@postEmail');

            Route::get('/settings/pages', 'AdminController@getPages');
            Route::post('/settings/pages', 'AdminController@postPages');

            Route::get('/proxies', 'AdminController@getProxies');
            Route::get('/proxies/add', 'AdminController@getAddProxy');
            Route::post('/proxies/add', 'AdminController@postAddProxy');
            Route::get('/proxies/edit/{id}', 'AdminController@getEditProxy');
            Route::post('/proxies/edit', 'AdminController@postEditProxy');
            Route::get('/proxies/delete/{id}', 'AdminController@postDeleteProxy');

            Route::post('/send_notification', 'AdminController@postSendNotification');
            
        });

    // user
        Route::group(['prefix' => 'user', 'middleware' => 'user'],function(){

            Route::get('/billing', 'UserController@getBilling');
            
            // only access when an instagram account is linked
            Route::group(['middleware'  =>  'instagramExists'],function(){
                // overview
                
            });

            // dashboard
            Route::get('/', 'UserController@index');
            
            // accounts
            Route::get('/accounts', 'UserController@getAccounts');
            Route::get('/accounts/edit', 'UserController@getEditAccounts');
            Route::post('/instagram/edit/', 'UserController@getEditInstaAccounts');
            Route::post('/accounts/settings/timezone', 'UserController@updateTimezone');
            Route::post('/accounts/settings/user_info', 'UserController@updateUserInfo');
            Route::post('/accounts/settings/update', 'UserController@updateAccountSettings');
            Route::post('/accounts/settings/messages', 'UserController@updateAccountMessages');
            Route::post('/accounts/settings/comments', 'UserController@updateAccountComments');
            Route::post('/accounts/settings/blacklist', 'UserController@updateAccountBlacklist');
            Route::post('/accounts/settings/groups', 'UserController@updateGroups');
            Route::post('/accounts/settings/general', 'UserController@updateGeneral');
            Route::post('/accounts/settings/general_mode', 'UserController@updateGeneralMode');

            Route::get('/refresh_group_list', 'UserController@getNewGroupList');

            Route::get('/profile', function(){
                return redirect('/');
            });

            Route::get('/payment', 'UserController@getPayment');

            // settings    
            // timezone, languages etc.
            Route::get('/settings', 'UserController@getSettings');

            Route::get('/support', 'UserController@getSupport');

            // instagram

            // add
            Route::post('/instagram/add', 'UserController@postAddInstagram');
            Route::post('/instagram/verify', 'UserController@postVerifyInstagramAccount');

            Route::get('/markReadAll', 'UserController@markReadAll');
            // delete
            Route::post('/instagram/delete', 'UserController@postDeleteInstagram');


        });

        Route::group(['prefix'  =>  'cron'], function(){

            // Route::get('/capture', 'CronController@capture');
            // Route::get('/test', 'CronController@schedule_posts');

            Route::get('/auto', 'CronController@cron_auto');
            Route::get('/semi', 'CronController@cron_semi');
            Route::get('/manual', 'CronController@cron_manual');
            Route::get('/schedule', 'CronController@schedule_posts');
        });


        // Paypal

        // Route::get('create_paypal_plan', 'PaypalController@create_plan');

        Route::get('/subscribe/paypal', 'PaypalController@paypalRedirect')->name('paypal.redirect');
        Route::get('/subscribe/paypal/return', 'PaypalController@paypalReturn')->name('paypal.return');
