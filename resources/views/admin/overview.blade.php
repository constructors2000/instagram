@extends('_layouts.admin')

@section('title','Home')

@section('content')

<h2 class="content-heading">All Users</h2>
<div class="col-md-12">
	<div class="block">
		<div class="block-content block-content-full">
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
				<thead>
					<tr>
						<th class="text-center">ID</th>
						<th>Name</th>
						<th>Email</th>
						<th>Level</th>
						<th>Created_By</th>
						<th class="text-center" style="width: 15%;">Profile</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td class="text-center">{{$user['id']}}</td>
						<td class="font-w600">{{$user['name']}}</td>
						<td class="font-w600">{{$user['email']}}</td>
						<td class="font-w600">
							@foreach($user['levels'] as $level)
								{{ $level['name']}} &nbsp;
							@endforeach
						</td>
						<td class="font-w600">{{$user['created_by']}}</td>
						<td class="text-center">
							<button data-id="{{ $user['id'] }}" class="btn btn-sm btn-secondary send_notification" data-toggle="tooltip" title="Send Notification">
								<i class="fa fa-bullhorn"></i>
							</button>

							<a href="{{ URL::to('admin/users/edit/' . $user['id']) }}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit User">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="{{ URL::to('admin/users/delete/' . $user['id']) }}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete User">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					<tr>
					@endforeach
				</tbody>
			</table>
			
		</div>
	</div>
</div>

<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form action="{{ url('admin/send_notification') }}" method="post">
      	<div class="modal-body">
			{{ csrf_field() }}
			<input type="hidden" name="user_id" id="notification_user_id"/>
			<input type="text" required name="title" class="form-control" placeholder="Title"/>
			<br>
			<textarea class="form-control" name="content" required rows="5" placeholder="Content"></textarea>
      	</div>
      	<div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        	<button type="submit" class="btn btn-primary">Send Notification</button>
      	</div>
	  </form>
    </div>
  </div>
</div>

@endsection

@section('footer')
<script>
	$(".send_notification").on("click", function(){
		var _id = $(this).attr('data-id');
		$("#notification_user_id").val(_id);
		$("#notificationModal").modal("show");
	})
</script>
@endsection