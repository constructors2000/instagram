@extends('_layouts.admin')

@section('title','Dashboard')

@section('header')
@endsection

@section('content')

    <h2 class="content-heading">Dashboard</h2>
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-3">

                <ul class="list-group">
                    <li class="list-group-item">
                        Total Users
                        <span class="float-right badge badge-primary">{{ $data['total'] }}</span>
                    </li>
                    <li class="list-group-item">
                        Free Users
                        <span class="float-right badge badge-info">{{ $data['free'] }}</span>
                    </li>
                    <li class="list-group-item">
                        Basic Plan
                        <span class="float-right badge badge-danger">{{ $data['basic'] }}</span>
                    </li>
                    <li class="list-group-item">
                        Pro Plan
                        <span class="float-right badge badge-success">{{ $data['pro'] }}</span>
                    </li>
                </ul>

            </div>
            <div class="col-md-9">
                <canvas id="myChart"></canvas>
            </div>
        </div>

    </div>
@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
    <script>
        var ctx = document.getElementById("myChart");


        var data = {
            datasets: [{
                data: [
                    {{ $data['free'] }},
                    {{ $data['basic'] }},
                    {{ $data['pro'] }}
                ],
                backgroundColor: [
                    '#26c6da',
                    '#ef5350',
                    '#9ccc65'
                ]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Direct (free)',
                'Basic Plan',
                'Pro Plan'
            ]
        };

        var myChart = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: {
                responsive: true
            }
        });
    </script>
@endsection