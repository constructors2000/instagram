@extends('_layouts.admin')

@section('title','Home')

@section('content')

<h2 class="content-heading">Edit User</h2>
<div class="col-md-6">
	<div class="block">
		<div class="block-content block-content-full">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
			<form  method="post" action="{{ url('/admin/users/edit') }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$user->id}}"/>
                <div class="form-group">
                    <label for="example-nf-email">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}" required>
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}" required>
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Email</label>
                    <input type="email" class="form-control" name="email" value="{{$user->email}}" disabled>
                </div>
                <div class="form-group">    
                    <label for="example-nf-email">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="New Password">
                </div>
                <div class="form-group">
                    @foreach($levels as $level)
                        <input type="radio" name="levels"  value="{{$level->id}}"
                        @if(in_array($level->id, $selected_levels)){
                            checked
                        @endif> 
                        <label>
                            {{$level->name}}
                        </label><br>
                    @endforeach
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-alt-primary"/>
                </div>
            </form>
		</div>
	</div>
</div>


@endsection

@section('footer')
@endsection