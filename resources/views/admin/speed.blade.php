@extends('_layouts.admin')

@section('title','Home')

@section('content')

	<h2 class="content-heading">Default Speed Settings</h2>

	<div class="col-md-6">
		
		<form action="{{ url('/admin/speed') }}" class="form" method="POST">
			{{ csrf_field() }}
			
			<label for="">
				Ultra Fast (in minutes)
			</label>

			<select name="ultra_fast_speed" id="ultra_fast_speed" class="form-control">
				@for($i = 2; $i <= 60; $i++)
					<option value="{{ $i*60 }}">{{ $i }} minutes</option>
				@endfor
			</select>

			<hr>

			<label for="">
				Fast (in minutes)
			</label>

			<select name="fast_speed" id="fast_speed" class="form-control">
				@for($i = 5; $i <= 60; $i++)
					<option value="{{ $i*60 }}">{{ $i }} minutes</option>
				@endfor
			</select>

			<hr>

			<label for="">
				Medium (in minutes)
			</label>

			<select name="medium_speed" id="medium_speed" class="form-control">
				@for($i = 10; $i <= 60; $i++)
					<option value="{{ $i*60 }}">{{ $i }} minutes</option>
				@endfor
			</select>

			<hr>

			<label for="">
				Slow (in minutes)
			</label>

			<select name="slow_speed" id="slow_speed" class="form-control">
				@for($i = 15; $i <= 60; $i++)
					<option value="{{ $i*60 }}">{{ $i }} minutes</option>
				@endfor
			</select>


			<br>
			<button type="submit" class="btn btn-success btn-block">Save</button>
		</form>

	</div>

@endsection

@section('footer')

	<script>
		$("#ultra_fast_speed").val("{{$data->ultra_fast}}");
		$("#fast_speed").val("{{$data->fast}}");
		$("#medium_speed").val("{{$data->medium}}");
		$("#slow_speed").val("{{$data->slow}}");
	</script>
@endsection