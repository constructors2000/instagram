@extends('_layouts.admin')

@section('title','Add Proxy')

@section('content')

<style>
input[type=radio], input[type=checkbox]{width:20px !Important;}
</style>
<h2 class="content-heading">Add New Proxy</h2>
<div class="col-md-6">
	<div class="block">
        <div class="block-content block-content-full">
            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
			<form  method="post" action="{{ url('/admin/proxies/add') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="example-nf-email">For Speed</label>
                    <select name="speed_type" class="form-control">
                        <option value="0">Ultra Fast (1 proxy for 1 instagram account with minimum speed 2 minutes)</option>
                        <option value="1">Fast (max 5 instagram accounts with minimum speed 5 minutes) </option>
                        <option value="2">Medium (max 7 instagram accounts with minimum speed 10 minutes)</option>
                        <option value="3">Slow (max 10 instagram accounts with minimum speed 15 minutes)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="example-nf-email">For How Many Users</label>
                    <input type="number" class="form-control" name="total_users" placeholder="enter Number of Users" required min="0" max="10" value="1">
                </div>
                
                <hr>
                <b>Proxy Provider Settings</b>
                <br><br>
                <div class="form-group">
                    <label>IP</label>
                    <input type="text" class="form-control" name="ip" placeholder="enter IP.." required>
                </div>
                <div class="form-group">
                    <label>Port</label>
                    <input type="text" class="form-control" name="port" placeholder="enter Port.." required>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="proxy_username" placeholder="enter proxy username.." required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="text" class="form-control" name="proxy_password" placeholder="enter proxy password.." required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-alt-primary" value="Add Proxy"/>
                </div>
            </form>
		</div>
	</div>
</div>


@endsection

@section('footer')
    <script>
        $("select[name=speed_type]").change(function(e){
            // var _sel = $("select[name=speed_type]:selected").val();
            // alert(_sel);
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            if(valueSelected == "0")
            {
                $("input[name=total_users]").val("1");
            }
            if(valueSelected == "1")
            {
                $("input[name=total_users]").val("5");
            }
            if(valueSelected == "2")
            {
                $("input[name=total_users]").val("7");
            }
            if(valueSelected == "3")
            {
                $("input[name=total_users]").val("10");
            }
        })
    </script>
@endsection