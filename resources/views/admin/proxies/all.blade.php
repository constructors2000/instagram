@extends('_layouts.admin')

@section('title','Proxies')

@section('content')

<div class="col-md-12">
    
    <h2 class="content-heading">
        All Proxies
        <small class="pull-right">
            <a href="{{ url('/admin/proxies/add') }}" class="btn btn-success">+ Add New</a>
        </small>
    </h2>

	<div class="block">
		<div class="block-content block-content-full">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
				<thead>
					<tr>
						<th>Speed Type</th>
						<th>Total Users</th>
                        <th>Using</th>
                        <th>Proxy</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				    @foreach($proxies as $proxy)
					<tr>
						<td class="text-center">
                            <b>
                                @if($proxy->speed_type == "0")
                                    Ultra Fast
                                @endif
                                @if($proxy->speed_type == "1")
                                    Fast
                                @endif
                                @if($proxy->speed_type == "2")
                                    Medium
                                @endif
                                @if($proxy->speed_type == "3")
                                    Slow
                                @endif
                            </b>
                        </td>
                        <td class="text-center">{{ $proxy->total_users }}</td>
                        <td class="text-center">{{ $proxy->using_users }}</td>
                        <td class="text-center">
                            {{ $proxy->ip }}:
                            {{ $proxy->port }}<br>
                            {{ $proxy->username }}:
                            {{ $proxy->password }}
                        </td>
						<td class="text-center">
							<a href="{{ URL::to('admin/proxies/edit/' . $proxy->id) }}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Proxy">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="{{ URL::to('admin/proxies/delete/' . $proxy->id) }}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete Proxy">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					<tr>
				    @endforeach
                </tbody>
			</table>
			
		</div>
	</div>
</div>


@endsection

@section('footer')
@endsection