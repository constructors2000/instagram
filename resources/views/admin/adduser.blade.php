@extends('_layouts.admin')

@section('title','Home')

@section('content')

<style>
input[type=radio], input[type=checkbox]{width:20px !Important;}
</style>
<h2 class="content-heading">Add New User</h2>
<div class="col-md-6">
	<div class="block">
        <div class="block-content block-content-full">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
			<form  method="post" action="{{ url('/admin/users/add') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="example-nf-email">First Name</label>
                    <input type="text" class="form-control" name="first_name" placeholder="Enter Name.." required>
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Last Name</label>
                    <input type="text" class="form-control" name="last_name" placeholder="Enter Name.." required>
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Enter Email.." required>
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Enter Password" required>
                </div>
                <div class="form-group">
                    @foreach($levels as $level)
                        <input type="radio" name="levels"  value="{{ $level->id }}"> 
                        <label>
                            {{$level->name}}
                        </label><br>
                    @endforeach
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-alt-primary" value="Add User"/>
                </div>
            </form>
		</div>
	</div>
</div>


@endsection

@section('footer')
@endsection