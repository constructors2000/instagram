@extends('_layouts.admin')

@section('title','Home')

@section('content')

	<h2 class="content-heading">Site Settings</h2>

	<div class="col-md-6">
		
		<form action="{{ url('/admin/site') }}" class="form" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			
			<label for="">
				Site Name
			</label>

			<input type="text" name="site_name" class="form-control" value="{{ $data->name }}"/>
			
			<hr>

			<label for="">
				Site Logo (optional)
			</label>

			@if($data->logo != null)
				<br>
				<img src="{{ asset('/public/uploads/logos/logo') }}" class="img-fluid" style="width: 100px;height: 100px;"/>
				<br>
				<br>
				<a href="{{ url('/admin/remove-logo') }}" class="btn btn-danger">remove</a>
				<br><br>
			@endif

			<input type="file" name="site_logo" class="form-control"/>

			<hr>
			
			<label for="">
				Default Language
			</label>

			<select name="site_language" class="form-control">
				<option value="en">English</option>
                <option value="es">Spanish</option>
				<option value="fr">French</option>
                <option value="it">Italian</option>
			</select>

			<hr>

			<label for="">
				Theme
			</label>

			<select name="site_theme" class="form-control">
				<option value="blue">Blue</option>
                <option value="green">Green</option>
				<option value="red">Red</option>
				<option value="purple">Purple</option>
				<option value="gray">Gray</option>
				<option value="dark">Dark</option>
			</select>


			<br>
			<button type="submit" class="btn btn-success btn-block">Save</button>
		</form>

	</div>

@endsection

@section('footer')

	<script>
        $("select[name=site_language]").val("{{$data->language}}");
		$("select[name=site_theme]").val("{{$data->theme}}");
	</script>
@endsection