@extends('_layouts.admin')

@section('title','Home')

@section('content')

<style>
    input[type=radio], input[type=checkbox]{
        width:20px !Important;
    }
</style>
<h2 class="content-heading">Edit Level</h2>
<div class="col-md-6">
	<div class="block">
        <div class="block-content block-content-full">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
			<form  method="post" action="{{ url('/admin/levels/edit') }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $level->id }}"/>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Plan Name</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="name" placeholder="Enter Name" required value="{{ $level->name }}">
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Price ($ in dollars)</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="price" placeholder="Enter Cost" required value="{{ $level->price }}">
                        @if ($errors->has('price'))
                            <span class="help-block text-danger">
                                {{ $errors->first('price') }}
                            </span>
                        @endif
                    </div>
                </div>

                <hr>
                <b>Options:</b>
                <br>
                <br>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Instagram Accounts:</label>
                    <div class="col-lg-7">
                        <input type="number" class="form-control" name="instagram_accounts" placeholder="" min="1" value="{{ $level->instagram_accounts }}" required>
                        @if ($errors->has('instagram_accounts'))
                            <span class="help-block text-danger">
                                {{ $errors->first('instagram_accounts') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Features to Enable:</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="features[]" value="follow"
                            @if($level->features != null)
                                @if(in_array("follow", json_decode($level->features)))
                                    checked
                                @endif
                            @endif
                        /> Auto Follow
                        <br>
                        <input type="checkbox" name="features[]" value="unfollow"
                            @if($level->features != null)
                                @if(in_array("unfollow", json_decode($level->features)))
                                    checked
                                @endif
                            @endif
                        /> Auto Un Follow
                        <br>
                        <input type="checkbox" name="features[]" value="like"
                            @if($level->features != null)
                                @if(in_array("like", json_decode($level->features)))
                                    checked
                                @endif
                            @endif
                        /> Like
                        <br>
                        <input type="checkbox" name="features[]" value="comment"
                            @if($level->features != null)
                                @if(in_array("comment", json_decode($level->features)))
                                    checked
                                @endif
                            @endif
                        /> Comment
                        <br>
                        <input type="checkbox" name="features[]" value="message"
                            @if($level->features != null)
                                @if(in_array("message", json_decode($level->features)))
                                    checked
                                @endif
                            @endif
                        /> Message
                        
                        @if ($errors->has('features'))
                            <br>
                            <span class="help-block text-danger">
                                {{ $errors->first('features') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">
                        Automation Speed:
                    </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="automation_speed">
                            <option value="slow">Slow</option>
                            <option value="medium">Medium</option>
                            <option value="fast">Fast</option>
                            <option value="ultra_fast">Ultra Fast</option>
                        </select>
                        <a href="{{ url('/admin/speed') }}" target="_blank">configure <b>slow</b>, <b>medium</b> and <b>fast</b> values</a>

                        @if ($errors->has('automation_speed'))
                            <br>
                            <span class="help-block text-danger">
                                {{ $errors->first('automation_speed') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Available Support:</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="support[]" value="email"
                            @if($level->support != null)
                                @if(in_array("email", json_decode($level->support)))
                                    checked
                                @endif
                            @endif
                        /> Email
                        <br>
                        <input type="checkbox" name="support[]" value="phone"
                            @if($level->support != null)
                                @if(in_array("phone", json_decode($level->support)))
                                    checked
                                @endif
                            @endif
                        /> Phone
                        <br>
                        <input type="checkbox" name="support[]" value="forum"
                            @if($level->support != null)
                                @if(in_array("forum", json_decode($level->support)))
                                    checked
                                @endif
                            @endif
                        /> Forum
                        <br>
                        <input type="checkbox" name="support[]" value="skype"
                            @if($level->support != null)
                                @if(in_array("skype", json_decode($level->support)))
                                    checked
                                @endif
                            @endif
                        /> Skype
                        @if ($errors->has('support'))
                            <br>
                            <span class="help-block text-danger">
                                {{ $errors->first('support') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Valid for (days):</label>
                    <div class="col-lg-7">
                        <input type="number" class="form-control" name="valid_time" placeholder="" min="1" required value="{{ $level->valid_time }}">
                        @if ($errors->has('valid_time'))
                            <span class="help-block text-danger">
                                {{ $errors->first('valid_time') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Stripe Plan Name:</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="stripe_plan_name" placeholder="Stripe Plan Name" value="{{ $level->stripe_plan_name }}"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Stripe Plan ID:</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="stripe_plan_id" placeholder="Stripe Plan ID" value="{{ $level->stripe_plan_id }}"/>
                    </div>
                </div>

                
                <div class="form-group">
                    <button type="submit" class="btn btn-lg btn-alt-primary">Update Level</button> 
                </div>
            </form>
		</div>
	</div>
</div>


@endsection

@section('footer')
    <script>
        $("select[name=automation_speed]").val("{{ $level->automation_speed }}");
    </script>
@endsection