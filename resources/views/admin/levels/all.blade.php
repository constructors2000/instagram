@extends('_layouts.admin')

@section('title','Home')

@section('content')

	<h2 class="content-heading">
        Levels
        <small class="pull-right">
            <a href="{{ url('/admin/levels/add') }}" class="btn btn-primary">
                + Add New
            </a>
        </small>
    </h2>


    <div class="row">

        @foreach($levels as $level)
            <div class="col-md-6 col-xl-3">
                <div class="block text-center block-themed">
                    @if($level->enabled)
                        <div class="block-content block-content-full ribbon ribbon-primary bg-success">
                    @else
                        <div class="block-content block-content-full ribbon ribbon-primary bg-danger">
                    @endif
                        <div class="ribbon-box">${{ $level->price }}</div>
                        <div class="text-center py-30">
                            <i class="fa fa-dollar fa-3x text-gray mb-15"></i>
                            <h4 class="mb-0">{{ $level->name }}</h4>
                        </div>
                    </div>
                    <div class="block-content">
                        <p><strong>{{ $level->instagram_accounts }}</strong> Instagram Accounts</p>
                        @if($level->features != null)
                            @foreach(json_decode($level->features) as $feature)
                                <p><strong>Auto</strong> {{ $feature }}</p>
                            @endforeach
                        @endif

                        <p><strong>{{ $level->automation_speed }}</strong> speed for automation</p>
                        
                        @if($level->support != null)
                            @foreach(json_decode($level->support) as $support)
                                <p><strong class="text-capitalize">{{ $support }}</strong> Support</p>
                            @endforeach
                        @endif
                        
                        <p><strong>Expires</strong> in {{ $level->valid_time }} days</p>
                    </div>
                    <div class="block-content block-content-full">
                        <a href="{{ url('/admin/levels/edit/'. $level->id) }}" class="btn btn-hero btn-sm btn-rounded btn-noborder btn-alt-primary">Edit Plan</a>
                        <br><br>
                        @if($level->enabled)
                            <a href="{{ url('/admin/levels/disable/'. $level->id) }}" class="btn btn-hero btn-sm btn-rounded btn-noborder btn-alt-danger">Disable Plan</a>
                        @else
                            <a href="{{ url('/admin/levels/enable/'. $level->id) }}" class="btn btn-hero btn-sm btn-rounded btn-noborder btn-alt-success">Enable Plan</a>
                        @endif
                    </div>
                </div>
                
            </div>
        @endforeach

    </div>

@endsection