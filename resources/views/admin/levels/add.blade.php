@extends('_layouts.admin')

@section('title','Home')

@section('content')

<style>
    input[type=radio], input[type=checkbox]{
        width:20px !Important;
    }
</style>
<h2 class="content-heading">Add New Level</h2>
<div class="col-md-6">
	<div class="block">
        <div class="block-content block-content-full">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
			<form  method="post" action="{{ url('/admin/levels/add') }}">
                {{ csrf_field() }}
                
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Plan Name</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="name" placeholder="Enter Name" required>
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Price ($ in dollars)</label>
                    <div class="col-lg-7">
                        <input type="string" class="form-control" name="price" placeholder="Enter Cost" required>
                        @if ($errors->has('price'))
                            <span class="help-block text-danger">
                                {{ $errors->first('price') }}
                            </span>
                        @endif
                    </div>
                </div>

                <hr>
                <b>Options:</b>
                <br>
                <br>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Instagram Accounts:</label>
                    <div class="col-lg-7">
                        <input type="number" class="form-control" name="instagram_accounts" placeholder="" min="1" value="1" required>
                        @if ($errors->has('instagram_accounts'))
                            <span class="help-block text-danger">
                                {{ $errors->first('instagram_accounts') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Features to Enable:</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="features[]" value="follow"/> Auto Follow
                        <br>
                        <input type="checkbox" name="features[]" value="unfollow"/> Auto Un Follow
                        <br>
                        <input type="checkbox" name="features[]" value="like"/> Like
                        <br>
                        <input type="checkbox" name="features[]" value="comment"/> Comment
                        <br>
                        <input type="checkbox" name="features[]" value="message"/> Message
                        
                        @if ($errors->has('features'))
                            <br>
                            <span class="help-block text-danger">
                                {{ $errors->first('features') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">
                        Automation Speed:
                    </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="automation_speed">
                            <option value="slow">Slow</option>
                            <option value="medium">Medium</option>
                            <option value="fast">Fast</option>
                            <option value="ultra_fast">Ultra Fast</option>
                        </select>
                        <a href="{{ url('/admin/speed') }}" target="_blank">configure <b>slow</b>, <b>medium</b> and <b>fast</b> values</a>

                        @if ($errors->has('automation_speed'))
                            <br>
                            <span class="help-block text-danger">
                                {{ $errors->first('automation_speed') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Available Support:</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="support[]" value="email"/> Email
                        <br>
                        <input type="checkbox" name="support[]" value="phone"/> Phone
                        <br>
                        <input type="checkbox" name="support[]" value="forum"/> Forum
                        <br>
                        <input type="checkbox" name="support[]" value="skype"/> Skype
                        @if ($errors->has('support'))
                            <br>
                            <span class="help-block text-danger">
                                {{ $errors->first('support') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Valid for (days):</label>
                    <div class="col-lg-7">
                        <input type="number" class="form-control" name="valid_time" placeholder="" min="1" value="7" required>
                        @if ($errors->has('valid_time'))
                            <span class="help-block text-danger">
                                {{ $errors->first('valid_time') }}
                            </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Stripe Plan Name:</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="stripe_plan_name" placeholder=""/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label">Stripe Plan ID:</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="stripe_plan_id" placeholder=""/>
                    </div>
                </div>

                
                <div class="form-group">
                    <button type="submit" class="btn btn-lg btn-alt-primary">Add Level</button> 
                </div>
            </form>
		</div>
	</div>
</div>


@endsection

@section('footer')
@endsection