@extends('_layouts.admin')

@section('title','Email Settings')

@section('content')

    <h2 class="content-heading">Email Settings</h2>
    <div class="col-md-5">
    <form  method="post" action="{{ url('/admin/settings/email') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="example-nf-email">Mail Host</label>
                    <input type="text" class="form-control" name="mail_host" placeholder="smtp.gmail.com" required
                        @if($email != null)
                            value="{{ $email->mail_host }}"
                        @endif
                    >
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Port</label>
                    <input type="text" class="form-control" name="mail_port" placeholder="587" required
                        @if($email != null)
                            value="{{ $email->mail_port }}"
                        @endif
                    >
                </div>
                <div class="form-group">
                    <label for="example-nf-email">From Address</label>
                    <input type="text" class="form-control" name="from_address" placeholder="you@gmail.com" required
                        @if($email != null)
                            value="{{ $email->from_address }}"
                        @endif
                    >
                </div>
                <div class="form-group">
                    <label for="example-nf-email">From Name</label>
                    <input type="text" class="form-control" name="from_name" placeholder="your company" required
                        @if($email != null)
                            value="{{ $email->from_name }}"
                        @endif
                    >
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Username</label>
                    <input type="email" class="form-control" name="username" placeholder="you@gmail.com" required
                        @if($email != null)
                            value="{{ $email->username }}"
                        @endif
                    >
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="password" required
                        @if($email != null)
                            value="{{ $email->password }}"
                        @endif
                    >
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-lg btn-alt-primary">Save Settings</button>
                </div>
            </form>
    </div>
@endsection