@extends('_layouts.admin')

@section('title','Pages')

@section('header')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <style>
        .note-popover.popover.in.note-link-popover.bottom,
        .note-popover.popover.in.note-image-popover.bottom,
        .note-popover.popover.in.note-table-popover.bottom
        {
            display: none;
        }
    </style>
@endsection

@section('content')

    <h2 class="content-heading">Pages</h2>
    <div class="col-md-12">
        <form  method="post" action="{{ url('/admin/settings/pages') }}">
            {{ csrf_field() }}

            <label for="">Privacy Policy</label>
            <div class="form-group">
                <textarea class="summernote" class="form-control" name="privacy_content" id="" cols="30" rows="10">@if($page != null){{$page->privacy}}@endif</textarea>
            </div>

            <label for="">Terms and Conditions</label>
            <div class="form-group">
                <textarea class="summernote" class="form-control" name="terms_content" id="" cols="30" rows="10">@if($page != null){{$page->terms}}@endif</textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-alt-primary">Save</button>
            </div>
        </form>
    </div>
@endsection

@section('footer')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote(
                {
                    placeholder: 'Page Content Here',
                    tabsize: 2,
                    height: 250
                }
            );
        });
    </script>
@endsection