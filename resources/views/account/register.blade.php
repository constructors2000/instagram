@extends('_layouts.guest')

@section('title','Login')

@section('header')
@endsection

@section('content')


    <div class="col-md-4 col-10 box-shadow-2 p-0">
        <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
            <div class="card-header border-0">
                <div class="text-center mb-1">
                    <img src="{{ asset('app-assets/images/logo/logo.png') }}" alt="branding logo">
                </div>
                <div class="font-large-1  text-center">
                    Become A Member
                </div>
            </div>
            <div class="card-content">

                <div class="card-body">

                    @if(session('status.error'))
                        <div class="alert alert-outline alert-danger">
                            {{ session('status.error') }}
                        </div>
                    @endif

                    <form class="form-horizontal" action="{{ url('/register') }}" method="post">
                        {{ csrf_field() }}
                        
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="text" class="form-control round" placeholder="First Name" aria-invalid="false" required name="first_name" minlength="3" maxlength="10" value="{{ old('first_name') }}"/>
                            <div class="form-control-position">
                                <i class="ft-user"></i>
                            </div>
                            @if ($errors->has('first_name'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('first_name') }}
                                </span>
                            @endif
                        </fieldset>

                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="text" class="form-control round" placeholder="Last Name" aria-invalid="false" required name="last_name" minlength="3" maxlength="10" value="{{ old('last_name') }}"/>
                            <div class="form-control-position">
                                <i class="ft-user"></i>
                            </div>
                            @if ($errors->has('last_name'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('last_name') }}
                                </span>
                            @endif
                        </fieldset>

                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="email" class="form-control round" placeholder="Your Email Address" required name="email" minlength="5" maxlength="50" value="{{ old('email') }}"/>
                            <div class="form-control-position">
                                <i class="ft-mail"></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </fieldset>

                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="password" class="form-control round" placeholder="Enter Password" required name="password" minlength="6" maxlength="20">
                            <div class="form-control-position">
                                <i class="ft-lock"></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </fieldset>

                        <div class="form-group text-center">
                            <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Register</button>
                        </div>

                    </form>
                </div>
                <p class="card-subtitle text-muted text-right font-small-3 mx-2 my-0">
                    <span>Already a member ?
                        <a href="{{ url('/login') }}" class="card-link">Sign In</a>
                    </span>
                </p>
            </div>
        </div>
    </div>
    
    
@endsection

@section('footer')
@endsection