@extends('_layouts.guest')

@section('title','Login')

@section('header')
@endsection

@section('content')



    <div class="col-md-4 col-10 box-shadow-2 p-0">
        <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
            <div class="card-header border-0">
                <div class="text-center mb-1">
                    <img src="{{ asset('app-assets/images/logo/logo.png') }}" alt="branding logo">
                </div>
                <div class="font-large-1  text-center">                       
                    Member Login
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    @if(session('status.success'))
                        <div class="alert alert-outline alert-success">
                            {{ session('status.success') }}
                        </div>
                    @endif

                    @if(session('status.error'))
                        <div class="alert alert-outline alert-danger">
                            {{ session('status.error') }}
                        </div>
                    @endif

                    @if ($errors->has('general'))
                        <div class="alert alert-outline alert-danger">
                            <strong>{{ $errors->first('general') }}</strong>
                        </div>
                    @endif
                    <form class="form-horizontal" action="{{ url('/login') }}" method="post">
                        {{ csrf_field() }}
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="email" class="form-control round" placeholder="Your Email" required name="email" minlength="5" maxlength="50" value="{{ old('email') }}">
                            <div class="form-control-position">
                                <i class="ft-user"></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </fieldset>
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="password" class="form-control round" placeholder="Enter Password" required name="password" minlength="6" maxlength="20">
                            <div class="form-control-position">
                                <i class="ft-lock"></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </fieldset>
                        <div class="form-group row">
                            <div class="col-md-6 col-12 text-center text-sm-left">
                            </div>
                            <div class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a href="{{ url('/reset') }}" class="card-link">Forgot Password?</a></div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Login</button>    
                        </div>
                    </form>
                </div>
                <p class="card-subtitle text-muted text-right font-small-3 mx-2 my-1"><span>Don't have an account ? <a href="{{ url('/register') }}" class="card-link">Sign Up</a></span></p>
            </div>
        </div>
    </div>

    
    
@endsection

@section('footer')
@endsection