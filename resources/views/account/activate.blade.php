@extends('_layouts.guest')

@section('title','Login')

@section('header')
@endsection

@section('content')



    <div class="col-md-4 col-10 box-shadow-2 p-0">
        <div class="card border-grey border-lighten-3 px-2 py-2 m-0">               
            <div class="card-header border-0">
                <div class="text-center mb-1">
                    <img src="{{ asset('app-assets/images/logo/logo.png') }}" alt="branding logo">
                </div>
                <div class="font-large-1  text-center">
                    Activate Account
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                    <span>We sent you a link to activate your account.</span>
                </h6>
            </div>
            <div class="card-content">
                <div class="card-body">
                
                    @if(session('status.error'))
                        <div class="alert alert-outline alert-danger">
                            {{ session('status.error') }}
                        </div>
                    @endif
                    <form class="form-horizontal" action="{{ url('/activate') }}" method="post">
                        
                        {{ csrf_field() }}
                        
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="text" class="form-control round" placeholder="Your Activation Code" required name="code" value="{{ old('code') }}" autofocus>
                            @if ($errors->has('code'))
                                <span class="help-block text-danger">
                                    {{ $errors->first('code') }}
                                </span>
                            @endif
                        </fieldset>                            
                        <div class="form-group text-center">
                                <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
            <div class="card-footer border-0 p-0">
                <p class="float-sm-center text-center">Not a member ?
                    <a href="{{ url('/register') }}" class="card-link">Sign Up</a>
                </p>
            </div>
        </div>
    </div>

    
    
@endsection

@section('footer')
@endsection