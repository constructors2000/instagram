<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Tubeans : @yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
        <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/switchery.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/switch.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-switch.css') }}">
        <!-- END VENDOR CSS-->
        
        <!-- BEGIN CHAMELEON  CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
        <!-- END CHAMELEON  CSS-->
        
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
        <!-- END Page Level CSS-->
        
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
        <!-- END Custom CSS-->

        @yield('header')
    </head>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
        
        <form id="logout-form" action="{{ url('logout') }}" method="post" style="display: none;">
            {{ csrf_field() }}
        </form>

        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
            <div class="navbar-wrapper">
                <div class="navbar-container content">
                    <div class="collapse navbar-collapse show" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <li class="dropdown dropdown-language nav-item">
                                <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language"></span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                                    <div class="arrow_box"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> Chinese</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-ru"></i> Russian</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-es"></i> Spanish</a></div>
                                </div>
                            </li>
                            <li class="dropdown dropdown-notification nav-item">
                                <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                    <i class="ficon ft-bell bell-@if(Auth::user()->unread_count() > 0)shake @endif" id="notification-navbar-link"></i>
                                    @if(Auth::user()->unread_count() > 0)
                                        <span class="badge badge-pill badge-sm badge-danger badge-default badge-up badge-glow">
                                            {{ Auth::user()->unread_count() }}
                                        </span>
                                    @endif
                                </a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <div class="arrow_box_right">
                                        <li class="dropdown-menu-header">
                                            <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6>
                                        </li>
                                        <li class="scrollable-container media-list w-100">
                                            @foreach(Auth::user()->notifications() as $notification)
                                                <div class="media">
                                                    
                                                    <div class="media-body">
                                                        <h6 class="media-heading info">{{ $notification->text_title }}</h6>
                                                        <p class="notification-text font-small-3 text-muted text-bold-600">
                                                            {{ $notification->text_content }}
                                                        </p>
                                                        <small>
                                                        <time class="media-meta text-muted unix_time" data-value="{{ $notification->created_at }}">{{ $notification->created_at }}</time></small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </li>
                                        <li class="dropdown-menu-footer">
                                            <a class="dropdown-item info text-right pr-1" href="{{ url('user/markReadAll') }}">Read all</a>
                                        </li>
                                    </div>
                                </ul>
                            </li>
                            <!-- <li class="dropdown dropdown-user nav-item">
                                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">             <span class="avatar avatar-online"><img src="{{ asset('app-assets/images/portrait/small/avatar-s-19.png') }}" alt="avatar"></span></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="arrow_box_right">
                                        <a class="dropdown-item" href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ft-power"></i> Logout</a>
                                    </div>
                                </div>
                            </li> -->
                            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">             <span class="avatar avatar-online"><img src="{{ asset('app-assets/images/portrait/small/avatar-s-19.png') }}" alt="avatar"></span></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="arrow_box_right">
                                        <a class="dropdown-item" href="#"><span class="avatar avatar-online">
                                        <img src="{{ asset('app-assets/images/portrait/small/avatar-s-19.png') }}" alt="avatar"><span class="user-name text-bold-700 ml-1">John Doe</span></span></a>
                                        <div class="dropdown-divider">
                                        </div>
                                        <a class="dropdown-item" href="{{url('user/accounts/edit')}}"><i class="ft-user"></i> Edit Profile</a>
                                        <!-- <a class="dropdown-item" href="email-application.html"><i class="ft-mail"></i> My Inbox</a>
                                        <a class="dropdown-item" href="project-summary.html"><i class="ft-check-square"></i> Task</a>
                                        <a class="dropdown-item" href="chat-application.html"><i class="ft-message-square"></i> Chats</a> -->
                                        <div class="dropdown-divider">
                                        </div>
                                        <a class="dropdown-item" href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ft-power"></i> Logout</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="{{ asset('app-assets/images/backgrounds/02.jpg') }}">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto">
                        <a class="navbar-brand" href="{{ url('/user') }}">
                            <img class="brand-logo" alt="Chameleon" src="{{ asset('app-assets/images/logo/logo.png') }}"/>
                            <h3 class="brand-text">Tubeans</h3>
                        </a>
                    </li>
                    <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
                </ul>
            </div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" nav-item">
                        <a href="{{ url('/user/') }}">
                            <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/accounts') }}">
                            <i class="ft-users"></i><span class="menu-title" data-i18n="">Accounts</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/payment') }}">
                            <i class="ft-shopping-cart"></i><span class="menu-title" data-i18n="">Payment</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/settings') }}">
                            <i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/support') }}">
                            <i class="ft-thumbs-up"></i><span class="menu-title" data-i18n="">Support</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navigation-background"></div>
        </div>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-wrapper-before"></div>
                
                @yield('content')
            
            </div>
        </div>
        <footer class="footer footer-static footer-light navbar-border navbar-shadow">
            <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
                <span class="float-md-left d-block d-md-inline-block">2018  &copy; Copyright <a class="text-bold-800 grey darken-2" href="#" target="_blank">Tubeans</a></span>
            </div>
        </footer>
       
        <!-- BEGIN VENDOR JS-->
        <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/js/scripts/forms/switch.min.js') }}" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
       
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
       
        <!-- BEGIN CHAMELEON  JS-->
        <script src="{{ asset('app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/vendors/js/jquery.sharrre.js') }}" type="text/javascript"></script>

        <script>
            $(".unix_time").each(function(){
                var _val = $(this).attr("data-value") * 1000;
                var _date = new Date(+_val);
                $(this).text(_date.toLocaleTimeString());
            });
        </script>
        <!-- END CHAMELEON  JS-->
       
        <!-- BEGIN PAGE LEVEL JS-->
        <!-- END PAGE LEVEL JS-->

        @yield('footer')
    </body>
</html>