<!doctype html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Admin :: @yield('title')</title>
    <meta name="description" content="Admin">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta property="og:title" content="Admin">
    <meta property="og:site_name" content="Admin">
    <meta property="og:description" content="Admin">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('css/admin.css') }}">
    <style>
        .content-heading{
            padding-top: 0px;
        }
    </style>
	@yield('header')
</head>

<body>
    <div id="page-container" class="side-scroll page-header-modern main-content-boxed side-trans-enabled sidebar-o">
        <nav id="sidebar">
            <div id="sidebar-scroll">
                <div class="sidebar-content">
                    <div class="content-header content-header-fullrow px-15">
                        <div class="content-header-section sidebar-mini-visible-b">
                            <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
								<span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                        </div>
                        <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times text-danger"></i>
                            </button>
                            <div class="content-header-item">
                                <a class="link-effect font-w700" href="{{ url('/admin') }}">
                                    <i class="si si-fire text-primary"></i>
                                    <span class="font-size-xl text-dual-primary-dark">Admin</span><span class="font-size-xl text-primary">Panel</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
                            <li>
                                <a href="{{ url('/admin/dashboard') }}"><i class="si si-pie-chart"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/levels') }}"><i class="si si-layers"></i><span class="sidebar-mini-hide">Levels</span></a>
                            </li>
                            <li class="has_submenu">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Users</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/admin') }}">All</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/admin/users/add') }}">Add New</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <header id="page-header">
            <div class="content-header">
                <div class="content-header-section">
                    <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                        <i class="fa fa-navicon"></i>
                    </button>
                </div>
                <div class="content-header-section">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->first_name }}
                            <i class="fa fa-angle-down ml-5"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                            <!-- <a class="dropdown-item" href="be_pages_generic_profile.php">
                                <i class="si si-user mr-5"></i> Profile
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                                <i class="si si-wrench mr-5"></i> Settings
                            </a> -->
                            <!-- <div class="dropdown-divider"></div> -->
                            <a class="dropdown-item" href="{{ url('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                <i class="si si-logout mr-5"></i> Logout
                            </a>

                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="page-header-search" class="overlay-header">
                <div class="content-header content-header-fullrow">
                    <form action="be_pages_generic_search.php" method="post">
                        <div class="input-group">
                            <span class="input-group-btn">
								<button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
									<i class="fa fa-times"></i>
								</button>
							</span>
                            <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                            <span class="input-group-btn">
								<button type="submit" class="btn btn-secondary">
									<i class="fa fa-search"></i>
								</button>
							</span>
                        </div>
                    </form>
                </div>
            </div>
            <div id="page-header-loader" class="overlay-header bg-primary">
                <div class="content-header content-header-fullrow text-center">
                    <div class="content-header-item">
                        <i class="fa fa-sun-o fa-spin text-white"></i>
                    </div>
                </div>
            </div>
        </header>
        <main id="main-container" style="min-height: 520px;">
            <div class="content">
				@yield('content')
            </div>
        </main>
        <footer id="page-footer" class="opacity-0" style="opacity: 1;">
            <div class="content py-20 font-size-xs clearfix">
                <div class="float-right">
                    Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://the360circle.com" target="_blank">the360circle</a>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/be_tables_datatables.js') }}"></script>
    <script>
        $(function(){
            var _c_url = window.location.href;
            $("ul.nav-main > li > a[href='"+_c_url+"']").addClass("active");
            // has_submenu

            $("ul.nav-main > li.has_submenu > ul > li > a[href='"+_c_url+"']").addClass("active");
            $("ul.nav-main > li.has_submenu > ul > li > a[href='"+_c_url+"']").parents("li.has_submenu").addClass("open");
            // $("a[href='"+_c_url+"']").parents("div.sub-item").parent("li.nav-item").addClass("active");
        });
    </script>
	@yield('footer')
</body>

</html>