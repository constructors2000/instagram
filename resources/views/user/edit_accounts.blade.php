@extends('_layouts.user')

@section('title','Settings')

@section('header')
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Edit Accounts</h3>
        </div>
    </div>
    @if(session('status.success'))
        <div class="alert alert-outline alert-success">
            {{ session('status.success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session('status.error'))
        <div class="alert alert-outline alert-danger">
            {{ session('status.error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="content-body">
        <!-- Border color start -->
        <section id="borderColor">
        
            <div class="row">
            @foreach($accounts as $account)
            <div class="col-md-12 card">    
                <table class="table">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Password</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                      
                            <tr>
                                <td>{{ $account->username }}</td>
                                <td>******</td>
                                <td class="tx-20">
                                <a href="" data-toggle="modal" data-target="#myModal_e{{ $account->id }}">edit</a>
                
                                </td>
                            </tr>
                       
                    </tbody>
                </table>

            </div>
            <div class="modal" id="myModal_e{{ $account->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h1><strong style="padding-left: 125px;">Edit account</strong></h1>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                            <form action="{{ url('/user/instagram/edit') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$account->id  }}"/>
                                <input class="form-control" type="text" value="{{ $account->username }}" name ="username" placeholder="username"><br>
                                <input class="form-control" type="password" name ="password" value="{{ $account->password }}" placeholder="password"><br>
                                <input class="form-control" type="text" name ="proxy" placeholder="proxy" value="{{ $account->proxy }}">
                                <br>
                                <input class="form-control btn btn-primary text-center" style="color: #fff;" type="submit" value="submit">
                                <!-- <input class="form-control btn btn-primary text-center" style="color: #fff;" type="submit" value="No"> -->
                            </form>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        
                        </div>
                        
                    </div>
                </div>
            </div>
            @endforeach
            </div>
        </section>
    </div>     
 

@endsection

@section('footer')
   
<script>
$(".alert-success").fadeTo(2000, 800).slideUp(800, function(){
            $(".alert-success").slideUp(800);
        });
</script>
@endsection