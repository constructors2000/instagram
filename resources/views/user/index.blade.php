@extends('_layouts.user')

@section('title','Settings')

@section('header')
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Dashboard</h3>
        </div>
    </div>

   <div class="content-body">
        <!-- Border color start -->
        <section id="borderColor">
            <div class="row">
                <div class="col-md-3">
                    <div class="card p-1 border-primary">
                        <div class="card-body text-center">
                            <?php
                                $total = 0;
                                foreach($accounts as $account)
                                {
                                    $total = $total + $account->instagram_following;
                                }
                            ?>
                            <strong style="font-size:30px">{{ $total }}</strong></i><br>
                            TOTAL FOLLOWING
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card p-1 border-primary">
                        <div class="card-body text-center">
                            <?php
                                $total = 0;
                                foreach($accounts as $account)
                                {
                                    $total = $total + $account->instagram_followers;
                                }
                            ?>
                            <strong style="font-size:30px">{{ $total }}</strong></i><br>
                            TOTAL FOLLOWERS
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card p-1 border-primary">
                        <div class="card-body text-center">
                            <strong style="font-size:30px">0</strong></i><br>
                            TOTAL LIKES
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card p-1 border-primary">
                        <div class="card-body text-center">
                            <strong style="font-size:30px">0</strong></i><br>
                            TOTAL COMMENTS
                        </div>
                    </div>
                </div>

            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="border-primary p-1">
                        <div class="card-body">
                            <strong>SUBSCRIPTION INFO</strong>
                            <br>
                            <br>
                            <a href="#" class="btn btn-danger">Choose Plan</a>

                            <!-- <spam>Created at: yyyy/mm/dd  </spam><br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="form-control border  btn btn-danger">Cancel Subscription</button>
                                </div>
                                <div class="col-md-6">
                                    <div class="border-primary">
                                        <div class="form-control">
                                            <strong>10 DAYS LEFT</strong>
                                        </div>
                                    </div>    
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="p-1">Instagram Account(s)</h3>
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                                <tr class="bg-primary bg-accent-2">
                                    <th></th>
                                    <th>username</th>
                                    <th>status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accounts as $account)
                                    <tr>
                                        <th scope="row"><span class="avatar avatar-online"><img src="{{ $account->instagram_pic }}" alt="avatar"></span></th>
                                        <td>{{ $account->username }}</td>
                                        <td class="text-success">
                                            Active
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><br>

            <div class="row">
                
            </div>
        </section>
        <!-- Border color end -->
        
    </div>
      


@endsection

@section('footer')
   

    <script>
        jQuery(function($) {
            $('#swap').on('click', function() {
                var $el = $(this),
                textNode = this.lastChild;
                $el.find('span').toggleClass('glyphicon-swap glyphicon-tint');
                textNode.nodeValue = ' Automation ' + ($el.hasClass('swap') ? 'On' : 'Off')
                $el.toggleClass('swap');
            });
        });
    </script>
    <script>
        function setColor(e, btn, color) {
            var target = e.target,
            count = +target.dataset.count;
            target.style.backgroundColor = count === 1 ? "#fa626b" : '#1e829b';
            target.dataset.count = count === 1 ? 0 : 1;
        }
    </script>
@endsection