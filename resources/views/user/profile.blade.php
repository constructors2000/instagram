@extends('_layouts.user')

@section('title','Settings')

@section('header')
@endsection

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Profile</h3>
        </div>
    </div>

    <div class="content-body">
        <section id="borderColor">
            <div class="row">
                <div class="col-md-7">
                    <div class="card p-1 border-primary">
                        <div class="row">
                            <div class="p-1 col-md-3">
                                <span class="avatar-online img"><img src="{{ asset('app-assets/images/portrait/medium/avatar-m-1.png') }}" alt="avatar" style="border-radius:  100%; height: 100px;width: 100%;"></span>
                            </div>
                            <div class="col-md-9 pt-2">
                                <h5><p><strong class="size">@</strong>username</p></h5>
                                <p>
                                    Lorem ipsum dolor sit, amet consectetur adipisicing. Lorem ipsum dolor sit, amet consectetur adipisicing. Lorem ipsum dolor sit, amet consectetur adipisicing.
                                </p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <h2><strong>200</strong></h2>
                                <h5><strong>Followers</strong></h5>
                            </div>
                            <div class="col-md-4 text-center">
                                <h2><strong>400</strong></h2>
                                <h5><strong>Following</strong></h5>
                            </div>
                            <div class="col-md-4 text-center">
                                <h2><strong>300</strong></h2>
                                <h5><strong>Posts</strong></h5>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
    </div>  

@endsection

@section('footer')
@endsection