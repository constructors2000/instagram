@extends('_layouts.user')

@section('title','Settings')

@section('header')

    <link rel="stylesheet" href="{{ asset('/css/tagmanager.css') }}"/>
    <link rel="stylesheet" hfre="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css"/>    

    <style>
    .swal-footer{
        text-align:center !important;
    }
    .banner {
    position: absolute;
    z-index: 9999;
    padding: .75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
    display: none;
    }

    .banner.active {
        display: block;
    }

    .banner-bottom {
        left: 0;
        right: 0;
        bottom: 10px;
    }

    .banner-top {
        left: 0;
        right: 0;
        top: 10px;
    }

    .banner-right {
        right: 10px;
        bottom: 10%;
        min-height: 10vh;
    }

    .banner-left {
        left: 10px;
        bottom: 10%;
        min-height: 10vh;
    }

    .alert-primary {
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    }

    .banner-close {
        position: absolute;
        right: 1.5%;
    }

    .banner-close:after {
        position: absolute;
        right: 0;
        top: 50%;
        content: 'X';
        color: #69f;
    }
        .fix-padding div.col-4{
            padding: 0px;
        }

        .settings_messages_values .tm-tag.tag-message{
            background-color: #6967ce;
            border-width: 0;
            color: #fff;
            font-size: 13px;
            letter-spacing: .7px;
            font-weight: 500;
            padding: 10px;
            border: 1px solid #6967ce;
        }
        .settings_messages_values .tm-tag.tag-message .tm-tag-remove{
            color: #fff;
            opacity: 0.5;
            border-radius: 0px;
        }

        .settings_comments_values .tm-tag.tag-comment{
            background-color: #6967ce;
            border-width: 0;
            color: #fff;
            font-size: 13px;
            letter-spacing: .7px;
            font-weight: 500;
            padding: 10px;
            border: 1px solid #6967ce;
        }
        .settings_comments_values .tm-tag.tag-comment .tm-tag-remove{
            color: #fff;
            opacity: 0.5;
            border-radius: 0px;
        }

        .settings_blacklist_values .tm-tag.tag-blacklist{
            background-color: #6967ce;
            border-width: 0;
            color: #fff;
            font-size: 13px;
            letter-spacing: .7px;
            font-weight: 500;
            padding: 10px;
            border: 1px solid #6967ce;
        }
        .settings_blacklist_values .tm-tag.tag-blacklist .tm-tag-remove{
            color: #fff;
            opacity: 0.5;
            border-radius: 0px;
        }

        #choose_mode_container button:hover{
            border-color: #5ed84f;
            background-color: #5ed84f;
        }

    </style>

@endsection

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Accounts</h3>
        </div>
    </div>
    @if(session('status.success'))
        <div class="alert alert-outline alert-success">
            {{ session('status.success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session('status.error'))
        <div class="alert alert-outline alert-danger">
            {{ session('status.error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="content-body">
        <!-- Border color start -->
        <section id="borderColor">
        
            <div class="row">
                
                @foreach($accounts as $account)
                    <div class="col-md-4">
                        <div class="card border-primary">
                            <img src="{{ $account['pic'] }}" class="img-fluid" style="width: 100%;"> 
                            <div style="padding:10px;">
                                <h5 class="text-center p-1">
                                    {{ $account['username'] }}
                                </h5> 
                                <div class="row">
                                    <div class="col-md-8">
                                        <strong style="font-size:17px;">Automation</strong>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <label class="switch">
                                            <input type="checkbox" name="button" value="" data-setting-insta-id="{{ $account['id'] }}" data-setting-name="status" class="js-switch change_account_settings"
                                                @if($account['enabled'])
                                                    checked
                                                    data-value="1"
                                                @else
                                                    unchecked
                                                    data-value="0"
                                                @endif
                                            />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <strong style="font-size:17px;">Comment</strong>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <label class="switch">
                                            <input type="checkbox" id=""  name="switch" data-setting-insta-id="{{ $account['id'] }}" data-setting-name="comment_by_bot" class="js-switch change_account_settings"
                                                @if($account['comment'])
                                                    checked
                                                    data-value="1"
                                                @else
                                                    unchecked
                                                    data-value="0"
                                                @endif/>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <strong style="font-size:17px;">Like</strong>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <label class="switch">
                                            <input type="checkbox" id=""  name="switch" data-setting-insta-id="{{ $account['id'] }}" data-setting-name="like_by_bot" class="js-switch change_account_settings"
                                                @if($account['like'])
                                                    checked
                                                    data-value="1"
                                                @else
                                                    unchecked
                                                    data-value="0"
                                                @endif/>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row pt-1 text-center fix-padding">
                                    <div class="col-4">
                                        <p class="primary lighten-2">Followers</p>
                                        <h6 class="text-center">{{ $account['followers'] }}</h6>
                                    </div>
                                    <div class="col-4">
                                        <p class="primary lighten-2">Posts</p> 
                                        <h6 class="text-center">{{ $account['posts'] }}</h6>         
                                    </div>
                                    <div class="col-4">
                                        <p class="primary lighten-2">Following</p>
                                        <h6 class="text-center">{{ $account['following'] }}</h6>         
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="p-1 border-primary text-center btn-square btn-primary" data-toggle="modal" data-target="#myModals_acc_{{ $account['id'] }}">
                                Setting
                            </button>
                            <button type="button" class="p-1 border-danger text-center btn-square btn-danger" data-toggle="modal" data-target="#myModal_r{{ $account['id'] }}">
                                Remove account
                            </button>

                            <div class="modal" id="myModals_acc_{{ $account['id'] }}">
                                <div class="modal-dialog" style="max-width: 670px; top: 50px;">
                                    <div class="modal-content">
                                    
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <strong class="col-md-11" style="font-size:22px;">Settings</strong>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            @if(session('status.success'))
                                                <div class="alert alert-outline alert-success">
                                                    {{ session('status.success') }}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            @if(session('status.error'))
                                                <div class="alert alert-outline alert-danger">
                                                    {{ session('status.error') }}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif

                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#m_{{ $account['id'] }}_chose_mode">Chose Mode</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#m_{{ $account['id'] }}_chose">Chose Group</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#m_{{ $account['id'] }}_setting">Setting</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#m_{{ $account['id'] }}_message">Message</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#m_{{ $account['id'] }}_comment">Comment</a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#m_{{ $account['id'] }}_blacklist">Blacklist</a>
                                                </li>
                                            </ul>
                                            
                                            <div class="tab-content">
                                                
                                                <div id="m_{{ $account['id'] }}_chose_mode" class="container tab-pane active">
                                                    <br><br>
                                                    <div class="row" id="choose_mode_container">
                                                        <button data-id="{{ $account['id'] }}" data-value="automatic"
                                                            @if($account["mode"] === "automatic")
                                                                class="btn btn-lg btn-success btn-block"
                                                            @else
                                                                class="btn btn-lg btn-danger btn-block"
                                                            @endif
                                                        >
                                                        Automatic</button>
                                                        <button data-id="{{ $account['id'] }}" data-value="semi"
                                                            @if($account["mode"] === "semi")
                                                                class="btn btn-lg btn-success btn-block"
                                                            @else
                                                                class="btn btn-lg btn-danger btn-block"
                                                            @endif
                                                        >
                                                        Semi Automatic</button>
                                                        <button data-id="{{ $account['id'] }}" data-value="manual"
                                                            @if($account["mode"] === "manual")
                                                                class="btn btn-lg btn-success btn-block"
                                                            @else
                                                                class="btn btn-lg btn-danger btn-block"
                                                            @endif
                                                        >
                                                        Manual</button>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12" style="padding: 0px;">
                                                            <ul class="pager pager-round">
                                                                <li class="previous disabled">
                                                                    
                                                                </li>
                                                                <li class="next">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_chose">Next <i class="ft-arrow-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="m_{{ $account['id'] }}_chose" class="container tab-pane">
                                                    <br><br>
                                                    <a class="btn btn-outline-primary" href="{{ url('user/refresh_group_list') }}?id={{ $account['id'] }}">
                                                        <i class="fa fa-refresh"></i>
                                                        Refresh
                                                    </a>
                                                    <br><br>
                                                    <div class="scroll" id="choose_groups_container">
                                                        
                                                        @foreach($account['groups'] as $group)
<!-- 
                                                            <div
                                                                data-id="{{ $account['id'] }}" data-message-group-id="{{ $group->id }}" data-status="{{ json_encode($group->status) }}"
                                                                @if($group->status)
                                                                    class="group col-md-11 mx-auto p-1 card btn-success"
                                                                @else
                                                                    class="group col-md-11 mx-auto p-1 card btn-danger"
                                                                @endif
                                                            >
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <i class="fa fa-users"></i>
                                                                        <strong class="size">{{ $group->group_name }}</strong>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <button data-id="{{ $account['id'] }}" data-message-group-id="{{ $group->id }}" data-status="{{ json_encode($group->status) }}"
                                                            @if($group->status)
                                                                class="btn btn-lg btn-success btn-block"
                                                            @else
                                                                class="btn btn-lg btn-danger btn-block"
                                                            @endif
                                                        >
                                                        {{ $group->group_name }}</button>

                                                        @endforeach

                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12" style="padding: 0px;">
                                                            <ul class="pager pager-round">
                                                                <li class="previous">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_chose_mode"><i class="ft-arrow-left"></i> Previous</a>
                                                                </li>
                                                                <li class="next">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_setting">Next <i class="ft-arrow-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                <div id="m_{{ $account['id'] }}_setting" class="container tab-pane">
                                                    <br>
                                                    <form action="{{ url('/user/accounts/settings/update') }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="instagram_id" value="{{ $account['id'] }}"/>

                                                        <div class="row">

                                                            <div name="prev_post" class="col-md-12"  @if($account['mode'] == "automatic") style="display:none"  @endif>
                                                                <strong>Count Previous Posts</strong>
                                                                <!-- <input type="number" min="1" class="form-control" name="settings_count" placeholder="" value="{{ $account['count'] }}"
                                                                @if($account['mode'] == "automatic")
                                                                    readonly
                                                                @endif
                                                                > -->
                                                                <input type="number" min="1" class="form-control" name="settings_count" placeholder="" value="{{ $account['count'] }}"
                                                               
                                                                >
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                       
                                                            <div class="col-md-6" name ="msg_after" style="@if($account['mode'] == "automatic" ||$account['mode'] == "semi")display:block @else display:none @endif">
                                                                <strong>Send Message After (Minutes) of Upload</strong>
                                                                <input type="number" id="settings_time_interval_{{ $account['id'] }}" name="settings_time" class="form-control" value=""
                                                               
                                                                >
                                                            </div> 
                                                        
                                                            <!-- <div class="col-md-6" name="msg_time" style="@if($account['mode'] == "automatic" ||$account['mode'] == "semi")display:none @else display:block @endif">
                                                            
                                                                <strong >Send Message After</strong>
                                                                <input type="time" class="form-control" name="settings_time" placeholder="" value="{{ $account['time'] }}"
                                                                @if($account['mode'] == "automatic")
                                                                    readonly
                                                                @endif
                                                                >
                                                            </div> -->
                                                            <div class="col-md-6">
                                                                <strong>Time Interval (in Seconds)</strong>
                                                                <!-- <input type="number" id="settings_time_interval_{{ $account['id'] }}" name="settings_time_interval" class="form-control" value="{{ $account['interval'] }}"
                                                                @if($account['mode'] == "automatic")
                                                                    readonly
                                                                @endif
                                                                > -->
                                                                <input type="number" id="settings_time_interval_{{ $account['id'] }}" name="settings_time_interval" class="form-control" value="{{ $account['interval'] }}"
                                                              
                                                                >
                                                            </div> 
                                                        </div>

                                                        <br>
                                                        <strong>Per Day (Max)</strong>
                                                        <br>
                                                        <hr>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <strong>Messages</strong>
                                                                <!-- <input type="number" class="form-control" name="settings_max_messages" placeholder="Per Day" value="{{ $account['max_messages'] }}"
                                                                @if($account['mode'] == "automatic" || $account['mode'] == "semi")
                                                                    readonly
                                                                @endif
                                                                > -->
                                                                <input type="number" class="form-control" name="settings_max_messages" placeholder="Per Day" value="{{ $account['max_messages'] }}"
                                                              
                                                                >
                                                            </div>                                        
                                                            <div class="col-md-4">
                                                                <strong>Likes</strong>
                                                                <!-- <input type="number" class="form-control" name="settings_max_likes" placeholder="Per Day" value="{{ $account['max_likes'] }}"
                                                                @if($account['mode'] == "automatic" || $account['mode'] == "semi")
                                                                    readonly
                                                                @endif
                                                                > -->
                                                                <input type="number" class="form-control" name="settings_max_likes" placeholder="Per Day" value="{{ $account['max_likes'] }}"
                                                               
                                                                >
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <strong>Comment</strong>
                                                                <!-- <input type="number" class="form-control" name="settings_max_comments" placeholder="Per Day" value="{{ $account['max_comments'] }}"
                                                                @if($account['mode'] == "automatic" || $account['mode'] == "semi")
                                                                    readonly
                                                                @endif
                                                                > -->
                                                                <input type="number" class="form-control" name="settings_max_comments" placeholder="Per Day" value="{{ $account['max_comments'] }}"
                                                               
                                                                >
                                                            </div>                                        
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-9"></div>
                                                            <div class="col-md-12 mt-1">
                                                                <strong>Proxy</strong>
                                                                <input type="texr" class="form-control" name="settings_proxy" placeholder="proxy" value="{{ $account['proxy'] }}">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-9"></div>
                                                            <div class="col-md-3 mt-1">
                                                                <input class="form-control btn btn-info btn-block" style="color: #fff;" type="submit" value="save">
                                                            </div>
                                                        </div>
                                                    </form>    


                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12" style="padding: 0px;">
                                                            <ul class="pager pager-round">
                                                                <li class="previous">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_chose"><i class="ft-arrow-left"></i> Previous</a>
                                                                </li>
                                                                <li class="next">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_message">Next <i class="ft-arrow-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="m_{{ $account['id'] }}_message" class="container tab-pane fade"><br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input id="settings_messages_account_{{ $account['id'] }}" type="text" class="settings_messages form-control" placeholder="Add your Message and Hit enter to save">
                                                            <br>
                                                            <div id="settings_messages_account_{{ $account['id'] }}_values" class="settings_messages_values"></div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12" style="padding: 0px;">
                                                            <ul class="pager pager-round">
                                                                <li class="previous">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_setting"><i class="ft-arrow-left"></i> Previous</a>
                                                                </li>
                                                                <li class="next">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_comment">Next <i class="ft-arrow-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="m_{{ $account['id'] }}_comment" class="container tab-pane fade"><br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input id="settings_comments_account_{{ $account['id'] }}" type="text" class="settings_comments form-control" placeholder="Add your Comment and Hit enter to save">
                                                            <br>
                                                            <div id="settings_comments_account_{{ $account['id'] }}_values" class="settings_comments_values"></div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12" style="padding: 0px;">
                                                            <ul class="pager pager-round">
                                                                <li class="previous">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_message"><i class="ft-arrow-left"></i> Previous</a>
                                                                </li>
                                                                <li class="next">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_blacklist">Next <i class="ft-arrow-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="m_{{ $account['id'] }}_blacklist" class="container tab-pane fade"><br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input id="settings_blacklist_account_{{ $account['id'] }}" type="text" class="settings_blacklist form-control" placeholder="Add Account and Hit enter to save">
                                                            <br>
                                                            <div id="settings_blacklist_account_{{ $account['id'] }}_values" class="settings_blacklist_values"></div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12" style="padding: 0px;">
                                                            <ul class="pager pager-round">
                                                                <li class="previous">
                                                                    <a class="paginate_click" href="#" data-action="#m_{{ $account['id'] }}_comment"><i class="ft-arrow-left"></i> Previous</a>
                                                                </li>
                                                                <li class="next">
                                                                    <a href="#" data-dismiss="modal">Done <i class="ft-close"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="modal" id="myModal_r{{ $account['id'] }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h1><strong style="padding-left: 125px;">Remove account</strong></h1>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        
                                        <!-- Modal body -->
                                        <div class="modal-body">

                                            <form action="{{ url('/user/instagram/delete') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{$account['id']  }}"/>
                                                <div class="position-relative has-icon-left">
                                                    <div class="form-control-position">
                                                        <i class="ft-code"></i>
                                                    </div>
                                                </div>
                                                <br>
                                                <input class="form-control btn btn-primary text-center" style="color: #fff;" type="submit" value="Yes">
                                                <!-- <input class="form-control btn btn-primary text-center" style="color: #fff;" type="submit" value="No"> -->
                                            </form>
                                        </div>
                                        
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach


                <div class="col-md-4">
                    <button type="button" class="button form-control border-primary text-center pt-5 pb-5 p-2 btn" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-user-plus blue pt-5" style="font-size:100px"></i><br><br>
                        <h1 class="pb-5">Add account</h3>
                    </button>
                   
                     <!-- The Modal -->
                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h1><strong style="padding-left: 125px;">Add account</strong></h1>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">

                                    @if(session('status.error'))
                                        <div class="alert alert-danger">
                                            {{ session('status.error') }}
                                        </div>
                                    @endif

                                    <form action="{{ url('/user/instagram/add') }}" method="POST">
                                        {{ csrf_field() }}
                                        <div class="position-relative has-icon-left">
                                            <input type="text" id="timesheetinput1" class="form-control" placeholder="Instagram username"name="username" required>
                                            <div class="form-control-position">
                                                <i class="ft-user"></i>
                                            </div>
                                        </div><br>
                                        <div class="position-relative has-icon-left">
                                            <input type="password" id="timesheetinput2" class="form-control" placeholder="Instagram password" name="password" required>
                                            <div class="form-control-position">
                                                <i class="fa fa-key"></i>
                                            </div>
                                        </div><br>
                                        <div class="position-relative has-icon-left">
                                            <input type="text" id="timesheetinput3" class="form-control" placeholder="proxy" name="proxy" >
                                            <div class="form-control-position">
                                                <i class="fa fa-link"></i>
                                            </div>
                                        </div><br>
                                        <div class="position-relative has-icon-left">
                                            <input type="text" id="timesheetinput4" class="form-control" placeholder="Verification Code" name="verificationCode">
                                            <div class="form-control-position">
                                                <i class="fa fa-key"></i>
                                            </div>
                                        </div><br>
                                        <input class="form-control btn btn-primary text-center" style="color: #fff;" type="submit" value="Add Account ">
                                        
                                    </form>
                                </div>
                                
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <!-- The Modal -->
                    <div class="modal" id="myModal_v">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h1><strong style="padding-left: 125px;">Verify account</strong></h1>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">

                                    @if(session('status.info'))
                                        <div class="alert alert-info">
                                            {{ session('status.info') }}
                                        </div>
                                    @endif
                                    @if(session('status.error'))
                                        <div class="alert alert-danger">
                                            {{ session('status.error') }}
                                        </div>
                                    @endif

                                    <form action="{{ url('/user/instagram/verify') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="username" value="{{ session('username') }}"/>
                                        <input type="hidden" name="password" value="{{ session('password') }}"/>
                                        <input type="hidden" name="proxy" value="{{ session('proxy') }}"/>
                                        <div class="position-relative has-icon-left">
                                            <input type="text" class="form-control" placeholder="Verification Code" name="code" required>
                                            <div class="form-control-position">
                                                <i class="ft-code"></i>
                                            </div>
                                        </div>
                                        <br>
                                        <input class="form-control btn btn-primary text-center" style="color: #fff;" type="submit" value="Submit Code">
                                    </form>
                                </div>
                                
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    </div>

@endsection

@section('footer')

    <script src="{{ asset('/js/tagmanager.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.btnNext').click(function() {
                $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
            });
        });
    </script>

    
    <script>
        $(document).ready(function(){
            // $("input:checkbox").change(function() {
            $('input[name=onoffswitch]').click(function(){
                
            var group_status=$(this).attr('id');
            // var group_status = $(this).val();
            // if(group_status == false) {
            //     group_status = true; 
            // } else {
            //     group_status = false; 
            // }
            // alert(group_status);
            
            var user_id = $(this).closest('tr').attr('id');
            $.ajax({
                    type:'POST',
                    url:'/post_group',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    data:{ "group_status" : group_status }
                });
            })
        });
    // });
    </script>

    <script>
        var _url_params = new URLSearchParams(window.location.search);

        if(_url_params.get('error') == "1")
        {
            $("#myModal").modal('show');
        }

        if(_url_params.get('verify') == "1")
        {
            $("#myModal_v").modal('show');
        }

        if(_url_params.has('accounts'))
        {
            $("#myModals_acc_"+_url_params.get('accounts')).modal('show');
        }
    </script>

    @foreach($accounts as $account)
        <script>
            $("#settings_messages_account_{{ $account['id'] }}").tagsManager({
                tagClass: 'tag-message',
                tagCloseIcon: 'x',
                deleteTagsOnBackspace: false,

                prefilled: [
                    @if($account['messages'] != null)
                        @foreach(explode(",",$account['messages']) as $item)
                            "{{ $item }}",
                        @endforeach
                    @endif
                ],
                AjaxPush: "{{ url('/user/accounts/settings/messages') }}",
                AjaxPushParameters: {
                    "id": "{{ $account['id'] }}",
                    "_token": "{{ csrf_token() }}"
                },
                AjaxPushAllTags: true,
                fillInputOnTagRemove: false,
                tagsContainer: "#settings_messages_account_{{ $account['id'] }}_values"
            });        
        </script>

        <script>
            $("#settings_comments_account_{{ $account['id'] }}").tagsManager({
                tagClass: 'tag-comment',
                tagCloseIcon: 'x',
                deleteTagsOnBackspace: false,

                prefilled: [
                    @if($account['comments'] != null)
                        @foreach(explode(",",$account['comments']) as $item)
                            "{{ $item }}",
                        @endforeach
                    @endif
                ],
                AjaxPush: "{{ url('/user/accounts/settings/comments') }}",
                AjaxPushParameters: {
                    "id": "{{ $account['id'] }}",
                    "_token": "{{ csrf_token() }}"
                },
                AjaxPushAllTags: true,
                fillInputOnTagRemove: false,
                tagsContainer: "#settings_comments_account_{{ $account['id'] }}_values"
            });        
        </script>

        <script>
            $("#settings_blacklist_account_{{ $account['id'] }}").tagsManager({
                tagClass: 'tag-blacklist',
                tagCloseIcon: 'x',
                deleteTagsOnBackspace: false,

                prefilled: [
                    @if($account['blacklist'] != null)
                        @foreach(explode(",",$account['blacklist']) as $item)
                            "{{ $item }}",
                        @endforeach
                    @endif
                ],
                AjaxPush: "{{ url('/user/accounts/settings/blacklist') }}",
                AjaxPushParameters: {
                    "id": "{{ $account['id'] }}",
                    "_token": "{{ csrf_token() }}"
                },
                AjaxPushAllTags: true,
                fillInputOnTagRemove: false,
                tagsContainer: "#settings_blacklist_account_{{ $account['id'] }}_values"
            });        
        </script>
    @endforeach

    <script>
        // $(".change_group_settings").on("change", function(){
            $("#choose_groups_container button").on("click", function(){
            var _id = $(this).attr("data-message-group-id");
            var _status = $(this).attr('data-status');
            // alert($(this).attr('class'));
            // alert(_status);
			if(_status == 0)
			{
				_status = "false";
			}
			if(_status == 1)
			{
				_status = "true";
			}
				
            if(_status == "false")
            {   
                // alert("set true");
                $(this).removeClass("btn btn-lg btn-block btn-danger");
                $(this).addClass("btn btn-lg btn-block btn-success");
                //$(this).removeClass("btn-success").addClass("btn-danger");
                $(this).attr('data-status', "true")

            }
            if(_status == "true")
            {
                // alert("set false");
                $(this).removeClass("btn btn-lg btn-block btn-success");
                $(this).addClass("btn btn-lg btn-block btn-danger");
                $(this).attr('data-status', "false")
            }
            if(_status == "true")
            {
                _status = false;
            }
            if(_status == "false")
            {
                _status = true;
            }

            // alert(_status);
        
            $.post("{{ url('/user/accounts/settings/groups') }}", {
                _token: "{{ csrf_token() }}",
                id: _id,
                status: _status
            }).done(function(data){
                swal("Saved mode");
            });
        });

        $(".change_account_settings").on("change", function(){
            var _id = $(this).attr("data-setting-name");
            var _insta_id = $(this).attr("data-setting-insta-id");

            var _val = $('input.change_account_settings[data-setting-name='+_id+']').attr('data-value');

            if(_val == "0")
            {
                $(this).prop("checked", 1);
                $.post("{{ url('/user/accounts/settings/general') }}", {
                    _token: "{{ csrf_token() }}",
                    insta_id: _insta_id,
                    name: _id,
                    value: true
                }).done(function(data){
                    $('input.change_account_settings[data-setting-name='+_id+']').attr('data-value', "1");
                    swal("saved");
                });
            }
            else
            {
                $(this).prop("checked", 0);
                $.post("{{ url('/user/accounts/settings/general') }}", {
                    _token: "{{ csrf_token() }}",
                    insta_id: _insta_id,
                    name: _id,
                    value: false
                }).done(function(data){
                    $('input.change_account_settings[data-setting-name='+_id+']').attr('data-value', "0");
                    swal("Saved");
                });
            }
        });
    </script>

    <script>
        $("#choose_mode_container button").on("click", function(){
            var _id = $(this).attr('data-id');
            var _val = $(this).attr('data-value');
            
            $("#choose_mode_container button").each(function(){
                $(this).removeClass("btn-success").addClass("btn-danger");
            })

            $(this).removeClass("btn-danger");
            $(this).addClass("btn-success");

            if(_val == "automatic")
            {
                // read only all of below elements
                $("#myModals_acc_"+_id+" div[name=msg_time]").css("display", "none");
                $("#myModals_acc_"+_id+" div[name=prev_post]").css("display", "none");
                $("#myModals_acc_"+_id+" div[name=msg_after]").css("display", "block");
                
                //$("#myModals_acc_"+_id+" input[name=settings_time]").css("display", "none");
                // $("#myModals_acc_"+_id+" input[name=settings_time_interval]").prop("readonly", true);
                // $("#myModals_acc_"+_id+" input[name=settings_max_messages]").prop("readonly", true);
                // $("#myModals_acc_"+_id+" input[name=settings_max_likes]").prop("readonly", true);
                // $("#myModals_acc_"+_id+" input[name=settings_max_comments]").prop("readonly", true);
            }
            if(_val == "semi")
            {
                // read only elements below
                $("#myModals_acc_"+_id+" div[name=msg_time]").css("display", "none");
                $("#myModals_acc_"+_id+" div[name=msg_after]").css("display", "block");
                
                $("#myModals_acc_"+_id+" div[name=prev_post]").css("display", "block");
                // $("#myModals_acc_"+_id+" input[name=settings_time]").prop("readonly", false);
                // $("#myModals_acc_"+_id+" input[name=settings_time_interval]").prop("readonly", false);
                // // and un read below items
                // $("#myModals_acc_"+_id+" input[name=settings_max_messages]").prop("readonly", true);
                // $("#myModals_acc_"+_id+" input[name=settings_max_likes]").prop("readonly", true);
                // $("#myModals_acc_"+_id+" input[name=settings_max_comments]").prop("readonly", true);
            }
            if(_val == "manual")
            {
                // un read only all of them
                $("#myModals_acc_"+_id+" div[name=msg_time]").css("display", "block");
                $("#myModals_acc_"+_id+" div[name=msg_after]").css("display", "none");
                $("#myModals_acc_"+_id+" div[name=prev_post]").css("display", "block");
                $("#myModals_acc_"+_id+" input[name=settings_time]").prop("readonly", false);
                $("#myModals_acc_"+_id+" input[name=settings_time_interval]").prop("readonly", false);
                $("#myModals_acc_"+_id+" input[name=settings_max_messages]").prop("readonly", false);
                $("#myModals_acc_"+_id+" input[name=settings_max_likes]").prop("readonly", false);
                $("#myModals_acc_"+_id+" input[name=settings_max_comments]").prop("readonly", false);
            }


            
            $.post("{{ url('/user/accounts/settings/general_mode') }}", {
                _token: "{{ csrf_token() }}",
                insta_id: _id,
                name: "mode",
                value: _val
            }).done(function(data){
             
                swal("Saved mode");
                   
            });
        });

        // $("#choose_groups_container .group").on("click", function(){
        //     var _id = $(this).attr("data-id");
        //     var _msg_grp_id = $(this).attr("data-message-group-id");
        //     var _status = $(this).attr("data-status");

        //     if(_status == "false")
        //     {
        //         _status = "true";
        //     }
        //     else
        //     {
        //         _status = "false";
        //     }

        //     $(this).attr("data-status", _status);

        //     var _this = $(this);
        //     if(_status == "true")
        //     {
        //         _this.removeClass("btn-danger").addClass("btn-success");
        //     }
        //     else
        //     {
        //         _this.removeClass("btn-success").addClass("btn-danger");
        //     }

        //     $.post("{{ url('/user/accounts/settings/groups') }}", {
        //         _token: "{{ csrf_token() }}",
        //         id: _msg_grp_id,
        //         status: _status
        //     }).done(function(data){
        //         swal("Saved");
        //     });

        //     // alert(_id + " : " + _msg_grp_id + " : " + _status)
        // });

        $(".paginate_click").on("click", function(e){
            e.preventDefault();
            var _action = $(this).attr("data-action");
            // $('.nav-tabs a[href="#m_1_chose"]').tab('show');
            // $("#m_1_chose").tab('show');

            $('.nav-tabs a[href="'+_action+'"]').tab('show');
        });
        $(".alert-success").fadeTo(2000, 800).slideUp(800, function(){
            $(".alert-success").slideUp(800);
        });

    </script>
 
@endsection

