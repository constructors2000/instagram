@extends('_layouts.user')

@section('title','Settings')

@section('header')
@endsection

@section('content')


    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Settings</h3>
        </div>
    </div>
    @if(session('status.success'))
    <div class="alert alert-outline alert-success">
        {{ session('status.success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(session('status.error'))
    <div class="alert alert-outline alert-danger">
        {{ session('status.error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
    <div class="content-body">
        <!-- Border color start -->
        <section id="borderColor">
            <div class="row">
                <div class="col-md-5">
                    
                    <div class="card p-1 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                               
                                <form action="{{ url('/user/accounts/settings/timezone') }}" method="post">
                                    {{ csrf_field() }}
                                    <h4 class="card-title"><strong>TIMEZONE SETTINGS</strong></h4>
                                    <p class="card-text">
                                        Set your prefered timezone, based on the timezone all your request will be processed.
                                    </p>
                                    <br>
                                    <select id="user_timezone" class="form-control"  name="timezone">
                                        <option value="Pacific/Midway">UTC-11:00 - Pacific/Midway</option>
                                        <option value="Pacific/Samoa">UTC-11:00 - Pacific/Samoa</option>
                                        <option value="Pacific/Honolulu">UTC-10:00 - Pacific/Honolulu</option>
                                        <option value="US/Alaska">UTC-09:00 - US/Alaska</option>
                                        <option value="America/Los_Angeles">UTC-08:00 - America/Los_Angeles</option>
                                        <option value="America/Tijuana">UTC-08:00 - America/Tijuana</option>
                                        <option value="US/Arizona">UTC-07:00 - US/Arizona</option>
                                        <option value="America/Chihuahua">UTC-07:00 - America/Chihuahua</option>
                                        <option value="America/Mazatlan">UTC-07:00 - America/Mazatlan</option>
                                        <option value="US/Mountain">UTC-07:00 - US/Mountain</option>
                                        <option value="America/Managua">UTC-06:00 - America/Managua</option>
                                        <option value="US/Central">UTC-06:00 - US/Central</option>
                                        <option value="America/Mexico_City">UTC-06:00 - America/Mexico_City</option>
                                        <option value="America/Mexico_City">UTC-06:00 - America/Mexico_City</option>
                                        <option value="America/Monterrey">UTC-06:00 - America/Monterrey</option>
                                        <option value="Canada/Saskatchewan">UTC-06:00 - Canada/Saskatchewan</option>
                                        <option value="America/Bogota">UTC-05:00 - America/Bogota</option>
                                        <option value="US/Eastern">UTC-05:00 - US/Eastern</option>
                                        <option value="US/East-Indiana">UTC-05:00 - US/East-Indiana</option>
                                        <option value="America/Lima">UTC-05:00 - America/Lima</option>
                                        <option value="America/Bogota">UTC-05:00 - America/Bogota</option>
                                        <option value="America/Bogota">UTC-04:00 - Canada/Atlantic</option>
                                        <option value="America/Caracas">UTC-04:30 - America/Caracas</option>
                                        <option value="America/La_Paz">UTC-04:00 - America/La_Paz</option>
                                        <option value="America/Santiago">UTC-04:00 - America/Santiago</option>
                                        <option value="Canada/Newfoundland">UTC-03:30 - Canada/Newfoundland</option>
                                        <option value="America/Sao_Paulo">UTC-03:00 - America/Sao_Paulo</option>
                                        <option value="America/Argentina/Buenos_Aires">UTC-03:00 - America/Argentina/Buenos_Aires</option>
                                        <option value="America/Godthab">UTC-03:00 - America/Godthab</option>
                                        <option value="America/Noronha">UTC-02:00 - America/Noronha</option>
                                        <option value="Atlantic/Azores">UTC-01:00 - Atlantic/Azores</option>
                                        <option value="Atlantic/Cape_Verde">UTC-01:00 - Atlantic/Cape_Verde</option>
                                        <option value="Africa/Casablanca">UTC+00:00 - Africa/Casablanca</option>
                                        <option value="Europe/London">UTC+00:00 - Europe/London</option>
                                        <option value="Etc/Greenwich">UTC+00:00 - Etc/Greenwich</option>
                                        <option value="Europe/Lisbon">UTC+00:00 - Europe/Lisbon</option>
                                        <option value="Europe/London">UTC+00:00 - Europe/London</option>
                                        <option value="Africa/Monrovia">UTC+00:00 - Africa/Monrovia</option>
                                        <option value="UTC">UTC+00:00 - UTC</option>
                                        <option value="Europe/Amsterdam">UTC+01:00 - Europe/Amsterdam</option>
                                        <option value="Europe/Belgrade">UTC+01:00 - Europe/Belgrade</option>
                                        <option value="Europe/Berlin">UTC+01:00 - Europe/Berlin</option>
                                        <option value="Europe/Bratislava">UTC+01:00 - Europe/Bratislava</option>
                                        <option value="Europe/Brussels">UTC+01:00 - Europe/Brussels</option>
                                        <option value="Europe/Budapest">UTC+01:00 - Europe/Budapest</option>
                                        <option value="Europe/Copenhagen">UTC+01:00 - Europe/Copenhagen</option>
                                        <option value="Europe/Ljubljana">UTC+01:00 - Europe/Ljubljana</option>
                                        <option value="Europe/Madrid">UTC+01:00 - Europe/Madrid</option>
                                        <option value="Europe/Paris">UTC+01:00 - Europe/Paris</option>
                                        <option value="Europe/Prague">UTC+01:00 - Europe/Prague</option>
                                        <option value="Europe/Rome">UTC+01:00 - Europe/Rome</option>
                                        <option value="Europe/Sarajevo">UTC+01:00 - Europe/Sarajevo</option>
                                        <option value="Europe/Skopje">UTC+01:00 - Europe/Skopje</option>
                                        <option value="Europe/Stockholm">UTC+01:00 - Europe/Stockholm</option>
                                        <option value="Europe/Vienna">UTC+01:00 - Europe/Vienna</option>
                                        <option value="Europe/Warsaw">UTC+01:00 - Europe/Warsaw</option>
                                        <option value="Africa/Lagos">UTC+01:00 - Africa/Lagos</option>
                                        <option value="Europe/Zagreb">UTC+01:00 - Europe/Zagreb</option>
                                        <option value="Europe/Athens">UTC+02:00 - Europe/Athens</option>
                                        <option value="Europe/Bucharest">UTC+02:00 - Europe/Bucharest</option>
                                        <option value="Africa/Cairo">UTC+02:00 - Africa/Cairo</option>
                                        <option value="Africa/Harare">UTC+02:00 - Africa/Harare</option>
                                        <option value="Europe/Helsinki">UTC+02:00 - Europe/Helsinki</option>
                                        <option value="Europe/Istanbul">UTC+02:00 - Europe/Istanbul</option>
                                        <option value="Asia/Jerusalem">UTC+02:00 - Asia/Jerusalem</option>
                                        <option value="Europe/Helsinki">UTC+02:00 - Europe/Helsinki</option>
                                        <option value="Africa/Johannesburg">UTC+02:00 - Africa/Johannesburg</option>
                                        <option value="Europe/Riga">UTC+02:00 - Europe/Riga</option>
                                        <option value="Europe/Sofia">UTC+02:00 - Europe/Sofia</option>
                                        <option value="Europe/Tallinn">UTC+02:00 - Europe/Tallinn</option>
                                        <option value="Asia/Baghdad">UTC+03:00 - Asia/Baghdad</option>
                                        <option value="Asia/Kuwait">UTC+03:00 - Asia/Kuwait</option>
                                        <option value="Europe/Minsk">UTC+03:00 - Europe/Minsk</option>
                                        <option value="Africa/Nairobi">UTC+03:00 - Africa/Nairobi</option>
                                        <option value="Asia/Riyadh">UTC+03:00 - Asia/Riyadh</option>
                                        <option value="Europe/Volgograd">UTC+03:00 - Europe/Volgograd</option>
                                        <option value="Asia/Tehran">UTC+03:30 - Asia/Tehran</option>
                                        <option value="Asia/Muscat">UTC+04:00 - Asia/Muscat</option>
                                        <option value="Asia/Baku">UTC+04:00 - Asia/Baku</option>
                                        <option value="Europe/Moscow">UTC+04:00 - Europe/Moscow</option>
                                        <option value="Asia/Muscat">UTC+04:00 - Asia/Muscat</option>
                                        <option value="Europe/Moscow">UTC+04:00 - Europe/Moscow</option>
                                        <option value="Asia/Tbilisi">UTC+04:00 - Asia/Tbilisi</option>
                                        <option value="Asia/Yerevan">UTC+04:00 - Asia/Yerevan</option>
                                        <option value="Asia/Kabul">UTC+04:30 - Asia/Kabul</option>
                                        <option value="Asia/Karachi">UTC+05:00 - Asia/Karachi</option>
                                        <option value="Asia/Tashkent">UTC+05:00 - Asia/Tashkent</option>
                                        <option value="Asia/Kolkata">UTC+05:30 - Asia/Kolkata</option>
                                        <option value="Asia/Katmandu">UTC+05:45 - Asia/Katmandu</option>
                                        <option value="Asia/Almaty">UTC+06:00 - Asia/Almaty</option>
                                        <option value="Asia/Dhaka">UTC+06:00 - Asia/Dhaka</option>
                                        <option value="Asia/Yekaterinburg">UTC+06:00 - Asia/Yekaterinburg</option>
                                        <option value="Asia/Rangoon">UTC+06:30 - Asia/Rangoon</option>
                                        <option value="Asia/Bangkok">UTC+07:00 - Asia/Bangkok</option>
                                        <option value="Asia/Jakarta">UTC+07:00 - Asia/Jakarta</option>
                                        <option value="Asia/Novosibirsk">UTC+07:00 - Asia/Novosibirsk</option>
                                        <option value="Asia/Hong_Kong">UTC+08:00 - Asia/Hong_Kong</option>
                                        <option value="Asia/Chongqing">UTC+08:00 - Asia/Chongqing</option>
                                        <option value="Asia/Krasnoyarsk">UTC+08:00 - Asia/Krasnoyarsk</option>
                                        <option value="Asia/Kuala_Lumpur">UTC+08:00 - Asia/Kuala_Lumpur</option>
                                        <option value="Australia/Perth">UTC+08:00 - Australia/Perth</option>
                                        <option value="Asia/Singapore">UTC+08:00 - Asia/Singapore</option>
                                        <option value="Asia/Taipei">UTC+08:00 - Asia/Taipei</option>
                                        <option value="Asia/Ulan_Bator">UTC+08:00 - Asia/Ulan_Bator</option>
                                        <option value="Asia/Urumqi">UTC+08:00 - Asia/Urumqi</option>
                                        <option value="Asia/Irkutsk">UTC+09:00 - Asia/Irkutsk</option>
                                        <option value="Asia/Tokyo">UTC+09:00 - Asia/Tokyo</option>
                                        <option value="Asia/Seoul">UTC+09:00 - Asia/Seoul</option>
                                        <option value="Australia/Adelaide">UTC+09:30 - Australia/Adelaide</option>
                                        <option value="Australia/Darwin">UTC+09:30 - Australia/Darwin</option>
                                        <option value="Australia/Brisbane">UTC+10:00 - Australia/Brisbane</option>
                                        <option value="Australia/Canberra">UTC+10:00 - Australia/Canberra</option>
                                        <option value="Pacific/Guam">UTC+10:00 - Pacific/Guam</option>
                                        <option value="Australia/Hobart">UTC+10:00 - Australia/Hobart</option>
                                        <option value="Australia/Melbourne">UTC+10:00 - Australia/Melbourne</option>
                                        <option value="Pacific/Port_Moresby">UTC+10:00 - Pacific/Port_Moresby</option>
                                        <option value="Australia/Sydney">UTC+10:00 - Australia/Sydney</option>
                                        <option value="Asia/Yakutsk">UTC+10:00 - Asia/Yakutsk</option>
                                        <option value="Asia/Vladivostok">UTC+11:00 - Asia/Vladivostok</option>
                                        <option value="Pacific/Auckland">UTC+12:00 - Pacific/Auckland</option>
                                        <option value="Pacific/Fiji">UTC+12:00 - Pacific/Fiji</option>
                                        <option value="Pacific/Kwajalein">UTC+12:00 - Pacific/Kwajalein</option>
                                        <option value="Asia/Kamchatka">UTC+12:00 - Asia/Kamchatka</option>
                                        <option value="Asia/Magadan">UTC+12:00 - Asia/Magadan</option>
                                        <option value="Pacific/Fiji">UTC+12:00 - Pacific/Fiji</option>
                                        <option value="Asia/Magadan">UTC+12:00 - Asia/Magadan</option>
                                        <option value="Pacific/Auckland">UTC+12:00 - Pacific/Auckland</option>
                                        <option value="Pacific/Tongatapu">UTC+13:00 - Pacific/Tongatapu</option>
                                    </select>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4 btn-sm-12 offset-sm-8">
                                            <button class="btn btn-info btn-block" type="submit">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                <div class="card p-1 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <form action="{{ url('/user/accounts/settings/user_info') }}" method="post">
                                {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">
                                            email
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="email" class="form-control" placeholder="email" required name="email" value="{{ $user->email }}"/>
                                            @if ($errors->has('email'))
                                                <span class="help-block text-danger">
                                                    {{ $errors->first('email') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">
                                            password
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="password" class="form-control" placeholder="password" required name="password" value="{{ old('password') }}"minlength="2" maxlength="20"/>
                                            @if ($errors->has('password'))
                                                <span class="help-block text-danger">
                                                    {{ $errors->first('password') }}
                                                </span>
                                            @endif
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 btn-sm-12 offset-sm-6">
                                            <button class="btn btn-info btn-block" type="submit">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Border color end -->
        
    </div>
    
    
@endsection

@section('footer')
    <script>
        $("#user_timezone").val('{{ $user->timezone }}');
        $(".alert-success").fadeTo(2000, 800).slideUp(800, function(){
            $(".alert-success").slideUp(800);
        });
    </script>
@endsection