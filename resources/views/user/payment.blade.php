@extends('_layouts.user')

@section('title','Settings')

@section('header')
@endsection

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Payment</h3>
        </div>
    </div>

    <div class="content-body">
        <section id="borderColor">
            <div class="row">
                <div class="col-md-4">
                    <div class="card p-1 border-primary text-center">
                        <strong class="size">FREE TRIAL</strong><br>
                        <h1><strong class="primary">$0</strong></h1>
                        <p>per 7 days</p><br>
                        <p><i class="fa fa-user-circle"></i> <b>1</b> instagram accounts</p>
                        <p><i class="fa fa-heart red"></i><strong>Auto</strong> like</p>
                        <p> <i class="fa fa-comment purple"></i><strong>Auto</strong> comment</p>
                        <p><i class="fa fa-send black"></i><strong>Auto</strong> message</p>
                        <button class="form-control border-primary text-center  btn btn-primary" data-toggle="modal" data-target="#myModal">BUY PLAN</button>    
                    </div>
                </div>
            </div>
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                            
                            <h1><strong style="padding-left: 125px;">Add account</strong></h1>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 mx-auto ">
                                    <div class="">
                                        <ul class="nav nav-pills rounded nav-fill mb-3" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
                                                <i class="fa fa-credit-card"></i> Credit Card</a></li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
                                                <i class="fa fa-paypal"></i>  Paypal</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="nav-tab-card">
                                                <form role="form">
                                                    <div class="form-group">
                                                        <label for="username">Full name (on the card)</label>
                                                        <input type="text" class="form-control" name="username" placeholder="" required>
                                                    </div> <!-- form-group.// -->
                                                    <div class="form-group">
                                                        <label for="cardNumber">Card number</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="cardNumber" placeholder="" required>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text text-muted">
                                                                    <i class="fa fa-cc-visa"></i> &nbsp; <i class="fa fa-cc-amex"></i> &nbsp; 
                                                                    <i class="fa fa-cc-mastercard"></i> 
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div> <!-- form-group.// -->
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label><span class="hidden-xs">Expiration</span> </label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" placeholder="MM" name="">
                                                                    <input type="number" class="form-control" placeholder="YY" name="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
                                                                <input type="number" class="form-control" required="">
                                                            </div> <!-- form-group.// -->
                                                        </div>
                                                    </div> <!-- row.// -->
                                                    <button class="subscribe btn btn-primary btn-block" type="button"> Confirm  </button>
                                                </form>
                                            </div> <!-- tab-pane.// -->
                                            <div class="tab-pane fade" id="nav-tab-paypal">
                                                <p>Paypal is easiest way to pay online</p>
                                                <p>
                                                    <a href="{{ url('subscribe/paypal') }}" class="btn btn-primary"> <i class="fa fa-paypal"></i> Log in my Paypal </a>
                                                </p>
                                                <p><strong>Note:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut labore et dolore magna aliqua. </p>
                                            </div>
                                        </div> <!-- tab-content .// -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>  

@endsection

@section('footer')
@endsection