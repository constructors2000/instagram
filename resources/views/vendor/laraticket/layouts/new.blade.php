
@if(Auth::user()->isUser())
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Tubeans : @yield('title')</title>
        @yield('before_styles')
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
        <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/switchery.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/switch.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-switch.css') }}">
        <!-- END VENDOR CSS-->
        
        <!-- BEGIN CHAMELEON  CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
        <!-- END CHAMELEON  CSS-->
        
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
        <!-- END Page Level CSS-->
        
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
        <!-- END Custom CSS-->

        @yield('after_styles')
    </head>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
        
        <form id="logout-form" action="{{ url('logout') }}" method="post" style="display: none;">
            {{ csrf_field() }}
        </form>

        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
            <div class="navbar-wrapper">
                <div class="navbar-container content">
                    <div class="collapse navbar-collapse show" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <li class="dropdown dropdown-language nav-item">
                                <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language"></span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                                    <div class="arrow_box"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> Chinese</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-ru"></i> Russian</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-es"></i> Spanish</a></div>
                                </div>
                            </li>
                            <li class="dropdown dropdown-notification nav-item">
                                <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell bell-shake" id="notification-navbar-link"></i><span class="badge badge-pill badge-sm badge-danger badge-default badge-up badge-glow"></span></a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <div class="arrow_box_right">
                                        <li class="dropdown-menu-header">
                                            <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6>
                                        </li>
                                        <li class="scrollable-container media-list w-100">
                                            <!-- <a href="javascript:void(0)">
                                                <div class="media">
                                                    <div class="media-left align-self-center"><i class="ft-share info font-medium-4 mt-2"></i></div>
                                                    <div class="media-body">
                                                        <h6 class="media-heading info">New Order Received</h6>
                                                        <p class="notification-text font-small-3 text-muted text-bold-600">Lorem ipsum dolor sit amet!</p>
                                                        <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">3:30 PM</time></small>
                                                    </div>
                                                </div>
                                            </a> -->
                                        </li>
                                        <li class="dropdown-menu-footer"><a class="dropdown-item info text-right pr-1" href="javascript:void(0)">Read all</a></li>
                                    </div>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-user nav-item">
                                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">             <span class="avatar avatar-online"><img src="{{ asset('app-assets/images/portrait/small/avatar-s-19.png') }}" alt="avatar"></span></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="arrow_box_right">
                                        <a class="dropdown-item" href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ft-power"></i> Logout</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="{{ asset('app-assets/images/backgrounds/02.jpg') }}">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto">
                        <a class="navbar-brand" href="index.html">
                            <img class="brand-logo" alt="Chameleon" src="{{ asset('app-assets/images/logo/logo.png') }}"/>
                            <h3 class="brand-text">Tubeans</h3>
                        </a>
                    </li>
                    <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
                </ul>
            </div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" nav-item">
                        <a href="{{ url('/user/') }}">
                            <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/accounts') }}">
                            <i class="ft-users"></i><span class="menu-title" data-i18n="">Accounts</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/payment') }}">
                            <i class="ft-shopping-cart"></i><span class="menu-title" data-i18n="">Payment</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/settings') }}">
                            <i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span>
                        </a>
                    </li>
                    <li class=" nav-item">
                        <a href="{{ url('/user/support') }}">
                            <i class="ft-thumbs-up"></i><span class="menu-title" data-i18n="">Support</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navigation-background"></div>
        </div>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-wrapper-before"></div>
                
                <br><br>
                <br><br>
                <br><br>
                
                @yield('content')
            
            </div>
        </div>
        <footer class="footer footer-static footer-light navbar-border navbar-shadow">
            <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
                <span class="float-md-left d-block d-md-inline-block">2018  &copy; Copyright <a class="text-bold-800 grey darken-2" href="#" target="_blank">Tubeans</a></span>
            </div>
        </footer>

        @yield('before_scripts')
       
        <!-- BEGIN VENDOR JS-->
        <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/js/scripts/forms/switch.min.js') }}" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
       
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
       
        <!-- BEGIN CHAMELEON  JS-->
        <script src="{{ asset('app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('app-assets/vendors/js/jquery.sharrre.js') }}" type="text/javascript"></script>
        <!-- END CHAMELEON  JS-->
       
        <!-- BEGIN PAGE LEVEL JS-->
        <!-- END PAGE LEVEL JS-->

        @yield('after_scripts')
    </body>
</html>
@endif

@if(Auth::user()->isAdmin())
<!doctype html>
<head>
    @yield('before_styles')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Admin :: @yield('title')</title>
    <meta name="description" content="Admin">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta property="og:title" content="Admin">
    <meta property="og:site_name" content="Admin">
    <meta property="og:description" content="Admin">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('css/admin.css') }}">
    <style>
        .content-heading{
            padding-top: 0px;
        }
    </style>
	@yield('header')
    @yield('after_styles')
</head>

<body>
    <div id="page-container" class="side-scroll page-header-modern main-content-boxed side-trans-enabled sidebar-o">
        <nav id="sidebar">
            <div id="sidebar-scroll">
                <div class="sidebar-content">
                    <div class="content-header content-header-fullrow px-15">
                        <div class="content-header-section sidebar-mini-visible-b">
                            <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
								<span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                        </div>
                        <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times text-danger"></i>
                            </button>
                            <div class="content-header-item">
                                <a class="link-effect font-w700" href="{{ url('/admin') }}">
                                    <i class="si si-fire text-primary"></i>
                                    <span class="font-size-xl text-dual-primary-dark">Admin</span><span class="font-size-xl text-primary">Panel</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
                            <li>
                                <a href="{{ url('/admin/dashboard') }}"><i class="si si-pie-chart"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/levels') }}"><i class="si si-layers"></i><span class="sidebar-mini-hide">Levels</span></a>
                            </li>
                            <li class="has_submenu">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Users</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/admin') }}">All</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/admin/users/add') }}">Add New</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has_submenu">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-question"></i><span class="sidebar-mini-hide">Tickets</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/tickets') }}">All</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/tickets/create') }}">Add New</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/admin/tickets/options/settings') }}">Options</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <header id="page-header">
            <div class="content-header">
                <div class="content-header-section">
                    <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                        <i class="fa fa-navicon"></i>
                    </button>
                </div>
                <div class="content-header-section">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->first_name }}
                            <i class="fa fa-angle-down ml-5"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                            <!-- <a class="dropdown-item" href="be_pages_generic_profile.php">
                                <i class="si si-user mr-5"></i> Profile
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                                <i class="si si-wrench mr-5"></i> Settings
                            </a> -->
                            <!-- <div class="dropdown-divider"></div> -->
                            <a class="dropdown-item" href="{{ url('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                <i class="si si-logout mr-5"></i> Logout
                            </a>

                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="page-header-search" class="overlay-header">
                <div class="content-header content-header-fullrow">
                    <form action="be_pages_generic_search.php" method="post">
                        <div class="input-group">
                            <span class="input-group-btn">
								<button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
									<i class="fa fa-times"></i>
								</button>
							</span>
                            <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                            <span class="input-group-btn">
								<button type="submit" class="btn btn-secondary">
									<i class="fa fa-search"></i>
								</button>
							</span>
                        </div>
                    </form>
                </div>
            </div>
            <div id="page-header-loader" class="overlay-header bg-primary">
                <div class="content-header content-header-fullrow text-center">
                    <div class="content-header-item">
                        <i class="fa fa-sun-o fa-spin text-white"></i>
                    </div>
                </div>
            </div>
        </header>
        <main id="main-container" style="min-height: 520px;">
            <div class="content">
				@yield('content')
            </div>
        </main>
        <footer id="page-footer" class="opacity-0" style="opacity: 1;">
            <div class="content py-20 font-size-xs clearfix">
                <div class="float-right">
                    Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://the360circle.com" target="_blank">the360circle</a>
                </div>
            </div>
        </footer>
    </div>
    @yield('before_scripts')
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/be_tables_datatables.js') }}"></script>
    <script>
        $(function(){
            var _c_url = window.location.href;
            $("ul.nav-main > li > a[href='"+_c_url+"']").addClass("active");
            // has_submenu

            $("ul.nav-main > li.has_submenu > ul > li > a[href='"+_c_url+"']").addClass("active");
            $("ul.nav-main > li.has_submenu > ul > li > a[href='"+_c_url+"']").parents("li.has_submenu").addClass("open");
            // $("a[href='"+_c_url+"']").parents("div.sub-item").parent("li.nav-item").addClass("active");
        });
    </script>
	@yield('footer')
    @yield('after_scripts')
</body>

</html>
@endif