<?php

namespace App\Libraries;

session_start();
// require_once '../../vendor/autoload.php';

use Illuminate\Support\Facades\Log;
use InstagramAPI\Signatures;
use \InstagramAPI\Instagram as Ig;
use InstagramAPI\Response\LoginResponse;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Exception\SentryBlockException;
use InstagramAPI\Exception\InvalidUserException;
use InstagramAPI\Exception\AccountDisabledException;
use InstagramAPI\Exception\ChallengeRequiredException;
use InstagramAPI\Exception\IncorrectPasswordException;
use InstagramAPI\Exception\CheckpointRequiredException;

\InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;

class ExtendedInstagram extends Ig {
    public function changeUser( $username, $password ) {
        $this->_setUser( $username, $password );
    }
}



class Instagram
{
    private $ig;

    const SEND_VERIFICATION_SMS = 2; //001010
    const SEND_VERIFICATION_EMAIL = 2; //000110
    const SEND_VERIFICATIONS_CODE = 2; //000010
    const AUTHENTICATION_SUCCESS = 1;
    const AUTHENTICATION_FAIL = 0;

    public const VERIFICATION_SMS = 0;
    public const VERIFICATION_EMAIL = 1;





    public function __construct($debug = false, $truncateDebug = false)
    {
        $this->ig = new ExtendedInstagram($debug, $truncateDebug);
    }



    /**
     *
     *
     *
     * @param $username
     * @param $password
     * @param null $proxy
     * @param null $code
     * @param bool $debug
     * @param bool $truncateDebug
     * @return string|array
     */
    public function login($username, $password, $proxy = null, $code = null, $debug = false, $truncateDebug = false){
        // Verification Method = 0; //0 = SMS, 1 = Email
        // Leave these
        $account_id      = '';
        $challenge_id = '';

            if(!empty($_SESSION) && !empty($_SESSION["CHALLENGE_REQUIRED"])){
            try {

                $username       = $_SESSION["CHALLENGE_REQUIRED"]["username"];
                $password       = $_SESSION["CHALLENGE_REQUIRED"]["password"];
                $proxy          = $_SESSION["CHALLENGE_REQUIRED"]["proxy"];
                $uuid           = $_SESSION["CHALLENGE_REQUIRED"]["uuid"];
                $device_id      = $_SESSION["CHALLENGE_REQUIRED"]["device_id"];
                $account_id     = $_SESSION["CHALLENGE_REQUIRED"]["account_id"];
                $token          = $_SESSION["CHALLENGE_REQUIRED"]["token"];
                $checkApiPath   = $_SESSION["CHALLENGE_REQUIRED"]["checkApiPath"];

                if($proxy != null)
                    $this->ig->setProxy($proxy);
                $this->ig->changeUser( $username, $password );
                $customResponse = $this->ig->request($checkApiPath)
                    ->setNeedsAuth(false)
                    ->addPost('security_code',  $code)
                    ->addPost('_uuid',          $uuid)
                    ->addPost('guid',           $uuid)
                    ->addPost('device_id',      $device_id)
                    ->addPost('_uid',           $account_id)
                    ->addPost('_csrftoken',     $token)
                    ->getDecodedResponse();

                Log::debug([$code, $uuid, $device_id, $account_id, $token]);
                Log::debug($customResponse);

                if ( ! is_array( $customResponse ) ) {
                    return "Weird response from challenge validation...";
                }
                if ( $customResponse['status'] == 'ok' && (int)$customResponse['logged_in_user']['pk'] === (int)$account_id ) {
                    return ['status'=>self::AUTHENTICATION_SUCCESS, 'msg'=>'Not a challenge required exception...'];
                } else {
                    return ['status'=>self::AUTHENTICATION_SUCCESS, 'msg'=>'Probably finished...'];
                }
            } catch ( \Exception $ex ) {
                dd($ex->getMessage());
            }
        }
        try {
            if(!is_null($proxy))
                $this->ig->setProxy($proxy);

            $loginResponse  = $this->ig->login($username, $password);
//            /**
//             * Two factory verification.
//             * The verification code will be sent by Instagram via SMS.
//             * */
//            if (!is_null($loginResponse) && $loginResponse->isTwoFactorRequired()) {
//                $twoFactorIdentifier = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
//                $_SESSION["TWOFA"] = [
//                    "twoFactorIdentifier"=>$twoFactorIdentifier,
//                    "username" => $username,
//                    "password" => $password,
//                    "proxy" => $proxy
//                ];
//
//                return ['status'=>self::SEND_VERIFICATION_SMS, 'msg'=>'Code that you received via email or sms:'];
//            }

            if ( $loginResponse instanceof LoginResponse || $loginResponse === null )
                Log::info("Not a challenge required exception...");

            return ['status'=>self::AUTHENTICATION_SUCCESS, 'msg'=>'Not a challenge required exception...'];

        } catch (CheckpointRequiredException $e) {
            return [
                'status'=>self::AUTHENTICATION_FAIL,
                'msg'=>"Please goto <a href='http://instagram.com' target='_blank'>instagram.com</a> and pass checkpoint!"
            ];
        } catch (AccountDisabledException $e) {
            return [
                'status'=>self::AUTHENTICATION_FAIL,
                'msg'=>"Your account has been disabled for violating Instagram terms. 
                        <a href='https://help.instagram.com/366993040048856'>Click here</a> 
                        to learn how you may be able to restore your account."];
        } catch (SentryBlockException $e) {
            return [
                'status'=>self::AUTHENTICATION_FAIL,
                'msg'=>"Your account has been banned from Instagram API for spam behaviour or otherwise abusing."];
        } catch (IncorrectPasswordException $e) {
            return [
                'status'=>self::AUTHENTICATION_FAIL,
                'msg'=>"The password you entered is incorrect. Please try again."];
        } catch (InvalidUserException $e) {
            return [
                'status'=>self::AUTHENTICATION_FAIL,
                'msg'=>"The username you entered doesn't appear to belong to an account. Please check your username and try again."];
        } catch(InstagramException $exception) {
            if ( ! method_exists( $exception, 'getResponse' ) ) {
                return [
                    'status'=>self::AUTHENTICATION_FAIL,
                    'msg'=> $exception->getMessage()];
            }

            $response = $exception->getResponse();

            /**
             *
             * Checkpoint challenge required.
             *
             * */
            if ( $exception instanceof ChallengeRequiredException
                && $response->getErrorType() === 'checkpoint_challenge_required') {

                sleep( 5 );

                $checkApiPath = substr( $response->getChallenge()->getApiPath(), 1);
                $customResponse = $this->ig->request($checkApiPath)
                    ->setNeedsAuth(false)
                    ->addPost('choice',     self::VERIFICATION_SMS)
                    ->addPost('_uuid',      $this->ig->uuid)
                    ->addPost('guid',       $this->ig->uuid)
                    ->addPost('device_id',  $this->ig->device_id)
                    ->addPost('_uid',       $this->ig->account_id)
                    ->addPost('_csrftoken', $this->ig->client->getToken())
                    ->getDecodedResponse();

                if ( is_array( $customResponse )) {
                    Log::notice($customResponse);
                    if ($customResponse['status']== 'ok') {
                        $account_id = $customResponse['user_id'];
                        $challenge_id = $customResponse['nonce_code'];
                    } else {
                        $customResponse = $this->ig->request($checkApiPath)
                            ->setNeedsAuth(false)
                            ->addPost('choice',     self::VERIFICATION_EMAIL)
                            ->addPost('_uuid',      $this->ig->uuid)
                            ->addPost('guid',       $this->ig->uuid)
                            ->addPost('device_id',  $this->ig->device_id)
                            ->addPost('_uid',       $this->ig->account_id)
                            ->addPost('_csrftoken', $this->ig->client->getToken())
                            ->getDecodedResponse();

                        if ( is_array( $customResponse )) {
                            if ($customResponse['status']== 'ok') {
                                $account_id = $customResponse['user_id'];
                                $challenge_id = $customResponse['nonce_code'];
                            } else {
                                return [
                                    'status'=>self::AUTHENTICATION_FAIL,
                                    'msg'=>"Oops, fail response (email)"];
                            }
                        } else {
                            return [
                                'status'=>self::AUTHENTICATION_FAIL,
                                'msg'=>"Oops, something went wrong (email)"
                            ];
                        }
                    }
                } else {
                    return [
                        'status'=>self::AUTHENTICATION_FAIL,
                        'msg'=>"Oops, something went wrong (sms)"];
                }
            } else {
                return [
                    'status'=>self::AUTHENTICATION_FAIL,
                    'msg'=>"Oops: `checkpoint_challenge_required` not found"];
            }
            $_SESSION["CHALLENGE_REQUIRED"] = [
                "username"      => $username,
                "password"      => $password,
                "proxy"         => $proxy,
                "uuid"          => $this->ig->uuid,
                "device_id"     => $this->ig->device_id,
                "account_id"    => $account_id,
                "token"         => $this->ig->client->getToken(),
                "checkApiPath"  => $checkApiPath
            ];

            return [
                'status'=>self::SEND_VERIFICATIONS_CODE,
                'msg'=>'Code that you received via email or sms:'];
        }
        catch(\Exception $ex) {
            return [
                'status'=>self::AUTHENTICATION_FAIL,
                'msg'=>$ex->getMessage()];

        }
    }


    public function postVideo($video_name)
    {
        try
        {
            $this->ig->timeline->uploadVideo($video_name);
            return true;
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function profileInfo()
    {
        try
        {
            $info = $this->ig->people->getSelfInfo();
            return $info;
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function getUserIdfromName($username)
    {
        try
        {
            $data = $this->ig->people->getUserIdForName($username);
            return $data;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function getNamefromID($pk)
    {
        try
        {
            $data = $this->ig->people->getInfoById($pk);
            return $data;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function get_profile($pk)
    {
        try
        {
            $feed = $this->ig->people->getInfoById($pk);
            return $feed;
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function get_profile_feed()
    {
        try
        {
            $feed = $this->ig->timeline->getSelfUserFeed();
            return $feed->getItems();
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function get_user_feed($pk)
    {
        try
        {
            $feed = $this->ig->timeline->getUserFeed($pk);
            return $feed->getItems();
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    // types:
        //  hashtag
        //  locations
        //  self feed
        //  popular feed
        //  timeline feed

    public function like_post($media_id)
    {
        try
        {
            $this->ig->media->like($media_id);
            return true;
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function comment_post($media_id, $comment)
    {
        try
        {
            $this->ig->media->comment($media_id, $comment);
            return true;
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function follow($pk)
    {
        try
        {
            $this->ig->people->follow($pk);
            return true;
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function unfollow($pk)
    {
        try
        {
            $this->ig->people->unfollow($pk);
            return true;
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function message($pk, $message)
    {
        try
        {
            $recipients = [
                "thread"   =>  $pk
            ];
            $this->ig->direct->sendText($recipients, $message);
            return true;
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function share_on_group($media_id, $group_id)
    {
        try
        {
            $recipients = [
                "thread"   =>  $group_id
            ];
            $this->ig->direct->sendPost($recipients, $media_id,[
                "media_type"    =>  "photo"
            ]);
            return true;
        }
        catch(\InstagramAPI\Exception\CheckpointRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\InstagramAPI\Exception\FeedbackRequiredException $ex)
        {
            return "ActionSpamError";
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function message_video($pk, $message, $media_id)
    {
        try
        {

            $recipients = [];
            array_push($recipients, $pk);
            $options = [
                "media_type"    =>  "video",
                "text"          =>  $message
            ];
            $this->ig->direct->sendPost($recipients, $media_id,$options);
            return true;
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function messages()
    {
        return $this->ig->direct->getInbox();
    }

    public function getThreadMessages($id)
    {
        return $this->ig->direct->getThread($id);
    }

    /*
    public function sendChallengeCode($url)
    {
        // default value
        // $pattern = '/window._sharedData = (.*);/';
        // preg_match($pattern, $res, $matches);
        // $json = json_decode($matches[1]);
        // $method = $json->entry_data->Challenge[0]->extraData->content[3]->fields[0]->values[0];
        
        $response = $this->request($url)
            ->setNeedsAuth(false)    
            ->addPost('choice', "1")
            ->addPost('device_id', $this->device_id)
            ->addPost('guid', $this->uuid)
            ->addPost('_csrftoken', $this->client->getToken())
            ->getRawResponse();

        return $response;
    }

    public function acceptChallengeCode($url, $code)
    {
        $response = $this->request($url)
            ->setNeedsAuth(false)    
            ->addPost('security_code', $code)
            ->addPost('device_id', $this->device_id)
            ->addPost('guid', $this->uuid)
            ->addPost('_csrftoken', $this->client->getToken())
            ->getRawResponse();

        return $response;
    }
    */
}

function readln( $prompt ) {
    if ( PHP_OS === 'WINNT' ) {
        echo "$prompt ";

        return trim( (string) stream_get_line( STDIN, 6, "\n" ) );
    }
    return trim( (string) readline( "$prompt " ) );
}
