<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'site_settings';

    protected $fillable = [
        'language','theme', 'name', 'logo'
    ];

    public static function settings(){
        return Site::first();
    }
}