<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'id','hashtags_done','accounts_done', 'location_all_ids', 'location_done_ids', 'timeline_feed_done','new_followers_done'
    ];

    public $timestamps = false;
}