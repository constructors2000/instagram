<?php

namespace App;

// use Laravel\Cashier\Billable;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Sdkcodes\LaraTicket\Traits\UserTicket;

class User extends Authenticatable
{
    use Notifiable;
    // use Billable;
    use UserTicket;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'first_name', 'last_name', 'email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        $data = User_Role::where('user_id',$this->id)->first();

        if($data){
            // TODO get from levels table
            $level = 0;

            if(in_array($level, json_decode($data->levels))){
                return true;
            }
        }
        
        return false;
    }

    public function isUser()
    {
        $data = User_Role::where('user_id',$this->id)->first();
        
        if($data){
            // TODO get from levels table
            $level = 1;

            if(in_array($level, json_decode($data->levels))){
                return true;
            }
        }
        
        return false;
    }

    public function instagramAccountExists()
    {
        try
        {
            $count = Instagram_Accounts::where('user_id',$this->id)->count();
            
            if($count > 0){
                return true;
            }
            
            return false;
        }
        catch(\Exception $ex)
        {
            return false;
        }
    }

    public function instagramAccounts()
    {
        try
        {
            $accounts = Instagram_Accounts::where('user_id',$this->id)->select('id','username')->get();
            
            return $accounts;
        }
        catch(\Exception $ex)
        {
            return null;
        }
    }

    public function plan()
    {
        $plan = User_Role::where('user_id',$this->id)->first();
        
        if($plan){
            $user_levels = json_decode($plan->levels);

            $details = Levels::where('id', $user_levels[1])->first();

            return $details;
        }
        
        return false;
    }

    public function bilingExists()
    {
        return true;
        $count = Subscriptions::where('user_id',$this->id)->count();        

        if($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function site()
    {
        try
        {
            $site = Site::first();
            $data = [
                "language"  =>  $site->language,
                "theme"     =>  $site->theme
            ];

            return $data;
        }
        catch(\Exception $ex)
        {
            return [
                "language"  =>  "en",
                "theme"     =>  "default"
            ];
        }
    }

    public function notifications()
    {
        $notifications = DB::table('notifications')->where('user_id', $this->id)->get();
        return $notifications;
    }

    public function unread_count()
    {
        $notifications = DB::table('notifications')->where('user_id', $this->id)->where('had_read',false)->count();
        return $notifications;
    }
}
