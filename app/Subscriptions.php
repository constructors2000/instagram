<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = [
			'user_id',
			'name',
            'stripe_id',
            'quantity',
            'trial_ends_at',
            'ends_at'
    ];
}