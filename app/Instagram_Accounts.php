<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instagram_Accounts extends Model
{
    protected $table = 'instagram_accounts';

    protected $fillable = [
			'user_id',
			'username',
			'password'
    ];
}