<?php

namespace App\Http\Controllers;

use File;
use App\User;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Libraries\Instagram as instagram_library;

date_default_timezone_set("UTC");

class CronController extends Controller
{
    // send DM when account uploads a photo/video to feed.
    // do like, comment or message on group and their posts

    public function capture()
    {
        // get all active instagram accounts and their settings
        DB::table('settings')
        ->where('status', true)
        // ->join('instagram_accounts', DB::raw('CAST(settings.instagram_id as INT)'),'=','instagram_accounts.id')
        // server
         ->join('instagram_accounts', 'settings.instagram_id','=','instagram_accounts.id')
        ->orderBy('settings.id')
        ->chunk(10, function($settings){
            foreach ($settings as $setting)
            {
                // check if limit is not crossed.
                $continue_message = false;
                $continue_comments = false;
                $continue_likes = false;

                $today_log_comments = DB::table('activity_log')
                    ->where('user_id', $setting->user_id)
                    ->where('instagram_id', $setting->instagram_id)
                    ->where('comment_done', true)
                    ->where('created_at', '<',strtotime("00:00"))
                    ->count();
                $today_log_likes = DB::table('activity_log')
                    ->where('user_id', $setting->user_id)
                    ->where('instagram_id', $setting->instagram_id)
                    ->where('like_done', true)
                    ->where('created_at', '<',strtotime("00:00"))
                    ->count();

                if($setting->message_time != null)
                {
                    $today_log_messages = DB::table('activity_log')
                        ->where('user_id', $setting->user_id)
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('message_done', true)
                        ->where('created_at', '<',strtotime("00:00"))
                        ->count();

                    
                    if($setting->max_messages > $today_log_messages)
                    {
                        // check time
                        // send after certain time of last post shared
                        $continue_message = true;
                    }
                }
                else
                {
                    $continue_message = true;
                }

                if($setting->max_comments > $today_log_comments)
                {
                    $continue_comments = true;
                }

                if($setting->max_likes > $today_log_likes)
                {
                    $continue_likes = true;
                }

                // get active groups
                $groups = DB::table('message_groups')
                    ->where('instagram_id', $setting->instagram_id)
                    ->where('user_id', $setting->user_id)
                    ->where('status', true)
                    ->get();

                
                if(sizeof($groups) > 0)
                {
                    // randomly select comment
                    if($setting->comments != null)
                    {
                        $comments = explode(",", $setting->comments);
                        $selected_comment = array_rand($comments);
                        $selected_comment = $comments[$selected_comment];
                    }
                    else
                    {
                        $selected_comment = "nice.";
                    }

                    $ig = new instagram_library();
                
                    $response = $ig->login($setting->username, $setting->password);

                    if(strpos($response, "challenge_code_sent") !== false )
                    {
                        // add to alerts
                        echo "unable to login";
                        continue;
                    }
                    else if($response == "logged_in")
                    {
                        // check for messages in groups and post like and comment

                        foreach ($groups as $group)
                        {
                            $thread_id = $group->group_id;
                            $messages = $ig->getThreadMessages($thread_id);
                            $messages1 = $messages->getThread()->getItems();
                            //echo json_encode($messages1); die();
                            // check for new posts on user profile
                            // get latest post pk from user profile
                            $info = $ig->get_profile_feed();

                            $latest_pk = 0;

                            $all_media_shared_ids = [];
                            if(array_key_exists(0, $info))
                            {
                                $latest_pk = $info[0]->getId();
                            }
                            foreach ($messages1 as $message)
                            {
                                $continue = false;
                                
                                if($message->getItemType() == "media_share")
                                {
                                    array_push($all_media_shared_ids, $message->getMediaShare()->getId());

                                    // skip if user in blacklist
                                    if($setting->blacklist != null)
                                    {
                                        $blacklist = explode(",", $setting->blacklist);
                                        if(in_array($message->getMediaShare()->getUser()->getUsername(), $blacklist))
                                        {
                                            $continue = false;
                                        }
                                        else
                                        {
                                            $continue = true;
                                        }
                                    }

                                    // skip if already done on the item
                                    $exists = DB::table('activity_log')->where('item_pk', $message->getMediaShare()->getPk())->exists();
                                    if($exists)
                                    {
                                        $continue = false;
                                    }
                                    else
                                    {
                                        $continue = true;
                                    }

                                    if($continue)
                                    {
                                        // check if user is not the author of this message
                                        if($message->getMediaShare()->getUser()->getPk() != $setting->instagram_pk)
                                        {
                                            $comment_sent = false;
                                            $like_sent = false;

                                            if($continue_likes)
                                            {
                                                if($setting->like_by_bot)
                                                {
                                                    //echo $message->getMediaShare()->getPk(); die();
                                                    // like on shared media
                                                    $ig->like_post($message->getMediaShare()->getPk());
                                                    $like_sent = true;
                                                }
                                            }
                                            
                                            if($continue_comments)
                                            {
                                                // if mode is automatic
                                                if($setting->comment_by_bot)
                                                {
                                                    // comment on shared media
                                                    $ig->comment_post($message->getMediaShare()->getPk(), $selected_comment);
                                                    $comment_sent = true;
                                                }
                                            }

                                            // save in activity log
                                            DB::table('activity_log')->insert([
                                                "user_id"       =>  $setting->user_id,
                                                "instagram_id"  =>  $setting->instagram_id,
                                                "item_pk"       =>  $message->getMediaShare()->getPk(),
                                                "like_done"     =>  $like_sent,
                                                "comment_done"  =>  $comment_sent,
                                                "created_at"    =>  time()
                                            ]);

                                            DB::table('activity_log')->insert([
                                                "user_id"       =>  $setting->user_id,
                                                "instagram_id"  =>  $setting->instagram_id,
                                                "item_pk"       =>  $thread_id,
                                                "message_done"  =>  false,
                                                "created_at"    =>  time()
                                            ]);
                                        }
                                    }
                                }
                                
                            }
                            if($continue_message)
                            {
                                // send new message
                                if($setting->messages != null)
                                {
                                    // send only one message to group
                                    $already_sent = DB::table('activity_log')
                                                        ->where('instagram_id', $setting->instagram_id)
                                                        ->where('user_id', $setting->user_id)
                                                        ->where('item_pk', $thread_id)
                                                        ->orderBy('id', 'desc')
                                                        ->first();
                                                   
                                    if($already_sent->message_done ==0)
                                    {
                                        // check for interval

                                        // time difference

                                        $last_posted = DB::table('activity_log')
                                                        ->where('user_id', $setting->user_id)
                                                        ->where('instagram_id', $setting->instagram_id)
                                                        ->where('item_pk', $thread_id)
                                                        ->where('message_done',false)
                                                        ->orderBy('id', 'desc')
                                                        ->first();
                                        $last_posted_count = DB::table('activity_log')
                                                        ->where('user_id', $setting->user_id)
                                                        ->where('instagram_id', $setting->instagram_id)
                                                        ->where('item_pk', $thread_id)
                                                        ->where('message_done',false)
                                                        ->count();
                                        
                                        if($last_posted != null)
                                        {
                                            if((time() - $last_posted->created_at) >  $setting->message_time_interval)
                                            {
                                                if($setting->messages_count_condition <  $last_posted_count)
                                                {
                                                    $messages = explode(",", $setting->messages);
                                                    $selected_message = array_rand($messages);
                                                    $selected_message = $messages[$selected_message];

                                                    // send message to group
                                                    $ig->message($thread_id, $selected_message);

                                                    // update in activity log
                                                    DB::table('activity_log')
                                                        ->where("user_id",$setting->user_id)
                                                        ->where("instagram_id",$setting->instagram_id)
                                                        ->where("item_pk",$thread_id)
                                                        ->where("id",$last_posted->id)
                                                        ->update([
                                                            "message_done"  =>  true
                                                        ]);   
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $messages = explode(",", $setting->messages);
                                            $selected_message = array_rand($messages);
                                            $selected_message = $messages[$selected_message];

                                            // send message to group
                                            $ig->message($thread_id, $selected_message);

                                            // save in activity log
                                            DB::table('activity_log')->insert([
                                                "user_id"       =>  $setting->user_id,
                                                "instagram_id"  =>  $setting->instagram_id,
                                                "item_pk"       =>  $thread_id,
                                                "message_done"  =>  true,
                                                "created_at"    =>  time()
                                            ]);
                                        }
                                       
                                    }
                                }
                            }
                            if($setting->mode=='automatic' || $setting->mode=='semi'){
        
                                if(!in_array($latest_pk, $all_media_shared_ids))
                                {
                                    
                                
                                    if($latest_pk != 0)
                                    {
                                        // post in this group
                                        
                                        $ress = $ig->share_on_group($latest_pk, $thread_id);
                                        
                                        DB::table('activity_log')->insert([
                                            "user_id"       =>  $setting->user_id,
                                            "instagram_id"  =>  $setting->instagram_id,
                                            "item_pk"       =>  $thread_id,
                                            "message_done"  =>  false,
                                            "created_at"    =>  time()
                                        ]);

                                        // post message

                                        $messages = explode(",", $setting->messages);
                                        $selected_message = array_rand($messages);
                                        $selected_message = $messages[$selected_message];

                                        // send message to group
                                        $ig->message($thread_id, $selected_message);

                                        
                                        echo $ress;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        });

        echo "done";
        
    }


    public function test()
    {
        // fetch mode
        
        // automatic
            // like and comment on all posts shared in the selected groups
                // trigger like/comment based on @mention.
            // when upload a photo (via timeline or share), send post + message.
                // apply X minutes of upload -> wait and time interval.
            // daily limits check
        
        // semi
            // like and comment on previous X posts and will trigger only when upload a photo
                // trigger like/comment based on @mention.
            // when upload a photo (via timeline or share), send post + message.
                // apply X minutes of upload -> wait and time interval. + previous counter.
            // daily limits check
        
        // manual
            // send message as per cron or settings, (if more than 1 group is selected, apply time interval)
                // trigger like/comment based on @mention.
            // daily limits check

        // note:
            // in case share post is deleted, don't send photo again.
    }

    public function cron_auto()
    {
        DB::table('settings')
        ->where('status', true)
        ->where('mode', 'automatic')
        // ->join('instagram_accounts', DB::raw('CAST(settings.instagram_id as INT)'),'=','instagram_accounts.id')
        // server
        ->join('instagram_accounts', 'settings.instagram_id','=','instagram_accounts.id')
        ->orderBy('settings.id')
        ->chunk(10, function($settings){
            // echo json_encode($settings);
            // die();
            foreach ($settings as $setting)
            {
                $ig = new instagram_library();
                $response = $ig->login($setting->username, $setting->password);
                if(strpos($response, "challenge_code_sent") !== false )
                {
                    // add to alerts
                    echo "unable to login";
                    continue;
                }
                else if($response == "logged_in")
                {   
                    // automatic

                    // limits
                    // per account
                    $max_messages = $setting->max_messages;
                    $can_send_message = false;

                    $done_message_count = DB::table('activity_log')
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('type', 'posted')
                        ->where('user_id', $setting->user_id)
                        ->where('done', true)
                        ->where('created_at', '>',strtotime("00:00"))
                        ->count();

                    if($max_messages > $done_message_count)
                    {
                        $can_send_message = true;
                    }

                    $max_comments = $setting->max_comments;
                    $can_send_comment = false;

                    if($setting->comment_by_bot)
                    {
                        $done_comment_count = DB::table('activity_log')
                            ->where('instagram_id', $setting->instagram_id)
                            ->where('type', 'shared')
                            ->where('user_id', $setting->user_id)
                            ->where('comment_done', true)
                            ->where('created_at', '>',strtotime("00:00"))
                            ->count();

                        if($max_comments > $done_comment_count)
                        {
                            $can_send_comment = true;
                        }
                    }

                    $max_likes = $setting->max_likes;
                    $can_send_like = false;

                    if($setting->like_by_bot)
                    {
                        $done_like_count = DB::table('activity_log')
                            ->where('instagram_id', $setting->instagram_id)
                            ->where('type', 'shared')
                            ->where('user_id', $setting->user_id)
                            ->where('like_done', true)
                            ->where('created_at', '>',strtotime("00:00"))
                            ->count();

                        if($max_likes > $done_like_count)
                        {
                            $can_send_like = true;
                        }
                    }
                    
                    // get active groups
                    $groups = DB::table('message_groups')
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('user_id', $setting->user_id)
                        ->where('status', true)
                        ->get();

                    
                    if(sizeof($groups) > 0)
                    {   
                        // get latest pk from user feed

                        $info = $ig->get_profile_feed();
                        $latest_pk = 0;
                        $latest_pk_time = 0;
                        
                        if(array_key_exists(0, $info))
                        {
                            $latest_pk = $info[0]->getId();
                            $latest_pk_time = $info[0]->getTakenAt();
                        }

                        foreach ($groups as $key => $group)
                        {

                            if($setting->comments != null)
                            {
                                $comments = explode(",", $setting->comments);
                                $selected_comment = array_rand($comments);
                                $selected_comment = $comments[$selected_comment];
                            }
                            else
                            {
                                $selected_comment = "nice.";
                            }

                            /**
                             * Like and Comment on all posts
                            */
                            // like and comment on all posts shared in the selected groups
                                // except blacklist
                                // skip own posts
                                // trigger like/comment based on @mention.
                                // within daily limits check
                            
                                $group_messages = $ig->getThreadMessages($group->group_id)->getThread()->getItems();
                                // echo json_encode($group_messages);
                                // die();
                                foreach ($group_messages as $g_msg)
                                {   
                                    // daily limits
                                    if($can_send_comment || $can_send_like)
                                    {
                                        $continue_with_action = false;

                                        // get shared messages
                                        if($g_msg->getItemType() == "media_share")
                                        {
                                            $g_msg_pk = $g_msg->getMediaShare()->getPk();
                                            $g_msg_time = $g_msg->getTimestamp();
                                            $g_msg_username = null;

                                            // get username from id
                                            $user_data = $ig->getNamefromID($g_msg->getUserId());
                                            if($user_data != null)
                                            {
                                                $g_msg_username = $user_data->getUser()->getUsername();
                                            }

                                            $g_msg_shared_user_id = $g_msg->getUserId();

                                            // check if already did action

                                            $already_did = DB::table('activity_log')
                                                ->where("user_id", $setting->user_id)
                                                ->where("instagram_id", $setting->instagram_id)
                                                ->where("type", "shared")
                                                ->where("group_id", $group->id)
                                                ->where("item_pk", $g_msg_pk)
                                                ->where("item_taken_at", $g_msg_time)
                                                ->where("done",true)
                                                ->exists();

                                            if($already_did)
                                            {
                                                continue;
                                            }

                                            // skip if shared by own account
                                            if($g_msg_shared_user_id != $setting->instagram_pk)
                                            {
                                                $continue_with_action = true;
                                            }
                                            else
                                            {
                                                continue;
                                            }

                                            // skip if user in blacklist
                                            if($setting->blacklist != null)
                                            {
                                                $blacklist = explode(",", $setting->blacklist);
                                                if(in_array($g_msg_username, $blacklist))
                                                {
                                                    $continue_with_action = false;
                                                    continue;
                                                }
                                                else
                                                {
                                                    $continue_with_action = true;
                                                }
                                            }
                                        }
                                        
                                        // get @mention messages
                                        if($g_msg->getItemType() == "text")
                                        {
                                            // check if message has @ or like/comment in it.
                                            $text = $g_msg->getText();

                                            if(strpos($text, '@') !== false)
                                            {
                                                // like pic of user mentioned with @

                                                $data = explode("@", $text);     
                                                $data = explode(" ", $data[1]);
                                                $user_name = $data[0];
                                                $user_pk = $ig->getUserIdfromName($user_name);
                                                if($user_pk != null)
                                                {
                                                    $feed = $ig->get_user_feed($user_pk);
                                                    if(array_key_exists("0", $feed))
                                                    {
                                                        $g_msg_pk = $feed[0]->getPk();
                                                        $g_msg_time = $g_msg->getTimestamp();
                                                        $g_msg_username = null;

                                                        // get username from id
                                                        $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                        if($user_data != null)
                                                        {
                                                            $g_msg_username = $user_data->getUser()->getUsername();
                                                        }

                                                        $g_msg_shared_user_id = $g_msg->getUserId();

                                                        // check if already did action

                                                        $already_did = DB::table('activity_log')
                                                            ->where("user_id", $setting->user_id)
                                                            ->where("instagram_id", $setting->instagram_id)
                                                            ->where("type", "shared")
                                                            ->where("group_id", $group->id)
                                                            ->where("item_pk", $g_msg_pk)
                                                            ->where("item_taken_at", $g_msg_time)
                                                            ->where("done",true)
                                                            ->exists();

                                                        if($already_did)
                                                        {
                                                            continue;
                                                        }

                                                        // skip if shared by own account
                                                        if($g_msg_shared_user_id != $setting->instagram_pk)
                                                        {
                                                            $continue_with_action = true;
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }

                                                        // skip if user in blacklist
                                                        if($setting->blacklist != null)
                                                        {
                                                            $blacklist = explode(",", $setting->blacklist);
                                                            if(in_array($g_msg_username, $blacklist))
                                                            {
                                                                $continue_with_action = false;
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                $continue_with_action = true;
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            if((strpos($text, 'like') !== false) && (strpos($text, '@') === false))
                                            {
                                                // like user pic who posted message

                                                $feed = $ig->get_user_feed($g_msg->getUserId());
                                                if(array_key_exists("0", $feed))
                                                {
                                                    $g_msg_pk = $feed[0]->getPk();
                                                    $g_msg_time = $g_msg->getTimestamp();
                                                    
                                                    $g_msg_username = null;

                                                    // get username from id
                                                    $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                    if($user_data != null)
                                                    {
                                                        $g_msg_username = $user_data->getUser()->getUsername();
                                                    }

                                                    $g_msg_shared_user_id = $g_msg->getUserId();

                                                    // check if already did action

                                                    $already_did = DB::table('activity_log')
                                                        ->where("user_id", $setting->user_id)
                                                        ->where("instagram_id", $setting->instagram_id)
                                                        ->where("type", "shared")
                                                        ->where("group_id", $group->id)
                                                        ->where("item_pk", $g_msg_pk)
                                                        ->where("item_taken_at", $g_msg_time)
                                                        ->where("done",true)
                                                        ->exists();

                                                    if($already_did)
                                                    {
                                                        continue;
                                                    }

                                                    // skip if shared by own account
                                                    if($g_msg_shared_user_id != $setting->instagram_pk)
                                                    {
                                                        $continue_with_action = true;
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }

                                                    // skip if user in blacklist
                                                    if($setting->blacklist != null)
                                                    {
                                                        $blacklist = explode(",", $setting->blacklist);
                                                        if(in_array($g_msg_username, $blacklist))
                                                        {
                                                            $continue_with_action = false;
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $continue_with_action = true;
                                                        }
                                                    }
                                                } 
                                            }
                                        }
                                        
                                        if($continue_with_action)
                                        {
                                            $like_sent = true;
                                            $comment_sent = false;

                                            if($can_send_like)
                                            {
                                                // like post
                                                $ig->like_post($g_msg_pk);
                                                $like_sent = true;
                                            }
                                            if($can_send_comment)
                                            {
                                                // comment post
                                                $ig->comment_post($g_msg_pk, $selected_comment);
                                                $comment_sent = true;
                                            }

                                            // save to activity
                                            DB::table('activity_log')->insert([
                                                "user_id"           =>  $setting->user_id,
                                                "instagram_id"      =>  $setting->instagram_id,
                                                "type"              =>  "shared",
                                                "group_id"          =>  $group->id,
                                                "item_pk"           =>  $g_msg_pk,
                                                "item_taken_at"     =>  $g_msg_time,
                                                "like_done"         =>  $like_sent,
                                                "comment_done"      =>  $comment_sent,
                                                "done"              =>  true,
                                                "created_at"        =>  time()
                                            ]);
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    }
                                }
                            
                            /**
                             * Upload Photo from own profile feed to active groups
                            */
                            // when upload a photo (via timeline or share), send post + message.
                                // apply X minutes of upload -> wait and time interval.

                            // check check if already posted on groups
                            $already_posted = DB::table('activity_log')
                                ->where('user_id', $setting->user_id)
                                ->where('instagram_id', $setting->instagram_id)
                                ->where('group_id', $group->id)
                                ->where('item_pk', $latest_pk)
                                ->where('type', 'posted')
                                ->exists();
                            
                            if(!$already_posted)
                            {
                                // see if it is latest item in profile

                                // get last item from activity log
                                $last_item = DB::table('activity_log')
                                    ->where('user_id', $setting->user_id)
                                    ->where('instagram_id', $setting->instagram_id)
                                    ->where('type', 'posted')
                                    ->orderBy('id', 'desc')
                                    ->first();

                                $post_this_item_in_group = false;
                                
                                if($last_item)
                                {
                                    // compare
                                    if($latest_pk_time > $last_item->item_taken_at)
                                    {
                                        $post_this_item_in_group = true;
                                    }
                                }
                                else
                                {
                                    $post_this_item_in_group = true;
                                }

                                if($post_this_item_in_group)
                                {
                                    $time_to_upload = time();

                                    $schedule_later = false;

                                    // apply time and interval (if any)
                                    if($setting->message_time != null)
                                    {
                                        $schedule_later = true;
                                        $time_to_upload = $time_to_upload + $setting->message_time;   
										// echo $setting->message_time;
										// die();
                                    }

                                    if($setting->message_time_interval != null)
                                    {
                                        $schedule_later = true;
                                        $incr = 0;
                                        if(sizeof($groups) > 0)
                                        {
                                            $groups_array = (array) $groups;
                                            $remaining = array_slice($groups_array, 0, $key);
                                            $incr = $setting->message_time_interval * sizeof($remaining);
                                        }

                                        $time_to_upload = $time_to_upload + $incr;
                                    }

                                    if($schedule_later)
                                    {       
                                        DB::table('activity_log')->insert([
                                            "user_id"           =>  $setting->user_id,
                                            "instagram_id"      =>  $setting->instagram_id,
                                            "type"              =>  "posted",
                                            "group_id"          =>  $group->id,
                                            "item_pk"           =>  $latest_pk,
                                            "item_taken_at"     =>  $latest_pk_time,
                                            "schedule_later"    =>  true,
                                            "schedule_time"     =>  $time_to_upload,
                                            "message_done"      =>  false,
                                            "done"              =>  false,
                                            "created_at"        =>  time()
                                        ]);
                                    }
                                    else
                                    {
                                        $ig->share_on_group($latest_pk, $group->group_id);
                                            
                                        DB::table('activity_log')->insert([
                                            "user_id"           =>  $setting->user_id,
                                            "instagram_id"      =>  $setting->instagram_id,
                                            "type"              =>  "posted",
                                            "group_id"          =>  $group->id,
                                            "item_pk"           =>  $latest_pk,
                                            "item_taken_at"     =>  $latest_pk_time,
                                            "schedule_later"    =>  false,
                                            "message_done"      =>  true,
                                            "done"              =>  true,
                                            "created_at"        =>  time()
                                        ]);

                                        // post message

                                        if($can_send_message)
                                        {
                                            if($setting->messages != NULL)
                                            {
                                                $messages = explode(",", $setting->messages);
                                                $selected_message = array_rand($messages);
                                                $selected_message = $messages[$selected_message];

                                                // send message to group
                                                $ig->message($group->group_id, $selected_message);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                else
                {
                    echo $response;
                }
            }
        });

    }

    public function cron_semi()
    {
        DB::table('settings')
        ->where('status', true)
        ->where('mode', 'semi')
        // ->join('instagram_accounts', DB::raw('CAST(settings.instagram_id as INT)'),'=','instagram_accounts.id')
        // server
        ->join('instagram_accounts', 'settings.instagram_id','=','instagram_accounts.id')
        ->orderBy('settings.id')
        ->chunk(10, function($settings){
            // echo json_encode($settings);
            // die();
            foreach ($settings as $setting)
            {
                $ig = new instagram_library();
                $response = $ig->login($setting->username, $setting->password);
                if(strpos($response, "challenge_code_sent") !== false )
                {
                    // add to alerts
                    echo "unable to login";
                    continue;
                }
                else if($response == "logged_in")
                {   
                    // automatic

                    // limits
                    // per account
                    $max_messages = $setting->max_messages;
                    $can_send_message = false;

                    $done_message_count = DB::table('activity_log')
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('type', 'posted')
                        ->where('user_id', $setting->user_id)
                        ->where('done', true)
                        ->where('created_at', '>',strtotime("00:00"))
                        ->count();

                    if($max_messages > $done_message_count)
                    {
                        $can_send_message = true;
                    }

                    $max_comments = $setting->max_comments;
                    $can_send_comment = false;

                    if($setting->comment_by_bot)
                    {
                        $done_comment_count = DB::table('activity_log')
                            ->where('instagram_id', $setting->instagram_id)
                            ->where('type', 'shared')
                            ->where('user_id', $setting->user_id)
                            ->where('comment_done', true)
                            ->where('created_at', '>',strtotime("00:00"))
                            ->count();

                        if($max_comments > $done_comment_count)
                        {
                            $can_send_comment = true;
                        }
                    }

                    $max_likes = $setting->max_likes;
                    $can_send_like = false;

                    if($setting->like_by_bot)
                    {
                        $done_like_count = DB::table('activity_log')
                            ->where('instagram_id', $setting->instagram_id)
                            ->where('type', 'shared')
                            ->where('user_id', $setting->user_id)
                            ->where('like_done', true)
                            ->where('created_at', '>',strtotime("00:00"))
                            ->count();

                        if($max_likes > $done_like_count)
                        {
                            $can_send_like = true;
                        }
                    }

                    $max_actions_to_do = $setting->messages_count_condition;
                    
                    // get active groups
                    $groups = DB::table('message_groups')
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('user_id', $setting->user_id)
                        ->where('status', true)
                        ->get();

                    
                    if(sizeof($groups) > 0)
                    {   
                        // get latest pk from user feed

                        $info = $ig->get_profile_feed();
                        $latest_pk = 0;
                        $latest_pk_time = 0;
                        
                        if(array_key_exists(0, $info))
                        {
                            $latest_pk = $info[0]->getId();
                            $latest_pk_time = $info[0]->getTakenAt();
                        }

                        foreach ($groups as $key => $group)
                        {

                            if($setting->comments != null)
                            {
                                $comments = explode(",", $setting->comments);
                                $selected_comment = array_rand($comments);
                                $selected_comment = $comments[$selected_comment];
                            }
                            else
                            {
                                $selected_comment = "nice.";
                            }

                            $continue_likes_comments_in_group = false;

                            // ********************************

                            /**
                             * Upload Photo from own profile feed to active groups
                            */
                            // when upload a photo (via timeline or share), send post + message.
                                // apply X minutes of upload -> wait and time interval.

                            // check check if already posted on groups
                            $already_posted = DB::table('activity_log')
                                ->where('user_id', $setting->user_id)
                                ->where('instagram_id', $setting->instagram_id)
                                ->where('group_id', $group->id)
                                ->where('item_pk', $latest_pk)
                                ->where('type', 'posted')
                                ->exists();
                            
                            if(!$already_posted)
                            {
                                // see if it is latest item in profile

                                // get last item from activity log
                                $last_item = DB::table('activity_log')
                                    ->where('user_id', $setting->user_id)
                                    ->where('instagram_id', $setting->instagram_id)
                                    ->where('type', 'posted')
                                    ->orderBy('id', 'desc')
                                    ->first();

                                $post_this_item_in_group = false;
                                
                                if($last_item)
                                {
                                    // compare
                                    if($latest_pk_time > $last_item->item_taken_at)
                                    {
                                        $post_this_item_in_group = true;
                                    }
                                }
                                else
                                {
                                    $post_this_item_in_group = true;
                                }

                                if($post_this_item_in_group)
                                {
                                    $time_to_upload = time();

                                    $schedule_later = false;

                                    // apply time and interval (if any)
                                    if($setting->message_time != null)
                                    {
                                        $schedule_later = true;
                                        $time_to_upload = $time_to_upload + $setting->message_time;   
                                    }

                                    if($setting->message_time_interval != null)
                                    {
                                        if($setting->message_time_interval > 0)
                                        {
                                            $schedule_later = true;
                                            $incr = 0;
                                            if(sizeof($groups) > 0)
                                            {
                                                $groups_array = (array) $groups;
                                                $remaining = array_slice($groups_array, 0, $key);
                                                $incr = $setting->message_time_interval * sizeof($remaining);
                                            }

                                            $time_to_upload = $time_to_upload + $incr;
                                        }
                                    }

                                    if($schedule_later)
                                    {       
                                        DB::table('activity_log')->insert([
                                            "user_id"           =>  $setting->user_id,
                                            "instagram_id"      =>  $setting->instagram_id,
                                            "type"              =>  "posted",
                                            "group_id"          =>  $group->id,
                                            "item_pk"           =>  $latest_pk,
                                            "item_taken_at"     =>  $latest_pk_time,
                                            "schedule_later"    =>  true,
                                            "schedule_time"     =>  $time_to_upload,
                                            "message_done"      =>  false,
                                            "done"              =>  false,
                                            "created_at"        =>  time()
                                        ]);

                                        // schedule like / comment
                                        // TODO
                                        $continue_likes_comments_in_group = true;
                                    }
                                    else
                                    {
                                        $ig->share_on_group($latest_pk, $group->group_id);
                                            
                                        DB::table('activity_log')->insert([
                                            "user_id"           =>  $setting->user_id,
                                            "instagram_id"      =>  $setting->instagram_id,
                                            "type"              =>  "posted",
                                            "group_id"          =>  $group->id,
                                            "item_pk"           =>  $latest_pk,
                                            "item_taken_at"     =>  $latest_pk_time,
                                            "schedule_later"    =>  false,
                                            "message_done"      =>  true,
                                            "done"              =>  true,
                                            "created_at"        =>  time()
                                        ]);

                                        $continue_likes_comments_in_group = true;

                                        // post message

                                        if($can_send_message)
                                        {
                                            if($setting->messages != NULL)
                                            {
                                                $messages = explode(",", $setting->messages);
                                                $selected_message = array_rand($messages);
                                                $selected_message = $messages[$selected_message];

                                                // send message to group
                                                $ig->message($group->group_id, $selected_message);
                                            }
                                        }
                                    }
                                }

                            }

                            // ********************************

                            /**
                             * Like and Comment on all posts
                            */
                            // like and comment on all posts shared in the selected groups
                            // except blacklist
                            // skip own posts
                            // trigger like/comment based on @mention.
                            // within daily limits check

                            if($continue_likes_comments_in_group)
                            {
                                $group_messages = $ig->getThreadMessages($group->group_id)->getThread()->getItems();
                                // echo json_encode($group_messages);
                                // die();

                                
                                $done_actions_to_do = 0;

                                foreach ($group_messages as $g_msg)
                                {   
                                    if($max_actions_to_do > $done_actions_to_do)
                                    {
                                        // daily limits
                                        if($can_send_comment || $can_send_like)
                                        {
                                            $continue_with_action = false;

                                            // get shared messages
                                            if($g_msg->getItemType() == "media_share")
                                            {
                                                $g_msg_pk = $g_msg->getMediaShare()->getPk();
                                                $g_msg_time = $g_msg->getTimestamp();
                                                $g_msg_username = null;

                                                // get username from id
                                                $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                if($user_data != null)
                                                {
                                                    $g_msg_username = $user_data->getUser()->getUsername();
                                                }

                                                $g_msg_shared_user_id = $g_msg->getUserId();

                                                // check if already did action

                                                $already_did = DB::table('activity_log')
                                                    ->where("user_id", $setting->user_id)
                                                    ->where("instagram_id", $setting->instagram_id)
                                                    ->where("type", "shared")
                                                    ->where("group_id", $group->id)
                                                    ->where("item_pk", $g_msg_pk)
                                                    ->where("item_taken_at", $g_msg_time)
                                                    ->where("done",true)
                                                    ->exists();

                                                if($already_did)
                                                {
                                                    continue;
                                                }

                                                // skip if shared by own account
                                                if($g_msg_shared_user_id != $setting->instagram_pk)
                                                {
                                                    $continue_with_action = true;
                                                }
                                                else
                                                {
                                                    continue;
                                                }

                                                // skip if user in blacklist
                                                if($setting->blacklist != null)
                                                {
                                                    $blacklist = explode(",", $setting->blacklist);
                                                    if(in_array($g_msg_username, $blacklist))
                                                    {
                                                        $continue_with_action = false;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        $continue_with_action = true;
                                                    }
                                                }
                                            }
                                            
                                            // get @mention messages
                                            if($g_msg->getItemType() == "text")
                                            {
                                                // check if message has @ or like/comment in it.
                                                $text = $g_msg->getText();

                                                if(strpos($text, '@') !== false)
                                                {
                                                    // like pic of user mentioned with @

                                                    $data = explode("@", $text);     
                                                    $data = explode(" ", $data[1]);
                                                    $user_name = $data[0];
                                                    $user_pk = $ig->getUserIdfromName($user_name);
                                                    if($user_pk != null)
                                                    {
                                                        $feed = $ig->get_user_feed($user_pk);
                                                        if(array_key_exists("0", $feed))
                                                        {
                                                            $g_msg_pk = $feed[0]->getPk();
                                                            $g_msg_time = $g_msg->getTimestamp();
                                                            $g_msg_username = null;

                                                            // get username from id
                                                            $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                            if($user_data != null)
                                                            {
                                                                $g_msg_username = $user_data->getUser()->getUsername();
                                                            }

                                                            $g_msg_shared_user_id = $g_msg->getUserId();

                                                            // check if already did action

                                                            $already_did = DB::table('activity_log')
                                                                ->where("user_id", $setting->user_id)
                                                                ->where("instagram_id", $setting->instagram_id)
                                                                ->where("type", "shared")
                                                                ->where("group_id", $group->id)
                                                                ->where("item_pk", $g_msg_pk)
                                                                ->where("item_taken_at", $g_msg_time)
                                                                ->where("done",true)
                                                                ->exists();

                                                            if($already_did)
                                                            {
                                                                continue;
                                                            }

                                                            // skip if shared by own account
                                                            if($g_msg_shared_user_id != $setting->instagram_pk)
                                                            {
                                                                $continue_with_action = true;
                                                            }
                                                            else
                                                            {
                                                                continue;
                                                            }

                                                            // skip if user in blacklist
                                                            if($setting->blacklist != null)
                                                            {
                                                                $blacklist = explode(",", $setting->blacklist);
                                                                if(in_array($g_msg_username, $blacklist))
                                                                {
                                                                    $continue_with_action = false;
                                                                    continue;
                                                                }
                                                                else
                                                                {
                                                                    $continue_with_action = true;
                                                                }
                                                            }
                                                        }
                                                    }

                                                }

                                                if((strpos($text, 'like') !== false) && (strpos($text, '@') === false))
                                                {
                                                    // like user pic who posted message

                                                    $feed = $ig->get_user_feed($g_msg->getUserId());
                                                    if(array_key_exists("0", $feed))
                                                    {
                                                        $g_msg_pk = $feed[0]->getPk();
                                                        $g_msg_time = $g_msg->getTimestamp();
                                                        
                                                        $g_msg_username = null;

                                                        // get username from id
                                                        $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                        if($user_data != null)
                                                        {
                                                            $g_msg_username = $user_data->getUser()->getUsername();
                                                        }

                                                        $g_msg_shared_user_id = $g_msg->getUserId();

                                                        // check if already did action

                                                        $already_did = DB::table('activity_log')
                                                            ->where("user_id", $setting->user_id)
                                                            ->where("instagram_id", $setting->instagram_id)
                                                            ->where("type", "shared")
                                                            ->where("group_id", $group->id)
                                                            ->where("item_pk", $g_msg_pk)
                                                            ->where("item_taken_at", $g_msg_time)
                                                            ->where("done",true)
                                                            ->exists();

                                                        if($already_did)
                                                        {
                                                            continue;
                                                        }

                                                        // skip if shared by own account
                                                        if($g_msg_shared_user_id != $setting->instagram_pk)
                                                        {
                                                            $continue_with_action = true;
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }

                                                        // skip if user in blacklist
                                                        if($setting->blacklist != null)
                                                        {
                                                            $blacklist = explode(",", $setting->blacklist);
                                                            if(in_array($g_msg_username, $blacklist))
                                                            {
                                                                $continue_with_action = false;
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                $continue_with_action = true;
                                                            }
                                                        }
                                                    } 
                                                }
                                            }
                                            
                                            if($continue_with_action)
                                            {
                                                $done_actions_to_do++;
                                                $like_sent = true;
                                                $comment_sent = false;

                                                if($can_send_like)
                                                {
                                                    // like post
                                                    $ig->like_post($g_msg_pk);
                                                    $like_sent = true;
                                                }
                                                if($can_send_comment)
                                                {
                                                    // comment post
                                                    $ig->comment_post($g_msg_pk, $selected_comment);
                                                    $comment_sent = true;
                                                }

                                                // save to activity
                                                DB::table('activity_log')->insert([
                                                    "user_id"           =>  $setting->user_id,
                                                    "instagram_id"      =>  $setting->instagram_id,
                                                    "type"              =>  "shared",
                                                    "group_id"          =>  $group->id,
                                                    "item_pk"           =>  $g_msg_pk,
                                                    "item_taken_at"     =>  $g_msg_time,
                                                    "like_done"         =>  $like_sent,
                                                    "comment_done"      =>  $comment_sent,
                                                    "done"              =>  true,
                                                    "created_at"        =>  time()
                                                ]);
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo $response;
                }
            }
        });

    }

    public function cron_manual()
    {
        DB::table('settings')
        ->where('status', true)
        ->where('mode', 'manual')
        // ->join('instagram_accounts', DB::raw('CAST(settings.instagram_id as INT)'),'=','instagram_accounts.id')
        // server
        ->join('instagram_accounts', 'settings.instagram_id','=','instagram_accounts.id')
        ->orderBy('settings.id')
        ->chunk(10, function($settings){
            foreach ($settings as $setting)
            {
                $ig = new instagram_library();
                $response = $ig->login($setting->username, $setting->password);
                if(strpos($response, "challenge_code_sent") !== false )
                {
                    // add to alerts
                    echo "unable to login";
                    continue;
                }
                else if($response == "logged_in")
                {   
                    // automatic

                    // limits
                    // per account
                    $max_messages = $setting->max_messages;
                    $can_send_message = false;

                    $done_message_count = DB::table('activity_log')
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('type', 'posted')
                        ->where('user_id', $setting->user_id)
                        ->where('done', true)
                        ->where('created_at', '>',strtotime("00:00"))
                        ->count();

                    if($max_messages > $done_message_count)
                    {
                        $can_send_message = true;
                    }

                    $max_comments = $setting->max_comments;
                    $can_send_comment = false;

                    if($setting->comment_by_bot)
                    {
                        $done_comment_count = DB::table('activity_log')
                            ->where('instagram_id', $setting->instagram_id)
                            ->where('type', 'shared')
                            ->where('user_id', $setting->user_id)
                            ->where('comment_done', true)
                            ->where('created_at', '>',strtotime("00:00"))
                            ->count();

                        if($max_comments > $done_comment_count)
                        {
                            $can_send_comment = true;
                        }
                    }

                    $max_likes = $setting->max_likes;
                    $can_send_like = false;

                    if($setting->like_by_bot)
                    {
                        $done_like_count = DB::table('activity_log')
                            ->where('instagram_id', $setting->instagram_id)
                            ->where('type', 'shared')
                            ->where('user_id', $setting->user_id)
                            ->where('like_done', true)
                            ->where('created_at', '>',strtotime("00:00"))
                            ->count();

                        if($max_likes > $done_like_count)
                        {
                            $can_send_like = true;
                        }
                    }

                    $max_actions_to_do = $setting->messages_count_condition;
                    
                    // get active groups
                    $groups = DB::table('message_groups')
                        ->where('instagram_id', $setting->instagram_id)
                        ->where('user_id', $setting->user_id)
                        ->where('status', true)
                        ->get();

                    
                    if(sizeof($groups) > 0)
                    {   
                        // get latest pk from user feed

                        $info = $ig->get_profile_feed();
                        $latest_pk = 0;
                        $latest_pk_time = 0;
                        
                        if(array_key_exists(0, $info))
                        {
                            $latest_pk = $info[0]->getId();
                            $latest_pk_time = $info[0]->getTakenAt();
                        }

                        foreach ($groups as $key => $group)
                        {

                            if($setting->comments != null)
                            {
                                $comments = explode(",", $setting->comments);
                                $selected_comment = array_rand($comments);
                                $selected_comment = $comments[$selected_comment];
                            }
                            else
                            {
                                $selected_comment = "nice.";
                            }

                            $continue_likes_comments_in_group = true;

                            // ********************************

                            /**
                             * Like and Comment on all posts
                            */
                            // like and comment on all posts shared in the selected groups
                            // except blacklist
                            // skip own posts
                            // trigger like/comment based on @mention.
                            // within daily limits check

                            if($continue_likes_comments_in_group)
                            {
                                $group_messages = $ig->getThreadMessages($group->group_id)->getThread()->getItems();
                                // echo json_encode($group_messages);
                                // die();

                                
                                $done_actions_to_do = 0;

                                foreach ($group_messages as $g_msg)
                                {   
                                    if($max_actions_to_do > $done_actions_to_do)
                                    {
                                        // daily limits
                                        if($can_send_comment || $can_send_like)
                                        {
                                            $continue_with_action = false;

                                            // get shared messages
                                            if($g_msg->getItemType() == "media_share")
                                            {
                                                $g_msg_pk = $g_msg->getMediaShare()->getPk();
                                                $g_msg_time = $g_msg->getTimestamp();
                                                $g_msg_username = null;

                                                // get username from id
                                                $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                if($user_data != null)
                                                {
                                                    $g_msg_username = $user_data->getUser()->getUsername();
                                                }

                                                $g_msg_shared_user_id = $g_msg->getUserId();

                                                // check if already did action

                                                $already_did = DB::table('activity_log')
                                                    ->where("user_id", $setting->user_id)
                                                    ->where("instagram_id", $setting->instagram_id)
                                                    ->where("type", "shared")
                                                    ->where("group_id", $group->id)
                                                    ->where("item_pk", $g_msg_pk)
                                                    ->where("item_taken_at", $g_msg_time)
                                                    ->where("done",true)
                                                    ->exists();

                                                if($already_did)
                                                {
                                                    continue;
                                                }

                                                // skip if shared by own account
                                                if($g_msg_shared_user_id != $setting->instagram_pk)
                                                {
                                                    $continue_with_action = true;
                                                }
                                                else
                                                {
                                                    continue;
                                                }

                                                // skip if user in blacklist
                                                if($setting->blacklist != null)
                                                {
                                                    $blacklist = explode(",", $setting->blacklist);
                                                    if(in_array($g_msg_username, $blacklist))
                                                    {
                                                        $continue_with_action = false;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        $continue_with_action = true;
                                                    }
                                                }
                                            }
                                            
                                            // get @mention messages
                                            if($g_msg->getItemType() == "text")
                                            {
                                                // check if message has @ or like/comment in it.
                                                $text = $g_msg->getText();

                                                if(strpos($text, '@') !== false)
                                                {
                                                    // like pic of user mentioned with @

                                                    $data = explode("@", $text);     
                                                    $data = explode(" ", $data[1]);
                                                    $user_name = $data[0];
                                                    $user_pk = $ig->getUserIdfromName($user_name);
                                                    if($user_pk != null)
                                                    {
                                                        $feed = $ig->get_user_feed($user_pk);
                                                        if(array_key_exists("0", $feed))
                                                        {
                                                            $g_msg_pk = $feed[0]->getPk();
                                                            $g_msg_time = $g_msg->getTimestamp();
                                                            $g_msg_username = null;

                                                            // get username from id
                                                            $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                            if($user_data != null)
                                                            {
                                                                $g_msg_username = $user_data->getUser()->getUsername();
                                                            }

                                                            $g_msg_shared_user_id = $g_msg->getUserId();

                                                            // check if already did action

                                                            $already_did = DB::table('activity_log')
                                                                ->where("user_id", $setting->user_id)
                                                                ->where("instagram_id", $setting->instagram_id)
                                                                ->where("type", "shared")
                                                                ->where("group_id", $group->id)
                                                                ->where("item_pk", $g_msg_pk)
                                                                ->where("item_taken_at", $g_msg_time)
                                                                ->where("done",true)
                                                                ->exists();

                                                            if($already_did)
                                                            {
                                                                continue;
                                                            }

                                                            // skip if shared by own account
                                                            if($g_msg_shared_user_id != $setting->instagram_pk)
                                                            {
                                                                $continue_with_action = true;
                                                            }
                                                            else
                                                            {
                                                                continue;
                                                            }

                                                            // skip if user in blacklist
                                                            if($setting->blacklist != null)
                                                            {
                                                                $blacklist = explode(",", $setting->blacklist);
                                                                if(in_array($g_msg_username, $blacklist))
                                                                {
                                                                    $continue_with_action = false;
                                                                    continue;
                                                                }
                                                                else
                                                                {
                                                                    $continue_with_action = true;
                                                                }
                                                            }
                                                        }
                                                    }

                                                }

                                                if((strpos($text, 'like') !== false) && (strpos($text, '@') === false))
                                                {
                                                    // like user pic who posted message

                                                    $feed = $ig->get_user_feed($g_msg->getUserId());
                                                    if(array_key_exists("0", $feed))
                                                    {
                                                        $g_msg_pk = $feed[0]->getPk();
                                                        $g_msg_time = $g_msg->getTimestamp();
                                                        
                                                        $g_msg_username = null;

                                                        // get username from id
                                                        $user_data = $ig->getNamefromID($g_msg->getUserId());
                                                        if($user_data != null)
                                                        {
                                                            $g_msg_username = $user_data->getUser()->getUsername();
                                                        }

                                                        $g_msg_shared_user_id = $g_msg->getUserId();

                                                        // check if already did action

                                                        $already_did = DB::table('activity_log')
                                                            ->where("user_id", $setting->user_id)
                                                            ->where("instagram_id", $setting->instagram_id)
                                                            ->where("type", "shared")
                                                            ->where("group_id", $group->id)
                                                            ->where("item_pk", $g_msg_pk)
                                                            ->where("item_taken_at", $g_msg_time)
                                                            ->where("done",true)
                                                            ->exists();

                                                        if($already_did)
                                                        {
                                                            continue;
                                                        }

                                                        // skip if shared by own account
                                                        if($g_msg_shared_user_id != $setting->instagram_pk)
                                                        {
                                                            $continue_with_action = true;
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }

                                                        // skip if user in blacklist
                                                        if($setting->blacklist != null)
                                                        {
                                                            $blacklist = explode(",", $setting->blacklist);
                                                            if(in_array($g_msg_username, $blacklist))
                                                            {
                                                                $continue_with_action = false;
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                $continue_with_action = true;
                                                            }
                                                        }
                                                    } 
                                                }
                                            }
                                            
                                            if($continue_with_action)
                                            {
                                                $done_actions_to_do++;
                                                $like_sent = true;
                                                $comment_sent = false;

                                                if($can_send_like)
                                                {
                                                    // like post
                                                    $ig->like_post($g_msg_pk);
                                                    $like_sent = true;
                                                }
                                                if($can_send_comment)
                                                {
                                                    // comment post
                                                    $ig->comment_post($g_msg_pk, $selected_comment);
                                                    $comment_sent = true;
                                                }

                                                // save to activity
                                                DB::table('activity_log')->insert([
                                                    "user_id"           =>  $setting->user_id,
                                                    "instagram_id"      =>  $setting->instagram_id,
                                                    "type"              =>  "shared",
                                                    "group_id"          =>  $group->id,
                                                    "item_pk"           =>  $g_msg_pk,
                                                    "item_taken_at"     =>  $g_msg_time,
                                                    "like_done"         =>  $like_sent,
                                                    "comment_done"      =>  $comment_sent,
                                                    "done"              =>  true,
                                                    "created_at"        =>  time()
                                                ]);
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo $response;
                }
            }
        });

    }

    // post scheduled post + messages

    public function schedule_posts()
    {
        $posts = DB::table('activity_log')
            ->where("schedule_later",true)
            ->where('type', 'posted')
            ->where("done",false)
            ->get();

        foreach ($posts as $post)
        {
            if(time() > $post->schedule_time)
            {
                // post in group

                // login

                $setting = DB::table('instagram_accounts')->where('user_id', $post->user_id)->first();

                $ig = new instagram_library();
                $response = $ig->login($setting->username, $setting->password);
                if(strpos($response, "challenge_code_sent") !== false )
                {
                    // add to alerts
                    echo "unable to login";
                    continue;
                }
                else if($response == "logged_in")
                {   
                    $thread = DB::table('message_groups')->where('id', $post->group_id)->first();
                    // post
                    $ress = $ig->share_on_group($post->item_pk, $thread->group_id);

                    // update log

                    DB::table('activity_log')->where('id', $post->id)->update([
                        'done'  =>  true
                    ]);
                }
            }
        }
        
    }
    // like comment (when semi mode)
}
