<?php

namespace App\Http\Controllers;

use App\Libraries\Instagram;
use File;
use App\User;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Libraries\Instagram as instagram_library;
use App\Libraries\Utility as app_library;

class UserController extends Controller
{
    public function index()
    {
        // $accounts = 
        $accounts = DB::table('instagram_accounts')->where('user_id',Auth::id())->get();
        return view('user.index')->with('accounts', $accounts);
    }

    public function getAccounts()
    {
        $get_accounts = DB::table('instagram_accounts')->where('user_id', Auth::id())->get();

        $accounts = [];

        foreach ($get_accounts as $item)
        {
            $setting = DB::table('settings')->where('instagram_id', $item->id)->first();

            $groups = DB::table('message_groups')->where('instagram_id', $item->id)->get();

            if($setting)
            {
                $account = [
                    "id"        =>  $item->id,
                    "username"  =>  $item->instagram_username,
                    "pic"       =>  $item->instagram_pic,
                    "followers" =>  $item->instagram_followers,
                    "following" =>  $item->instagram_following,
                    "posts"     =>  $item->instagram_posts,
                    "proxy"     =>  $item->proxy,

                    "enabled"   =>  $setting->status,
                    "like"      =>  $setting->like_by_bot,
                    "comment"   =>  $setting->comment_by_bot,

                    "mode"      =>  $setting->mode,
                    "interval"  =>  $setting->message_time_interval,
                    "time"      =>  $setting->message_time,

                    "max_messages"  =>  $setting->max_messages,
                    "max_likes"     =>  $setting->max_likes,
                    "max_comments"  =>  $setting->max_comments,

                    "messages"      =>  $setting->messages,
                    "comments"      =>  $setting->comments,
                    "blacklist"     =>  $setting->blacklist,

                    "groups"        =>  $groups,
                    "count"         =>  $setting->messages_count_condition

                ];

                array_push($accounts, $account);
            }
        }

        return view('user.accounts')->with('accounts', $accounts);
    }

    public function getEditAccounts(Request $request){
        $get_accounts = DB::table('instagram_accounts')->where('user_id', Auth::id())->get();
        return view('user.edit_accounts')->with('accounts', $get_accounts);
    }

    public function getEditInstaAccounts(Request $request){
        try
        {
            DB::table('instagram_accounts')->where('id', $request->input('id'))->update([
                // 'mode'          =>  $request->input('settings_mode'),
                'username'  =>  $request->input('username'),
                'proxy'  =>  $request->input('proxy'),
                'password'     =>  $request->input('password')
            ]);
            return redirect('/user/accounts/edit')->with('status.success',"Saved");
        }
        catch(Exception $ex)
        {
            return redirect('/user/accounts/edit')->with('status.error',"Something went wrong.");
        }
    }
    public function updateUserInfo(Request $request){        
        try
        {
            DB::table('users')->where('id', Auth::id())->update([
                'email'  =>  $request->input('email'),
                'password'     =>  bcrypt($request->input('password'))
            ]);
            return redirect('/user/settings')->with('status.success',"Saved");
        }
        catch(Exception $ex)
        {
            return redirect('/user/settings')->with('status.error',"Something went wrong.");
        }
    }

    public function updateAccountSettings(Request $request)
    {
        try
        {
            //return $request->all();
            DB::table('settings')->where('user_id', Auth::id())->where('instagram_id', $request->input('instagram_id'))->update([
                // 'mode'          =>  $request->input('settings_mode'),
                'message_time'  =>  $request->input('settings_time'),
                'max_messages'  =>  $request->input('settings_max_messages'),
                'max_likes'     =>  $request->input('settings_max_likes'),
                'max_comments'  =>  $request->input('settings_max_comments'),
                'message_time_interval' =>  $request->input('settings_time_interval'),
                'messages_count_condition'  =>  $request->input('settings_count'),
            ]);

            DB::table('instagram_accounts')->where('user_id', Auth::id())->where('id', $request->input('instagram_id'))->update([
                'proxy' => $request->input('settings_proxy')
            ]);
            
            return redirect('/user/accounts?accounts=1')->with('status.success',"Saved")->with('status.success',"Saved");
        }
        catch(Exception $ex)
        {
            return redirect('/user/accounts?accounts=1')->with('status.error',"Something went wrong.");
        }
    }

    public function updateAccountMessages(Request $request)
    {
        DB::table('settings')->where('instagram_id', $request->input('id'))->update([
            'messages'  =>  $request->input('tags')
        ]);

        return "done";
    }

    public function updateAccountComments(Request $request)
    {
        DB::table('settings')->where('instagram_id', $request->input('id'))->update([
            'comments'  =>  $request->input('tags')
        ]);

        return "done";
    }

    public function updateAccountBlacklist(Request $request)
    {
        DB::table('settings')->where('instagram_id', $request->input('id'))->update([
            'blacklist'  =>  $request->input('tags')
        ]);

        return "done";
    }

    public function updateGroups(Request $request)
    {
        $value = $request->input('status');
		
		// return $value;
        if($value == "true")
		{
			$value = 1;
		}
		if($value == "false")
		{
			$value = 0;
        }
    
       
        DB::table('message_groups')->where('user_id', Auth::id())->where('id', $request->input('id'))->update([
          
            'status'    => $value
        ]);

        return "done";
    }

    public function updateGeneral(Request $request)
    {
        $value = $request->input('value');
		
		if($value == "true")
		{
			$value = 1;
		}
		else
		{
			$value = 0;
        }
        
        $name = $request->input('name');
      
        DB::table('settings')->where('instagram_id', $request->input('insta_id'))->update([
            $name =>    $value
        ]);

        return "done";
    }

    public function updateGeneralMode(Request $request)
    {
        $value = $request->input('value');
        
        $name = $request->input('name');
      
        DB::table('settings')->where('instagram_id', $request->input('insta_id'))->update([
            $name =>    $value
        ]);

        return "done";
    }

    public function getProfile()
    {
        return view('user.profile');
    }

    public function getPayment()
    {
        return view('user.payment');
    }

    public function getSettings()
    {
        $user = DB::table('users')->where('id', Auth::id())->first();
        return view('user.settings')->with('user', $user);
    }

    public function updateTimezone(Request $request)
    {
        DB::table('users')->where('id', Auth::id())->update([
            'timezone'  =>  $request->input('timezone')
        ]);

        return redirect('/user/settings');
    }


    public function getsupport()
    {
        return redirect('tickets');
    }
    public function postDeleteInstagram(Request $request){

        DB::table('instagram_accounts')->where('id',$request->input('id'))->delete();
        DB::table('settings')->where('instagram_id',$request->input('id'))->delete();
        DB::table('activity_log')->where('instagram_id',$request->input('id'))->delete();
        DB::table('message_groups')->where('instagram_id',$request->input('id'))->delete();
        return redirect('/user/accounts');

    }

    public function postAddInstagram(Request $request)
    {
        try
        {
            $instagram = new instagram_library();

            $proxy_i = explode(":", $request->input("proxy"));
            if(sizeof($proxy_i) > 3)
            {
                $proxy_c = "http://".$proxy_i[2].":".$proxy_i[3]."@".$proxy_i[0].":".$proxy_i[1];

            }
            elseif(sizeof($proxy_i)==1 && !empty($proxy_i[0]))
            {
                return redirect('/user/accounts?error=1')->with('status.error',"Please check your proxy.");
            }
            else
            {
                $proxy_c = $request->input('proxy');
            }
            $response = $instagram->login(
                $request->input('username'),
                $request->input('password'),
                $proxy_c,
                $request->input('verificationCode')
            );
            
            // return json_encode($response);

            if($response['status'] == Instagram::SEND_VERIFICATIONS_CODE)
            {
//                $response = explode(":",$response);

                return redirect('/user/accounts?verify=1')
                    ->with('status.info','Verification code sent')
                    ->with('proxy', $proxy_c)
                    ->with('username', $request->input('username'))
                    ->with('password', $request->input('password'));
            }
            else if($response['status'] == Instagram::AUTHENTICATION_SUCCESS)
            {
                $instagram = new instagram_library();
                $response = $instagram->login(
                    $request->input('username'),
                    $request->input('password'),
                    $request->input('password'),
                    $proxy_c,
                    $request->input('verificationCode'));

                if($response['status'] == Instagram::AUTHENTICATION_SUCCESS)
                {
                    $info = $instagram->profileInfo();

                    // save profile data to table
                    $user = DB::table('instagram_accounts')->insertGetId([
                        'user_id'               =>  Auth::id(),
                        'username'              =>  $request->input('username'),
                        'password'              =>  $request->input('password'),
                        'proxy'                 =>  $request->input('proxy'),
                        'instagram_pk'          =>  $info->getUser()->getPk(),
                        'instagram_bio'         =>  $info->getUser()->getBiography(),
                        'instagram_full_name'   =>  $info->getUser()->getFull_name(),
                        'instagram_username'    =>  $info->getUser()->getUsername(),
                        'instagram_followers'   =>  $info->getUser()->getFollower_count(),
                        'instagram_following'   =>  $info->getUser()->getFollowing_count(),
                        'instagram_posts'       =>  $info->getUser()->getMedia_count(),
                        'instagram_pic'         =>  $info->getUser()->getHd_profile_pic_versions()[0]->getUrl(),
                        'instagram_url'         =>  $info->getUser()->getExternal_url()
                    ]);

                    // set active session
                    session(['active_instagram_account' => $user]);

                    // save to settings
                    DB::table('settings')->insert([
                        'instagram_id'      =>  $user,
                        'user_id'           =>  Auth::id()
                    ]);

                    // add groups
                    $data = $instagram->messages();

                    $groups = [];

                    foreach($data->getInbox()->getThreads() as $thread)
                    {
                        if($thread->getIsGroup())
                        {
                            $item = [
                                "id"    =>  $thread->getThreadId(),
                                "name"  =>  $thread->getThreadTitle()
                            ];
                            array_push($groups, $item);
                        }
                    }

                    foreach ($groups as $group)
                    {
                        DB::table('message_groups')->insert([
                            "user_id"       =>  Auth::id(),
                            "instagram_id"  =>  $user,
                            "group_id"      =>  $group['id'],
                            "group_name"    =>  $group['name']
                        ]);
                    }
                
                    return redirect(url('/user/accounts'));
                }
                else
                {
                    return redirect('/user/accounts?error=1')->with('status.error',"Please try Again with different proxy or wait few minutes.");
                }
            }
            else
            {
                return redirect('/user/accounts?error=1')->with('status.error',$response)->withInput();
                return $response;
            }
        }
        catch(Exception $ex)
        {
            return redirect('/user/accounts?error=1')->with('status.error',"Something went wrong")->withInput();
            dd($ex);
            return $ex;
        }
    }

    public function postVerifyInstagramAccount(Request $request)
    {
        try
        {
            $instagram = new instagram_library();

            $response = $instagram->login($request->input('username'), $request->input('password'), $request->input('proxy'), $request->input('code'));

            if($response == "challenge_code_accepted")
            {
                // TODO

                sleep(20);

                $instagram = new instagram_library();
                $response = $instagram->login($request->input('username'), $request->input('password'), $request->input('proxy'));
                if($response == "logged_in")
                {
                    $info = $instagram->profileInfo();

                    // save profile data to table
                    $user = DB::table('instagram_accounts')->insertGetId([
                        'user_id'               =>  Auth::id(),
                        'username'              =>  $request->input('username'),
                        'password'              =>  $request->input('password'),
                        'proxy'              =>  $request->input('proxy'),
                        'instagram_pk'          =>  $info->getUser()->getPk(),
                        'instagram_bio'         =>  $info->getUser()->getBiography(),
                        'instagram_full_name'   =>  $info->getUser()->getFull_name(),
                        'instagram_username'    =>  $info->getUser()->getUsername(),
                        'instagram_followers'   =>  $info->getUser()->getFollower_count(),
                        'instagram_following'   =>  $info->getUser()->getFollowing_count(),
                        'instagram_posts'       =>  $info->getUser()->getMedia_count(),
                        'instagram_pic'         =>  $info->getUser()->getProfile_pic_url(),
                        'instagram_url'         =>  $info->getUser()->getExternal_url()
                    ]);

                    // set active session
                    session(['active_instagram_account' => $user]);

                    // save to settings
                    DB::table('settings')->insert([
                        'instagram_id'      =>  $user,
                        'user_id'           =>  Auth::id()
                    ]);

                    // add groups
                    $data = $instagram->messages();

                    $groups = [];

                    foreach($data->getInbox()->getThreads() as $thread)
                    {
                        if($thread->getIsGroup())
                        {
                            $item = [
                                "id"    =>  $thread->getThreadId(),
                                "name"  =>  $thread->getThreadTitle()
                            ];
                            array_push($groups, $item);
                        }
                    }

                    foreach ($groups as $group)
                    {
                        DB::table('message_groups')->insert([
                            "user_id"       =>  Auth::id(),
                            "instagram_id"  =>  $user,
                            "group_id"      =>  $group['id'],
                            "group_name"    =>  $group['name']
                        ]);
                    }
                
                    return redirect(url('/user/accounts'));
                }
                else
                {
                    return redirect('/user/accounts?error=1')->with('status.error',"Please try Again with different proxy or wait few minutes.");
                }
                
            }
            else
            {
                return redirect('/user/accounts?error=1')->with('status.error',"Inavlid Verification Code")->withInput();
            }
        }
        catch(\Exception $ex)
        {
            return redirect('/user/accounts?error=1')->with('status.error',$ex->getMessage())->withInput();
        }
    }

    public function getNewGroupList(Request $request)
    {
        $account = DB::table('instagram_accounts')->where('id', $request->input('id'))->first();

        if($account != null)
        {
            $instagram = new instagram_library();
                
            $response = $instagram->login($account->username, $account->password);
            
            if(strpos($response, "challenge_code_sent") !== false )
            {
                // add to alerts
                return "unable to login";
            }
            else if($response == "logged_in")
            {

                // add groups
                $data = $instagram->messages();

                $groups = [];

                foreach($data->getInbox()->getThreads() as $thread)
                {
                    if($thread->getIsGroup())
                    {
                        // check if not already added
                        
                        $exists = DB::table('message_groups')->where('user_id', Auth::id())->where('group_id', $thread->getThreadId())->exists();
                        if(!$exists)
                        {
                            $item = [
                                "id"    =>  $thread->getThreadId(),
                                "name"  =>  $thread->getThreadTitle()
                            ];
                            array_push($groups, $item);
                        }
                    }
                }

                foreach ($groups as $group)
                {
                    DB::table('message_groups')->insert([
                        "user_id"       =>  Auth::id(),
                        "instagram_id"  =>  $account->id,
                        "group_id"      =>  $group['id'],
                        "group_name"    =>  $group['name']
                    ]);
                }
            }
        }

        return redirect(url("/user/accounts?accounts=".$request->input('id')));
    }

    public function markReadAll()
    {
        DB::table('notifications')->where('user_id', Auth::id())->update([
            "had_read"  =>  true
        ]);
        return redirect(url("/user"));
    }
}