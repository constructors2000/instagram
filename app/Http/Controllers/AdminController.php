<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function index()
    {
        $users = DB::table('users')->orderBy('id', 'asc')->get();
		
		$data = [];

		foreach ($users as $user_item) {
            
            $get_levels = DB::table('user_role')->where('user_id',$user_item->id)->first();

            
            if($get_levels)
            {
                $continue = true;
                // hide admin users
                if(in_array("0", json_decode($get_levels->levels))){
                    $continue = false;
                }
                
                if($continue)
                {
                    $levels = [];

                    foreach (json_decode($get_levels->levels) as $level_item) {
                        if($level_item != null && $level_item != "")
                        {
                            $level_item_name = DB::table('levels')->where('id', '!=', '1')->where('id',$level_item)->first();
                            if($level_item_name != null){
                                $item = [
                                    'id'	=>	$level_item_name->id,
                                    'name'	=>	$level_item_name->name
                                ];
                                array_push($levels, $item);
                            }
                        }
                    }

                    $item = [
                        'id'		=>	$user_item->id,
                        'name'		=>	$user_item->first_name." ".$user_item->last_name,
                        'email'		=>	$user_item->email,
                        'timezone'	=>	$user_item->timezone,
                        'created_by'=>	$user_item->created_by,
                        'created_at'=>	$user_item->created_at,
                        'levels'	=>	$levels
                    ];

                    array_push($data, $item);
                }
            }
		}

        return view('admin.overview')->with('users', $data);
    }

    public function dashboard()
    {
        $data = [
            'total' =>  '0',
            'free'  =>  '0',
            'basic' =>  '0',
            'pro'   =>  '0'
        ];

        $users = DB::table('users')->get();
        
        $total  = 0;
        $free   = 0;
        $basic  = 0;
        $pro    = 0;

        foreach ($users as $user) {

            $role = DB::table('user_role')->where('user_id', $user->id)->first();
            if($role)
            {
                if(!in_array("0", json_decode($role->levels))){
                    $total++;
                    if($user->created_by == "admin"){
                        $free++;
                    }
                    if($user->created_by == "direct")
                    {
                        if(in_array("1", json_decode($role->levels))){
                            $basic++;
                        }
                        if(in_array("2", json_decode($role->levels))){
                            $pro++;
                        }
                    }
                }
            }
        }

        $data = [
            'total' =>  $total,
            'free'  =>  $free,
            'basic' =>  $basic,
            'pro'   =>  $pro
        ];

        return view('admin.dashboard')->with('data', $data);
    }

    public function getUsers()
    {
        return view('admin.users');
    }

    public function getAddUser()
    {
        $levels = DB::table('levels')->where('enabled', true)->where('id','!=','1')->get();
        return view('admin.adduser')->with('levels',$levels);
    }

    public function postAddUser(Request $request)
    {
        $isValid =  Validator::make(Input::all(), [
            'first_name'    => 'required|string|min:3|max:10',
            'last_name'     => 'required|string|min:3|max:10',
            'email'         => 'required|string|email|min:5|max:50|unique:users',
            'password'      => 'required|string|min:6|max:20'
        ]);

        if($isValid->fails()){
            return redirect('/admin/users/add')->withErrors($isValid)->withInput();
        }
        else
        {
            // create user
            $id = DB::table('users')->insertGetId(
                [
                    'first_name'	=>	$request->input('first_name'),
                    'last_name'     =>  $request->input('last_name'),
                    'email'	        =>	$request->input('email'),
                    'password'	    =>	bcrypt($request->input('password')),
                    'enabled'       =>  true,
                    'created_by'    => 'admin'
                ]
            );
            
            // create user role
            $query = DB::table('user_role')->insert([
                'levels'    => '["1","'.$request->input('levels').'"]', // by default user with 'basic' plan
                'user_id'	=>	$id
            ]);

            return redirect('/admin');
        }
    }

    public function getEditUser(int $id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        $level = DB::table('user_role')->where('user_id',$user->id)->first();
		$selected_levels = json_decode($level->levels);
		$levels = DB::table('levels')->where('id','!=','1')->where('enabled', true)->get();
        return view('admin.edituser')->with('user', $user)->with('levels', $levels)->with('selected_levels',$selected_levels);
    }

    public function postEditUser(Request $request)
    {
        $isValid =  Validator::make(Input::all(), [
            'first_name'    => 'required|string|min:3|max:10',
            'last_name'     => 'required|string|min:3|max:10'
        ]);

        if($isValid->fails()){
            return redirect('/admin/users/edit/'.$request->input('id'))->withErrors($isValid)->withInput();
        }
        else
        {
            if($request->input('password') != null)
            {
                // update user
                DB::table('users')->where('id', $request->input('id'))->update(
                    [
                        'first_name'	=>	$request->input('first_name'),
                        'last_name'     =>  $request->input('last_name'),
                        'password'	    =>	bcrypt($request->input('password'))
                    ]
                );
            }
            else
            {
                DB::table('users')->where('id', $request->input('id'))->update(
                    [
                        'first_name'	=>	$request->input('first_name'),
                        'last_name'     =>  $request->input('last_name')
                    ]
                );
            }
            
            // create user role
            $query = DB::table('user_role')->where('user_id', $request->input('id'))->update([
                'levels'    => '["1","'.$request->input('levels').'"]'
            ]);

            return redirect('/admin');
        }
    }

    public function getDeleteUser(int $id)
    {
        DB::table('users')->where('id',$id)->delete();
		$query = DB::table('user_role')->where('user_id',$id)->delete();
		return redirect('/admin');
    }

    public function getLevels()
    {
        $levels = DB::table('levels')->orderBy('id','asc')->where('id','!=',1)->get();
        return view('admin.levels.all')->with('levels',$levels);
    }

    public function getAddLevel()
    {
        return view('admin.levels.add');
    }

    public function postAddLevel(Request $request)
    {
        try{
            // validate
            $isValid =  Validator::make(Input::all(), [
                'name'                =>  'required|string|min:3|max:10',
                'price'               =>  'required|string|min:1|max:7',
                'instagram_accounts'  =>  'required|string|min:1|max:2',
                'features'            =>  'required',
                'automation_speed'    =>  'required|string',
                'support'             =>  'required',
                'valid_time'          =>  'required|string|min:1|max:3'
            ]);
            
            if($isValid->fails()){
                return redirect('/admin/levels/add')->withErrors($isValid)->withInput();
            }
            else{
                DB::table('levels')->insert([
                    'name'  =>  $request->input('name'),
                    'price' =>  $request->input('price'),
                    'instagram_accounts'    =>  $request->input('instagram_accounts'),
                    'features'              =>  json_encode($request->input('features')),
                    'automation_speed'      =>  $request->input('automation_speed'),
                    'support'               =>  json_encode($request->input('support')),
                    'valid_time'            =>  $request->input('valid_time'),
                    'stripe_plan_id'        =>  $request->input('stripe_plan_id'),
                    'stripe_plan_name'      =>  $request->input('stripe_plan_name')
                ]);
                return redirect('/admin/levels');
            }
        }
        catch(\Exception $ex){
            return redirect('/admin/levels/add')->with('status.error', 'Something went wrong, try again later');
        }
    }

    public function getEditLevel($id)
    {
        $level = DB::table('levels')->where('id', $id)->first();
        return view('admin.levels.edit')->with('level', $level);
    }

    public function postEditLevel(Request $request)
    {
        try{
            // validate
            $isValid =  Validator::make(Input::all(), [
                'name'                =>  'required|string|min:3|max:10',
                'price'               =>  'required|string|min:1|max:7',
                'instagram_accounts'  =>  'required|string|min:1|max:2',
                'features'            =>  'required',
                'automation_speed'    =>  'required|string',
                'support'             =>  'required',
                'valid_time'          =>  'required|string|min:1|max:3'
            ]);
            
            if($isValid->fails()){
                return redirect('/admin/levels/edit'.$request->input('id'))->withErrors($isValid)->withInput();
            }
            else{
                DB::table('levels')->where('id', $request->input('id'))->update([
                    'name'  =>  $request->input('name'),
                    'price' =>  $request->input('price'),
                    'instagram_accounts'    =>  $request->input('instagram_accounts'),
                    'features'              =>  json_encode($request->input('features')),
                    'automation_speed'      =>  $request->input('automation_speed'),
                    'support'               =>  json_encode($request->input('support')),
                    'valid_time'            =>  $request->input('valid_time'),
                    'stripe_plan_id'        =>  $request->input('stripe_plan_id'),
                    'stripe_plan_name'      =>  $request->input('stripe_plan_name')
                ]);
                return redirect('/admin/levels');
            }
        }
        catch(\Exception $ex){
            return redirect('/admin/levels/edit'. $request->input('id'))->with('status.error', 'Something went wrong, try again later');
        }
    }

    public function postEnableLevel($id)
    {
        DB::table('levels')->where('id', $id)->update([
            'enabled'   =>  true
        ]);

        return redirect('/admin/levels');
    }

    public function postDisableLevel($id)
    {
        DB::table('levels')->where('id', $id)->update([
            'enabled'   =>  false
        ]);

        return redirect('/admin/levels');
    }
    
    public function getSpeed()
	{
        $speed_settings = DB::table('speed')->first();
        if(!$speed_settings)
        {
            DB::table('speed')->insert([
                'ultra_fast'    =>  '120',
                'fast'          =>  '300',
                'medium'        =>  '600',
                'slow'          =>  '900'
            ]);
            $speed_settings = DB::table('speed')->first();
        }
		return view('admin.speed')->with('data',$speed_settings);
	}

	public function postSpeed(Request $request)
	{
        $ultra_fast = $request->input('ultra_fast_speed');
		$fast = $request->input('fast_speed');
		$medium = $request->input('medium_speed');
		$slow = $request->input('slow_speed');

		DB::table('speed')->update([
            'ultra_fast'    =>  $ultra_fast,
            'fast'		    =>	$fast,
			'medium'	    =>	$medium,
			'slow'		    =>	$slow
		]);

		return redirect('/admin');
    }

    public function getSite()
    {
        $site_settings = DB::table('site_settings')->first();
        if(!$site_settings)
        {
            DB::table('site_settings')->insert([
                'language'  =>  'en',
                'theme'     =>  'blue'
            ]);
            $site_settings = DB::table('site_settings')->first();
        }
		return view('admin.site')->with('data',$site_settings);
    }

    public function postSite(Request $request)
    {
        $file_to_upload = $request->file('site_logo');
        $path = null;

        if($file_to_upload)
        {
            // $path = $request->file('site_logo')->store('avatars');

            Storage::disk('uploads')->putFileAs('logos', $request->file('site_logo'), 'logo');
            $path = "uploads/logos/logo";
        }
        else
        {
            $query = DB::table('site_settings')->first();
            $path = $query->logo;

        }

        DB::table('site_settings')->update([
            'name'      =>  $request->input('site_name'),
            'logo'      =>  $path,
            'language'  =>  $request->input('site_language'),
            'theme'     =>  $request->input('site_theme')
        ]);
        return redirect('/admin');
    }

    public function getRemoveLogo()
    {
        DB::table('site_settings')->update([
            'logo'      =>  ""
        ]);
        return redirect('/admin');
    }
    
    public function getSettings()
    {
        return view('admin.settings');
    }

    public function postSettings(Request $request)
    {
        return json_encode($request->all());
    }

    public function getEmail()
    {
        $email = DB::table('email_settings')->first();
        return view('admin.settings.email')->with('email', $email);
    }

    public function postEmail(Request $request)
    {
        $email = DB::table('email_settings')->first();
        if($email)
        {
            // update
            DB::table('email_settings')->update([
                'mail_host'     =>  $request->input('mail_host'),
                'mail_port'     =>  $request->input('mail_port'),
                'from_address'  =>  $request->input('from_address'),
                'from_name'     =>  $request->input('from_name'),
                'username'      =>  $request->input('username'),
                'password'      =>  $request->input('password')
            ]);
        }
        else
        {
            // insert
            DB::table('email_settings')->insert([
                'mail_host'     =>  $request->input('mail_host'),
                'mail_port'     =>  $request->input('mail_port'),
                'from_address'  =>  $request->input('from_address'),
                'from_name'     =>  $request->input('from_name'),
                'username'      =>  $request->input('username'),
                'password'      =>  $request->input('password')
            ]);
        }
        return redirect('/admin/settings/email');
    }

    public function getPages()
    {
        $page = DB::table('pages')->first();
        return view('admin.settings.pages')->with('page', $page);
    }
    
    public function postPages(Request $request)
    {
        $page = DB::table('pages')->first();
        if($page)
        {
            // update
            DB::table('pages')->update([
                'privacy'   =>  $request->input('privacy_content'),
                'terms'   =>  $request->input('terms_content')
            ]);
        }
        else
        {
            // insert
            DB::table('pages')->insert([
                'privacy'   =>  $request->input('privacy_content'),
                'terms'   =>  $request->input('terms_content')
            ]);
        }
        return redirect('/admin/settings/pages');
    }

    public function getProxies()
    {
        $proxies = DB::table('app_proxies')->get();
        return view('admin.proxies.all')->with('proxies',$proxies);
    }

    public function getAddProxy()
    {
        return view('admin.proxies.add');
    }

    public function postAddProxy(Request $request)
    {
        // return json_encode($request->all());
        // validate
        $isValid =  Validator::make(Input::all(), [
            'speed_type'        =>  'required',
            'total_users'       =>  'required',
            'ip'                =>  'required',
            'port'              =>  'required',
            'proxy_username'    =>  'required',
            'proxy_password'    =>  'required',
        ]);
        
        if($isValid->fails()){
            return redirect('/admin/proxies/add')->withErrors($isValid)->withInput();
        }
        else
        {
            if($fp = @fsockopen(
                $request->input('ip'),
                $request->input('port'),
                $errCode,
                $errStr,
                30
            )){   
                fclose($fp);
                
                DB::table('app_proxies')->insert([
                    'ip'            =>  $request->input('ip'),
                    'port'          =>  $request->input('port'),
                    'username'      =>  $request->input('proxy_username'),
                    'password'      =>  $request->input('proxy_password'),
                    'speed_type'    =>  $request->input('speed_type'),
                    'total_users'   =>  $request->input('total_users'),
                    'using_users'   =>  '0',
                    'time_added'    =>  time()
                ]);

                return redirect('/admin/proxies');

            } else {
                return redirect('/admin/proxies/add')->with('status', $errStr. " : " .$errCode);
            } 
        }
    }
    
    public function getEditProxy($id)
    {
        $proxy = DB::table('app_proxies')->where('id', $id)->first();
        return view('admin.proxies.edit')->with('proxy', $proxy);
    }

    public function postEditProxy(Request $request)
    {
        $isValid =  Validator::make(Input::all(), [
            'speed_type'        =>  'required',
            'total_users'       =>  'required',
            'ip'                =>  'required',
            'port'              =>  'required',
            'proxy_username'    =>  'required',
            'proxy_password'    =>  'required',
        ]);
        
        if($isValid->fails()){
            return redirect('/admin/proxies/edit/'.$request->input('id'))->withErrors($isValid)->withInput();
        }
        else
        {
            if($fp = @fsockopen(
                $request->input('ip'),
                $request->input('port'),
                $errCode,
                $errStr,
                30
            )){   
                fclose($fp);
                
                DB::table('app_proxies')
                ->where('id', $request->input('id'))
                ->update([
                    'ip'            =>  $request->input('ip'),
                    'port'          =>  $request->input('port'),
                    'username'      =>  $request->input('proxy_username'),
                    'password'      =>  $request->input('proxy_password'),
                    'speed_type'    =>  $request->input('speed_type'),
                    'total_users'   =>  $request->input('total_users')
                ]);

                return redirect('/admin/proxies');

            } else {
                return redirect('/admin/proxies/edit/'.$request->input('id'))->with('status', $errStr. " : " .$errCode);
            }    
        }
    }

    public function postDeleteProxy($id)
    {
        // return $id;
        DB::table('app_proxies')->where('id', $id)->delete();
        return redirect('/admin/proxies');
    }

    public function postSendNotification(Request $request)
    {
        DB::table('notifications')->insert([
            "user_id"   =>  $request->input('user_id'),
            "text_title"    =>  $request->input("title"),
            "text_content"  =>  $request->input("content"),
            "created_at"    =>  time()
        ]);
        return redirect(url("/admin"));
    }
}