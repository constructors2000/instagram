<?php

namespace App\Http\Controllers;

use App\User;
use App\User_Role;

use Mail;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Libraries\Utility as app_library;

class AccountController extends Controller
{
    public function index()
    {
        if(Auth::check())
        {
            if(Auth::user()->isAdmin() === true)
            {
                return redirect()->intended('/admin');
            }
            else {
                return redirect()->intended('/user');
            }
        }
        else
        {
            // return view('guest');
            return redirect()->intended('/login');
        }
    }

    public function getLogin()
    {
        return view('account.login');
    }

    public function postLogin(Request $request)
    {
        try{
            // validate
            $isValid =  Validator::make(Input::all(), [
                'email'         => 'required|string|email|min:5|max:50',
                'password'      => 'required|string|min:6|max:20'
            ]);
            
            if($isValid->fails()){
                return redirect('/login')->withErrors($isValid)->withInput();
            }
            else{
                
                if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'enabled' => true], true)) {
				
                    if(Auth::user()->isAdmin() === true)
                    {
                        return redirect()->intended('/admin');
                    }
                    else {
                        // make first instagram account as default for active instagram account.
                        $i_c = DB::table('instagram_accounts')->where('user_id', Auth::user()->id)->first();
                        if($i_c)
                        {
                            session(['active_instagram_account' => $i_c->id]);
                        }
                        return redirect()->intended('/user');
                    }
                }
                else{
                    $messages = [
                        'general'	=>	'invalid username or password'
                    ];
                    return view('account.login')->withErrors($messages);
                }

            }
        }
        catch(\Exception $ex)
        {
            return redirect('/login')->with('status.error', $ex->getMessage());
        }
    }

    public function getRegister()
    {
        // $plans = DB::table('levels')->where('id', '!=', 1)->orderBy('id', 'asc')->where('enabled', true)->get();
        return view('account.register');// ->with('plans',$plans);
    }

    public function postRegister(Request $request)
    {
        try{
            // validate
            $isValid =  Validator::make(Input::all(), [
                'first_name'    => 'required|string|min:3|max:10',
                'last_name'     => 'required|string|min:3|max:10',
                'email'         => 'required|string|email|min:5|max:50|unique:users',
                'password'      => 'required|string|min:6|max:20'
            ]);
            
            if($isValid->fails()){
                $messages = $isValid->messages();
                return redirect('/register')->withErrors($isValid)->withInput();
            }
            else{

                $app_library = new app_library;
                
                // random values to be inserted
                $random_token = $app_library->generate_random_value();

                // create account
                $user = User::create([
                    'first_name'    =>  $request->input('first_name'),
                    'last_name'     =>  $request->input('last_name'),
                    'email'         =>  $request->input('email'),
                    'password'      =>  bcrypt($request->input('password')),
                    'email_token'   =>  $random_token
                ]);

                // save role
                $save_role = User_Role::create([
                    'user_id'	=>	$user->id,
                    'levels'    => '["1"]', // by default user with 'basic' plan
                ]);

                // send email
                if($user)
                {
                    $data = [
                        'email' =>  $request->input('email'),
                        'name'  =>  $request->input('first_name'),
                        'code'  =>  $random_token
                    ];

                    $emails_to = array(
                        'email' => $request->input('email'),
                        'name' => $request->input('first_name')
                    );
                    
//                    Mail::send('emails.welcome', $data, function($message) use ($emails_to)
//                    {
//                        $message->to($emails_to['email'], $emails_to['name'])->subject('Welcome To Tubeans');
//                    });
                }

            
                // redirect to login page with message
                return redirect('/login')->with('status.success', 'Account Created, Please check email to activate account');
            }
        }
        catch(\Exception $ex)
        {
            // log error to database
            return redirect('/register')->with('status.error', $ex->getMessage());
        }
    }

    public function getReset()
    {
        return view('account.reset');
    }

    public function postReset(Request $request)
    {
        try
        {
            // validate
            $isValid =  Validator::make(Input::all(), [
                'email' =>  'required|string|email|min:5|max:50'
            ]);
            
            if($isValid->fails()){
                $messages = $isValid->messages();
                return redirect('/reset')->withErrors($isValid)->withInput();
            }
            else{

                // check user exists
                $user = DB::table('users')->where('email', $request->input('email'))->first();
                if($user)
                {
                    // create reset token
                    $app_library = new app_library;
                    
                    // random values to be inserted
                    $random_token = $app_library->generate_random_value();

                    $update_user = DB::table('users')->where('id', $user->id)->update([
                        'reset_token'   =>  $random_token
                    ]);

                    // send token to email
                    if($update_user)
                    {
                        $data = [
                            'email' =>  $user->email,
                            'name'  =>  $user->first_name,
                            'code'  =>  $random_token
                        ];

                        $emails_to = array(
                            'email' => $user->email,
                            'name' => $user->first_name
                        );
                        
                        Mail::send('emails.reset', $data, function($message) use ($emails_to)
                        {
                            $message->to($emails_to['email'], $emails_to['name'])->subject('Tubeans :: Reset Request');
                        });
                    }
                }

                return redirect('/login')->with('status.success', 'Reset Instructions Sent to your email.');
            }
        }
        catch(\Exception $ex)
        {
            return redirect('/reset')->with('status.error', 'Something went wrong, try again later');
        }
    }

    public function getResetPassword(Request $request)
    {
        if($request->input('code'))
        {
            return view('account.reset_password')->with('code',$request->input('code'));
        }
        else
        {
            return redirect('/reset')->with('status.error', 'unable to process the request, please reset again.');
        }
    }

    public function postResetPassword(Request $request)
    {
        try
        {
            // validate inputs
            // validate
            $isValid =  Validator::make(Input::all(), [
                'password'      => 'required|string|min:6|max:20|confirmed'
            ]);
            
            if($isValid->fails()){
                return redirect('/reset_password?code='.$request->input('reset_token'))->withErrors($isValid)->withInput();
            }
            else{
                // validate token
                // update password and delete old token
                $exists = DB::table('users')->where('reset_token', $request->input('reset_token'))->update([
                    'password'  =>  bcrypt($request->input('password')),
                    'reset_token'   =>  null
                ]);
                
                return redirect('/login')->with('status.success', 'password has been reset.');
            }
        }
        catch(\Exception $ex)
        {
            return redirect('/reset')->with('status.error', 'something went wrong, please try again later.');
        }
    }

    public function getActivate(Request $request)
    {
        try{
            if($request->input('code'))
            {
                // activate without asking for input
                $done = DB::table('users')->where('enabled', false)->where('email_token', $request->input('code'))->update([
                    'enabled'   =>  true
                ]);

                if($done)
                {
                    return redirect('/login')->with('status.success','Account Activated, login to continue.');
                }
                else
                {
                    return redirect('/activate')->with('status.error','Unable to activate, please check code again.');
                }
            }
            else{
                // ask for code
                return view('account.activate');
            }
        }
        catch(\Exception $ex)
        {
            return redirect('/activate')->with('status.error','Something went wrong, try again later');
        }
    }

    public function postActivate(Request $request)
    {
        try
        {
            // validate
            $isValid =  Validator::make(Input::all(), [
                'code'    => 'required|string'
            ]);
            
            if($isValid->fails()){
                $messages = $isValid->messages();
                return redirect('/activate')->withErrors($isValid)->withInput();
            }
            else{

                $done = DB::table('users')->where('enabled', false)->where('email_token', $request->input('code'))->update([
                    'enabled'   =>  true
                ]);

                if($done)
                {
                    return redirect('/login')->with('status.success','Account Activated, login to continue.');
                }
                else
                {
                    return redirect('/activate')->with('status.error','Unable to activate, please check code again.');
                }
            }
        }
        catch(\Exception $ex)
        {
            return redirect('/activate')->with('status.error','Something went wrong, try again later');
        }
        
    }

    public function postLogout(Request $request)
    {
        Session::flush();
        return redirect('/login');
    }

    public function postResetRequest(Request $request)
    {
        Session::flush();
        return redirect('/reset');
    }

    public function getPrivacy()
    {
        $pages = DB::table('pages')->first();
        return view('account.privacy')->with('content',$pages->privacy);
    }

    public function getTerms()
    {
        $pages = DB::table('pages')->first();
        return view('account.terms')->with('content',$pages->terms);
    }

    public function getPlans()
    {
        $plans = DB::table('levels')->where('id', '!=', 1)->orderBy('id', 'asc')->where('enabled', true)->get();
        return view('account.plans')->with('plans', $plans);
    }

    public function postChoosePlan(Request $request)
    {
        $data = [
            "plan_id"   =>  $request->input("plan_id"),
            "plan_name" =>  $request->input("plan_name")
        ];
        return redirect('/register')->with('status.plan_details',$data);
    }
}