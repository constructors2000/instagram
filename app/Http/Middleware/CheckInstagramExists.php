<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class CheckInstagramExists
{
	public function handle($request, Closure $next)
	{
		$user = Auth::user();

		if ($user) {
			if ($user->isUser()) {
				// billing exists
				if($user->bilingExists())
				{
					if($user->instagramAccountExists()){
						return $next($request);
					}
					else{
						return redirect('/user/accounts?error=1')->with('status.info', 'Add an Instagram Account First.');
					}
				}
				else
				{
					return redirect('/user/payment')->with('status.info', 'Add Billing Info');
				}
			}
		}
		
		return redirect('/login');
		
	}
}