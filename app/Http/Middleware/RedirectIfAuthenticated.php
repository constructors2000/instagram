<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->isAdmin() === true)
            {
                return redirect()->intended('/admin');
            }
            else {
                // set active instagram account
                if(sizeof(Auth::user()->instagramAccounts()) > 0)
                {
                    session(['active_instagram_account' => Auth::user()->instagramAccounts()[0]->id]);
                }
                return redirect()->intended('/user');
            }
        }

        return $next($request);
    }
}
