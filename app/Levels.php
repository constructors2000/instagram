<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Levels extends Model
{
    protected $table = 'levels';

    protected $fillable = [
        'name','instagram_accounts', 'features', 'automation_speed', 'support', 'valid_time'
    ];
}