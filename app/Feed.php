<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $table = 'feed';

    protected $fillable = [
		'post_one_pk',
		'post_two_pk',
        'post_three_pk',
        
        'liked_one_done',
        'liked_one_time',

        'liked_two_done',
        'liked_two_time',

        'liked_three_done',
        'liked_three_time',

        'comment_one',
        'comment_one_done',
        'comment_one_time',

        'comment_two',
        'comment_two_done',
        'comment_two_time',

        'follow_done',
        'follow_time'.

        'unfollow_done',
        'unfollow_time',

        'message',
        'message_attachment',
        'message_done',
        'message_time',

        'done',
        'exception'
    ];
}